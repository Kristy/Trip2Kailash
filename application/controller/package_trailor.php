<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Package_trailor extends Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $Page = "Package_Booking_Page";

        $package_object = "";
        $package_id = $_GET['package'];
        if($package_id){
            $package_object = $this->model->getPackageRow($package_id);
        }
        $full_name = "";
        $gender = "";
        $nationality = "";
        $dob = "";
        $passport_no = "";
        $date_of_issue = "";
        $place_of_issue = "";
        $date_of_expiry = "";
        $permanent_address = "";
        $profession = "";
        $mobile_no = "";
        $telephone = "";
        $email_id = "";
        $e_name = "";
        $e_relation = "";
        $e_telephone = "";
        $e_email_id = "";
        $preffered_date = "";
        $description = "";
        $adv_deposit = "";

        $msg = "";
        $msgtype = "";
        if(isset($_POST["trailorPackage"]))
        {
          $full_name = $_POST['full_name'];
          $gender = $_POST['gender'];
          $nationality = $_POST['nationality'];
          $dob = $_POST['dob'];
          $passport_no = $_POST['passport_no'];
          $date_of_issue = $_POST['date_of_issue'];
          $place_of_issue = $_POST['place_of_issue'];
          $date_of_expiry = $_POST['date_of_expiry'];
          $permanent_address = $_POST['permanent_address'];
          $profession = $_POST['profession'];
          $mobile_no = $_POST['mobile_no'];
          $telephone = $_POST['telephone'];
          $email_id = $_POST['email_id'];
          $e_name = $_POST['e_name'];
          $e_relation = $_POST['e_relation'];
          $e_telephone = $_POST['e_telephone'];
          $e_email_id = $_POST['e_email_id'];
          $preffered_date = $_POST['preffered_date'];
          $description = $_POST['description'];

          $maxsize = 10000000; //set to approx 10 MB

          if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
            //check whether file is uploaded with HTTP POST
            if(is_uploaded_file($_FILES['userfile']['tmp_name']))
            {
              //checks size of uploaded image on server side
              if( $_FILES['userfile']['size'] < $maxsize)
              {
                  $full_name = stripslashes($full_name);
                  $gender = stripslashes($gender);
                  $nationality= stripslashes($nationality);
                  $dob = stripslashes($dob);
                  $passport_no = stripslashes($passport_no);
                  $date_of_issue = stripslashes($date_of_issue);
                  $place_of_issue= stripslashes($place_of_issue);
                  $date_of_expiry = stripslashes($date_of_expiry);
                  $permanent_address = stripslashes($permanent_address);
                  $profession= stripslashes($profession);
                  $mobile_no = stripslashes($mobile_no);
                  $telephone = stripslashes($telephone);
                  $email_id = stripslashes($email_id);
                  $e_name= stripslashes($e_name);
                  $e_relation = stripslashes($e_relation);
                  $e_telephone = stripslashes($e_telephone);
                  $e_email_id= stripslashes($e_email_id);
                  $preffered_date = stripslashes($preffered_date);
                  $description = stripslashes($description);

                  $file_name = $_FILES['userfile']['name'];
                  $ext = new SplFileInfo($file_name);
                  $file_name = 'package_trailor_'.time().'.'.$ext->getExtension();
                  $file_tmp = $_FILES['userfile']['tmp_name'];
                  $target_dir = "uploads/".$file_name;

                  $full_name = trim($full_name);
                  $gender = trim($gender);
                  $nationality= trim($nationality);
                  $dob = trim($dob);
                  $passport_no = trim($passport_no);
                  $date_of_issue = trim($date_of_issue);
                  $place_of_issue= trim($place_of_issue);
                  $date_of_expiry = trim($date_of_expiry);
                  $permanent_address = trim($permanent_address);
                  $profession= trim($profession);
                  $mobile_no = trim($mobile_no);
                  $telephone = trim($telephone);
                  $email_id = trim($email_id);
                  $e_name= trim($e_name);
                  $e_relation = trim($e_relation);
                  $e_telephone = trim($e_telephone);
                  $e_email_id= trim($e_email_id);
                  $preffered_date = trim($preffered_date);
                  $description = trim($description);

                  move_uploaded_file($file_tmp, $target_dir);
                  $this->model->addPackageTrailor($full_name, $file_name, $gender, $package_id, $nationality, $dob, $passport_no, $date_of_issue, $place_of_issue, $date_of_expiry, $permanent_address, $profession, $mobile_no, $telephone, $email_id, $e_name, $e_relation, $e_telephone, $e_email_id, $preffered_date, $description);
                  $msg = "Booked Successfully";
                  $msgtype = "success";
              }
              else
              {
                // if the file is not less than the maximum allowed, print an error
                $msgtype = "error";
                $msg='File exceeds the Maximum File limit<br/>
                    Maximum File limit is '.$maxsize.' bytes<br/>
                File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
                ' bytes';
              }
            }
            else
            {
              $msgtype = "error";
              $msg="File not uploaded successfully.";
            }

          }
          else
          {
            $msgtype = "error";
            $error_code = $_FILES['userfile']['error'];
            if($error_code == UPLOAD_ERR_INI_SIZE)
            {
              $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
            }
            else if($error_code == UPLOAD_ERR_FORM_SIZE)
            {
              $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
            }
            else if($error_code == UPLOAD_ERR_PARTIAL)
            {
              $msg = 'The uploaded file was only partially uploaded';
            }
            else if($error_code == UPLOAD_ERR_NO_FILE)
            {
              $msg = 'No file was uploaded';
            }
            else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
            {
              $msg = 'Missing a temporary folder';
            }
            else if($error_code == UPLOAD_ERR_CANT_WRITE)
            {
              $msg = 'Failed to write file to disk';
            }
            else if($error_code == UPLOAD_ERR_EXTENSION)
            {
              $msg = 'File upload stopped by extension';
            }
            else
            {
              $msg = 'Unknown upload error';
            }
          }
        }
        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/packageTrailor_view.php';
        require APP . 'view/website/templates/footer.php';

    }

}
