<?php

class ManageServiceItenary extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        $sp_id = $_GET['sp_id'];
        $itenaries = $this->model->getServiceItenary($sp_id);
        $browser_title = "Trip2Kailash | Manage Service Itenary";
        $ActivePage = "Service";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListServiceItenary_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addItenary()
  {
    $msg = "";
    $msgtype = "";
    $sp_id = $_GET['sp_id'];

    $name = "";
    $title = "";
    $description = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addItenary"]))
    {
      $name = $_POST['name'];
      $title = $_POST['title'];
      $description = $_POST['description'];

      $name = stripslashes($name);
      $title = stripslashes($title);
      $description= stripslashes($description);

      $name = trim($name);
      $title = trim($title);
      $description= trim($description);

      $this->model->addServiceItenaryModel($name, $title, $description, $sp_id);
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageServiceItenary/addItenary?sp_id='.$sp_id, $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Itenary";
    $ActivePage = "Service";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddServiceItenary_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteItenary()
  {
     $sp_id = $_GET['sp_id'];
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $this->model->deleteServiceItenaryModel($id);

      $flashmsg->info('A data has been deleted', URL . 'manageServiceItenary?sp_id='.$sp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageServiceItenary');
    }

  }

  public function editItenary()
  {
    $sp_id = $_GET['sp_id'];

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $itenary = $this->model->getServiceItenaryRow($id);
      if($itenary)
      {
        $name = $itenary->name;
        $title = $itenary->title;
        $description = $itenary->description;
        $browser_title = "Trip2Kailash | Edit Itenary";
        $ActivePage = "Service";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditServiceItenary_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageServiceItenary');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {

      $id = $_POST['editActivity_submit'];
      $name = $_POST['name'];
      $title = $_POST['title'];
      $description = $_POST['description'];

      $id = stripslashes($id);
      $name = stripslashes($name);
      $title = stripslashes($title);
      $description = stripslashes($description);

      $id = trim($id);
      $name = trim($name);
      $title = trim($title);
      $description = trim($description);



      $titleCount = $this->model->getotherServiceItenaryRowheading($title, $id);


      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageServiceItenary/editItenary?submit_to_edit='.$id, 'error');
      }

      $this->model->editSItenary($id, $name, $title, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageServiceItenary?sp_id='.$sp_id, 'INFO');

    }
    else if (isset($_POST["editImage_submit"]))
    {
      $id = $_POST['editImage_submit'];

      $id = stripslashes($id);

      $id = trim($id);

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'service_itenary_'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $itenary = $this->model->getServiceItenaryRow($id);


              unlink("uploads/".$itenary->image);
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->editServiceItenary($id, $file_name);

              $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
              $flashmsg->info($title.' has been edited', URL . 'manageServiceItenary?sp_id='.$sp_id, 'INFO');

            }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }
      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    else
    {
      header('location: ' . URL . 'manageServiceItenary');
    }
  }
}
