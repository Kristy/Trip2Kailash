<?php

class ManageTestimonial extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $testimonials = $this->model->getallTestimonial();
        $browser_title = "Trip2Kailash | Manage Testimonial";
        $ActivePage = "Manage_Testimonial";



        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListTestimonial_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addTestimonial()
  {
    $msg = "";
    $msgtype = "";
    $name = "";
    $feedback = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addTestimonial"]))
    {
      $name = $_POST['name'];
      $feedback = $_POST['feedback'];
      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $name = stripslashes($name);
            $description= stripslashes($feedback);

            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'team_member'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $name = trim($name);
            $feedback= trim($feedback);

            $count_heading = $this->model->getTestimonialRowbyheading($name);

            if($count_heading->RowCount() > 0)
            {
              $msg = "Please enter unique heading.";
              $msgtype = "error";
            }
            else
            {
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->addTestimonial($name, $file_name, $feedback);
              $msg = "Successfully added";
              $msgtype = "success";
            }
          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
                Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else{
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }
      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageTestimonial/addTestimonial', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Testimonial";
    $ActivePage = "Manage_Testimonial";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddTestimonial_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteTestimonial()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->getTestimonialRow($id);
      if($package){
        $this->model->deleteTestimonial($id);

        $flashmsg->info('A data has been deleted', URL . 'manageTestimonial', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageTestimonial', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageTestimonial');
    }

  }

  public function editTestimonial()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $Testimonial = $this->model->getTestimonialRow($id);
      if($Testimonial)
      {
        $name = $Testimonial->name;
        $feedback = $Testimonial->feedback;
        $browser_title = "Trip2Kailash | Edit Package";
        $ActivePage = "Manage_FAQ";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditTestimonial_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageTestimonial');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $name = $_POST['name'];
      $feedback = $_POST['feedback'];

      $id = stripslashes($id);
      $name = stripslashes($name);
      $feedback = stripslashes($feedback);

      $id = trim($id);
      $name = trim($name);
      $feedback = trim($feedback);

      $titleCount = $this->model->getotherTestimonialRowheading($name, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageTestimonial/editTestimonial?submit_to_edit='.$id, 'error');
      }

      $this->model->editTestimonial($id, $name, $feedback);
      print_r($name);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageTestimonial', 'INFO');

    }
    else if (isset($_POST["editImage_submit"]))
    {
      $id = $_POST['editImage_submit'];

      $id = stripslashes($id);

      $id = trim($id);

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'testimonial_'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $itenary = $this->model->getTestimonialRow($id);

              
              unlink("uploads/".$itenary->image);
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->editTestimonialImage($id, $file_name);
                
              $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
              $flashmsg->info($title.' has been edited', URL . 'manageTestimonial', 'INFO');
              
            }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }
      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    else
    {
      header('location: ' . URL . 'manageTestimonial');
    }
  }
}
