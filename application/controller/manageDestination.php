<?php

class ManageDestination extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $destinations = $this->model->getDestinations();
        $browser_title = "Trip2Kailash | Tour Requirement";
        $ActivePage = "Service";
        $PageBranch = "Manage_Destination";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListDestination_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addDestination()
  {
    $msg = "";
    $msgtype = "";

    $title = "";
    $description = "";
    $summary = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addRequirement"]))
    {
      $name = $_POST['name'];
      $description = $_POST['description'];
      $summary = $_POST['summary'];
      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $name = stripslashes($name);
            $description = stripslashes($description);
            $summary = stripslashes($summary);

            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'destination_'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $name = trim($name);
            $description = trim($description);
            $summary = trim($summary);

            $check_name = $this->model->getDestinationRowByName($name);
            if($check_name->RowCount() > 0){
              $msg = "Please enter unique name.";
              $msgtype = "error";
            }
            else{
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->addDestinationModel($name, $file_name, $description, $summary);
              $msg = "Successfully added";
              $msgtype = "success";
            }
          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
            Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }

      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageDestination/addDestination', $msgtype);
      }
      else{
        $formmsg->info($msg, '', $msgtype);
      }
    }

    $browser_title = "Trip2Kailash | Add Destination";
    $ActivePage = "Service";
    $PageBranch = "Manage_Destination";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/addDestination_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteDestination()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $this->model->deleteDestinationModel($id);

      $flashmsg->info('A data has been deleted', URL . 'manageDestination', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageDestination');
    }

  }
  public function editDestination()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $destination = $this->model->getDestinationRow($id);
      if($destination)
      {
        $name = $destination->name;
        $description = $destination->description;
        $summary = $destination->summary;
        $browser_title = "Trip2Kailash | Edit Destination";
        $ActivePage = "Manage_Destination";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/editDestination_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'editDestination');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $name = $_POST['name'];
      $description = $_POST['description'];
      $summary = $_POST['summary'];

      $id = stripslashes($id);
      $name = stripslashes($name);
      $description = stripslashes($description);
      $summary = stripslashes($summary);

      $id = trim($id);
      $name = trim($name);
      $description = trim($description);

            $summary = trim($summary);

      $titleCount = $this->model->getotherDestinationRowheading($name, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageDestination/editDestination?submit_to_edit='.$id, 'error');
      }

      $this->model->editDestination($id, $name, $description, $summary);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageDestination', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageDestination');
    }
  }
}
