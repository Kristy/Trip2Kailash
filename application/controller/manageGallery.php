<?php

class ManageGallery extends Controller
{
  function __construct()
	{
    parent::__construct();
	  session_start();
	  if(isset($_SESSION['logged_in']) == null)
		{
      header('location: ' . URL . 'Errorsite');
			exit;
		}
	}

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
            $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

            $title = "Trip2Kailash | Manage Gallery";
    		$ActivePage = "Gallery";
    		$PageBranch = "";

            $gallery_images = $this->model->getallImages();

            require APP . 'view/AdminPanel/template/header.php';
    		require APP . 'view/AdminPanel/ListGallery_view.php';
    		require APP . 'view/AdminPanel/template/footer.php';
            unset($_SESSION['flash_messages']);
		}
		else{
            require APP . 'view/error/index.php';
		}
	}
	else{
		header('location: ' . URL . 'cmslogin');
	}
  }

  public function addImages()
  {
    $msg = "";
    $msgtype = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (!empty($_FILES))
    {
      $targetDir = "uploads/";
      $file_name = $_FILES['file']['name'];
      $ext = new SplFileInfo($file_name);
      $file_name = 'gallery_'.microtime().'.'.$ext->getExtension();
      $file_tmp = $_FILES['file']['tmp_name'];
      $target_dir = "uploads/".$file_name;

      if(move_uploaded_file($file_tmp, $target_dir)){
        $this->model->addImages($file_name);
      }

    }

    $title = "Trip2Kailash | Add Gallery";
    $ActivePage = "Gallery";
    $PageBranch = "";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddGallery_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteImage()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $GalleryRow = $this->model->getGalleryRow($id);
      $Imagename = $GalleryRow->image_name;
      if($GalleryRow){
        if(unlink("uploads/".$GalleryRow->image_name)){
          $this->model->deleteGalleryImage($GalleryRow->id);
          $flashmsg->info($Imagename.'  has been deleted', URL . 'manageGallery', 'INFO');
        }
        else{
          $flashmsg->info($Imagename.' was not found', URL . 'manageGallery', 'ERROR');
        }
      }
      else {
        $flashmsg->info('Data was not found', URL . 'manageGallery', 'ERROR');
      }
    }
    else
    {
      header('location: ' . URL . 'manageGallery');
    }

  }
}
