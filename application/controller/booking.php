<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Booking extends Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $Page = "Kailash_detail_Page";
        $kailash = $_GET['kailash'];

        $kailash_id = $_GET['kailash'];
        $package_detail = $this->model->getkailashPackageRow($kailash_id);
        $package_itenaries = $this->model->getKailashItenary($kailash_id);
        $package_IEs = $this->model->getKailashTourRequirements($kailash_id);
        $FAQs = $this->model->getallFAQ();
        $bookings = $this->model->getKailashBooking($kailash_id);

        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/kailash_tour_detail_view.php';
        require APP . 'view/website/templates/footer.php';

    }

    public function addBooking()
  {
    $msg = "";
    $msgtype = "";
    $kp_id = $_GET['kp_id'];
    $from = "";
    $to = "";
    $status = "";
    $package_detail = $this->model->getkailashPackageRow($kp_id);
    print_r($package_detail);
    die;

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addBooking"]))
    {
      $from = $_POST['from'];
      $to = $_POST['to'];
      $status = $_POST['status'];

      $from = stripslashes($from);
      $status = stripslashes($status);
      $to= stripslashes($to);

      $from = trim($from);
      $status = trim($status);
      $to= trim($to);

      $this->model->addBooking($kp_id, $from, $to, $status);
      
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageKailashBooking/addBooking?kp_id='.$kp_id, $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Booking";
    $ActivePage = "Kailash_Page";
    $PageBranch = "Manage_Booking";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddKailashBooking_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }


}
