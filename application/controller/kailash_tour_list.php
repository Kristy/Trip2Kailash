<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Kailash_tour_list extends Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $Page = "KailashTourList";

        $kailash_detail = $this->model-> get_kailash_detail_model(1);
        $kailash_packages = $this->model->getallKailashPackages();
        $kailash_guides = $this->model->getallKailashGuides();

        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/kailash_tour_list_view.php';
        require APP . 'view/website/templates/footer.php';

    }

}
