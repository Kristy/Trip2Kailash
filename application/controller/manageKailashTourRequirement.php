<?php

class ManageKailashTourRequirement extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        $kp_id = $_GET['kp_id'];
        $requirements = $this->model->getKailashTourRequirements($kp_id);
        $browser_title = "Trip2Kailash | Kailash Tour Requirement";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListKailashTourRequirement_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addTourRequirement()
  {
    $msg = "";
    $msgtype = "";
    $kp_id = $_GET['kp_id'];
    $title = "";
    $description = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addRequirement"]))
    {
      $title = $_POST['title'];
      $description = $_POST['description'];

      $title = stripslashes($title);
      $description= stripslashes($description);

      $title = trim($title);
      $description= trim($description);

      $this->model->addKailashTourRequirementModel($title, $kp_id, $description);
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageKailashTourRequirement/addTourRequirement?kp_id='.$kp_id, $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Requirement";
    $ActivePage = "Kailash_Page";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddKailashTourRequirement_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteTourRequirement()
  {
     $kp_id = $_GET['kp_id'];
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $this->model->deleteKailashTourRequirementModel($id);

      $flashmsg->info('A data has been deleted', URL . 'manageKailashTourRequirement?kp_id='.$kp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageKailashTourRequirement?kp_id='.$kp_id);
    }

  }
}
