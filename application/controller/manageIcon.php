<?php

class ManageIcon extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $icons = $this->model->getallIcon();
        $browser_title = "Trip2Kailash | Manage Icon";
        $ActivePage = "Manage_Icon";



        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListIcon_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addIcon()
  {
    $msg = "";
    $msgtype = "";
    $icon = "";
    $icon_name = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addIcon"]))
    {
      $icon = $_POST['icon'];
      $icon_name = $_POST['icon_name'];

      $icon = stripslashes($icon);
      $icon_name= stripslashes($icon_name);

      $icon = trim($icon);
      $icon_name= trim($icon_name);

      $this->model->addIcon($icon, $icon_name);
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageIcon/addIcon', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Icon";
    $ActivePage = "Manage_Icon";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddIcon_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteIcon()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->getIconRow($id);
      if($package){
        $this->model->deleteIcon($id);

        $flashmsg->info('A data has been deleted', URL . 'manageIcon', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageIcon', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageIcon');
    }

  }

  public function editPackage()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $PackageDetail = $this->model->getPackageRow($id);
      $activities = $this->model->getallActivities();
      if($PackageDetail)
      {
        $browser_title = "Trip2Kailash | Edit Package";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditPackage_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageKailashPackage');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['heading'];
      $activity = $_POST['activity'];
      $description = $_POST['description'];

      $id = stripslashes($id);
      $title = stripslashes($title);
      $activity = stripslashes($activity);
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $activity = trim($activity);
      $description = trim($description);

      $titleCount = $this->model->getotherPackageHeading($title, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageKailashPackage/editPackage?submit_to_edit='.$id, 'error');
      }

      $this->model->editPackageModel($id, $title, $activity, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageKailashPackage', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageKailashPackage');
    }
  }
}
