<?php

class ManageActivity extends Controller
{
  function __construct()
	{
    parent::__construct();
	  session_start();
	  if(isset($_SESSION['logged_in']) == null)
		{
      header('location: ' . URL . 'Errorsite');
			exit;
		}
	}

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $activities = $this->model->get_activities_model();
        $browser_title = "Trip2Kailash | List Activities";
		$ActivePage = "Service";
		$PageBranch = "Manage_Activities";

        require APP . 'view/AdminPanel/template/header.php';
		require APP . 'view/AdminPanel/ListActivities_view.php';
		require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
	  }else{
        require APP . 'view/error/index.php';
	  }
	}
	else{
		header('location: ' . URL . 'cmslogin');
	}
  }

  public function addActivity()
  {
    $msg = "";
    $msgtype = "";
    //$file_name = "";
    $title = "";
    $destination = "";
    $description = "";
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    $destinations = $this->model->getDestinations();
    if(isset($_POST["addActivity"]))
    {
      $title = $_POST['title'];
      $destination = $_POST['destination'];
      $description = $_POST['description'];
      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
              $title = stripslashes($title);
              $destination = stripslashes($destination);
              $description= stripslashes($description);

              $file_name = $_FILES['userfile']['name'];
              $ext = new SplFileInfo($file_name);
              $file_name = 'activity_'.time().'.'.$ext->getExtension();
              $file_tmp = $_FILES['userfile']['tmp_name'];
              $target_dir = "uploads/".$file_name;

              $title = trim($title);
              $destination = trim($destination);
              $description= trim($description);

              $count_heading = $this->model->getActivityRowbyheading($title, $destination);

              if($count_heading->RowCount() > 0)
              {
                $msg = "Please enter unique title for selected destination.";
                $msgtype = "error";
              }
              else
              {
                move_uploaded_file($file_tmp, $target_dir);
                $this->model->addActivityModel($title, $file_name, $destination, $description);
                $msg = "Successfully added";
                $msgtype = "success";
              }
          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
                Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }

      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageActivity/addActivity', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Activity";
    $ActivePage = "Service";
    $PageBranch = "Manage_Activities";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddActivity_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteActivity()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $activity = $this->model->getActivityRow($id);
      
      if($activity){
        unlink("uploads/".$activity->image);
        $this->model->deleteActivityModel($id);

        $flashmsg->info('A data has been deleted', URL . 'manageActivity', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageActivity', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageActivity');
    }

  }

  public function editActivity()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $ActivityDetail = $this->model->getActivityRow($id);
      $destinations = $this->model->getDestinations($id);
      if($ActivityDetail)
      {
        $title = $ActivityDetail->title;
        $d_name = $ActivityDetail->d_name;;
        $description = $ActivityDetail->description;

        $browser_title = "Trip2Kailash | Edit Actiity";
        $ActivePage = "Service";
        $PageBranch = "Manage_Activities";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditActivity_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageActivity');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $destination = $_POST['destination'];
      $description = $_POST['description'];

      $id = stripslashes($id);
      $title = stripslashes($title);
      $destination = $_POST['destination'];
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $destination = $_POST['destination'];
      $description = trim($description);

      $titleCount = $this->model->getotherActivityRowheading($title, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageActivity/editActivity?submit_to_edit='.$id, 'error');
      }

      $this->model->editActivity($id, $title, $destination, $description);
      

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageActivity', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageActivity');
    }
  }

}
