<?php

class DashBoardPanel extends Controller
{
  function __construct()
	{
    parent::__construct();
	  session_start();
	  if(isset($_SESSION['logged_in']) == null)
		{
      header('location: ' . URL . 'Errorsite');
			exit;
		}
	}

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        // if ($flashmsg->hasMessages()) {
        //     print_r($flashmsg->display());
        //     echo "<br/>";
        //     print_r($flashmsg->hasMessages()[0]['sticky']);
        //     die;
        // }
          // if($msg->hasMessages($msg::SUCCESS)) {
          //   print_r($msg::TYPE);
          //   die;
          // }
        $banners = $this->model->getallBanner();
        $title = "Feel Himalaya | Dashboard";
				$ActivePage = "Customize_Content";
				$PageBranch = "Banner_Page";

        require APP . 'view/AdminPanel/template/header.php';
				require APP . 'view/AdminPanel/DashBoard.php';
				require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
			}
			else{
        require APP . 'view/error/index.php';
			}
		}
		else{
			header('location: ' . URL . 'cmslogin');
		}
  }

  public function addBanner()
  {
    $msg = "";
    $msgtype = "";

    if(isset($_POST['submitcropped'])){
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $BannerTitle = $_POST['BannerTitle'];
      $BannerDesc = $_POST['BannerDesc'];

      $x = $_POST['x'];
      $y = $_POST['y'];
      $w = $_POST['w'];
      $h = $_POST['h'];

      $BannerTitle = stripslashes($BannerTitle);
      $BannerDesc = stripslashes($BannerDesc);

      $x = stripslashes($x);
      $y = stripslashes($y);
      $w = stripslashes($w);
      $h = stripslashes($h);

      $BannerTitle = trim($BannerTitle);
      $BannerDesc = trim($BannerDesc);

      $x = trim($x);
      $y = trim($y);
      $w = trim($w);
      $h = trim($h);
      if($_FILES['file']['error'] != 0)
      {
        $flashmsg->info("Error Uploading file", URL . 'DashBoardPanel', "success");
      }
      if (!empty($_FILES))
      {
        $targetDir = "uploads/banner/";
        $file_name = $_FILES['file']['name'];
        $ext = new SplFileInfo($file_name);
        $file_name = 'banner_'.microtime().'.'.$ext->getExtension();
        $file_tmp = $_FILES['file']['tmp_name'];
        $target_dir = "uploads/banner/".$file_name;

        if(move_uploaded_file($file_tmp, $target_dir)){
          $new_image = imagecreatefromjpeg($target_dir);
          $new_hw = imagecreatetruecolor($w,$h);
          imagecopyresampled($new_hw,$new_image,0,0,$x,$y,$w,$h,$w,$h);
          if(imagejpeg($new_hw,$target_dir, 100)){
            $this->model->addBanner($file_name, $BannerTitle, $BannerDesc);
            print_r($BannerDesc);
          }
        }

      }
      $flashmsg->info("Successfully banner uploaded", URL . 'DashBoardPanel', "success");
    }
    $title = "Feel Himalaya | Dashboard";
    $ActivePage = "Customize_Home";
    $PageBranch = "Banner_Page";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddBanner_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);

  }

  public function deleteBanner()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $BannerRow = $this->model->getBannerRow($id);
      $BannerImage = $BannerRow->banner_image;
      if($BannerImage){
        if(unlink("uploads/banner/".$BannerImage)){
          $this->model->deleteBanner($id);
          $flashmsg->info($BannerImage.'  has been deleted', URL . 'DashBoardPanel', 'INFO');
        }
        else{
          $flashmsg->info($BannerImage.' was not found', URL . 'DashBoardPanel', 'ERROR');
        }
      }
      else {
        $flashmsg->info('Data was not found', URL . 'DashBoardPanel', 'ERROR');
      }
    }
    else
    {
      header('location: ' . URL . 'DashBoardPanel');
    }

  }

}
