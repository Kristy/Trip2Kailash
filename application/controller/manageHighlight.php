<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ManageHighlight extends Controller
{
    function __construct()
    {
       parent::__construct();
       session_start();
        if(isset($_SESSION['logged_in']) == null)
        {
          header('location: ' . URL . 'Errorsite');
          exit;
        }
    }
    /***
    * PAGE: index
    * This method handles the error page that will be shown when a page is not found
    */
    public function index()
    {
      if(isset($_SESSION['logged_in']))
      {
        if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin"))
        {
          $page_purpose = "Update";
          $button_name = "Update";
          $form_url = URL . 'manageHighlight';
          $msg = "";
          $msgtype = "";
          //$file_name = "";
          $id = 1;
          $description = "";
          $exists = "False";
          $highlight = $this->model->gethighlight($id);

          if($highlight){
            $exists = "True";
            $description = $highlight->description;
          }
          $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
          $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
          if(isset($_POST["submit_theme"]))
          {
            $description = $_POST['description'];

            $description = stripslashes($description);

            $description= trim($description);

            if($exists == "True")
            {
              $this->model->edit_highlight_model($id, $description);
            }
            else
            {
              $this->model->add_highlight_model($id, $description);
            }
            $msg = "Updated Successfully.";
            $msgtype = "success";
          }

          if($msg != "" && $msgtype != ""){
            if($msgtype == "success"){
              $formmsg->info($msg, URL . 'managehighlight', $msgtype);
            }
            else{
              $formmsg->info($msg, '', $msgtype);
            }
          }
        $browser_title = "Trip2Kailash | Highlight ";
          $ActivePage = "Service";
          $PageBranch = "ManageHighlight";

          require APP . 'view/AdminPanel/template/header.php';
          require APP . 'view/AdminPanel/update_highlight_view.php';
          require APP . 'view/AdminPanel/template/footer.php';
          unset($_SESSION['flash_messages']);
        }
        else
        {
          require APP . 'view/error/index.php';
        }
      }
      else
      {
        title('location: ' . URL . 'cmslogin');
      }
    }


}
