<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ManageAssociatedWith extends Controller
{
	function __construct()
	{
	   parent::__construct();
	   session_start();
			if(isset($_SESSION['logged_in']) == null)
			{
				header('location: ' . URL . 'Errorsite');
				exit;
			}

	}
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
      if(isset($_SESSION['logged_in']))
      {
        if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin"))
  			{
				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    			$browser_title = "Trip2Kailash | Manage Associated";
    			$ActivePage = "Manage_Associated";
    			$PageBranch = "";

				$Associated_details = $this->model->getAssociated();

  				require APP . 'view/AdminPanel/template/header.php';
  				require APP . 'view/AdminPanel/ListAssociated_view.php';
  				require APP . 'view/AdminPanel/template/footer.php';
				unset($_SESSION['flash_messages']);
  			}
  			else
  			{
  				require APP . 'view/error/index.php';
  			}
		   }
		  else
		  {
        header('location: ' . URL . 'cmslogin');
		   }
    }

		public function addAssociatedWith()
		{
			$page_purpose = "Add";
			$button_name = "Save";
			$form_url = URL . 'manageAssociatedWith/addAssociatedWith';
			$msg = "";
			$msgtype = "";

			$id = "";
			$title = "";
			$url = "";
			$formmsg = new \Plasticbrain\FlashMessages\FlashMessages();

			if(isset($_POST["submit_Associateddetail"]))
			{
				$title = $_POST['title'];
				$url = $_POST['url'];

				$title = stripslashes($title);
				$url = stripslashes($url);

				$title = trim($title);
				$url= trim($url);
				$this->model->addAssociatedModel($title, $url);
				$msg = "Successfully added";
				$msgtype = "success";

			}
			if($msg != "" && $msgtype != ""){
				if($msgtype == "success"){
					$formmsg->info($msg, $form_url, $msgtype);
				}
				else{
					$formmsg->info($msg, '', $msgtype);
				}
			}
			$browser_title = "Trip2Kailash | Add Associated";
			$ActivePage = "Manage_Associated";
			$PageBranch = "Manage_Associated";

			require APP . 'view/AdminPanel/template/header.php';
			require APP . 'view/AdminPanel/UpdateAssociated_view.php';
			require APP . 'view/AdminPanel/template/footer.php';
			unset($_SESSION['flash_messages']);
		}

		public function deleteAssociated()
		{
			if (isset($_POST["SubmitDelete"]))
			{
				$id = $_POST['SubmitDelete'];

				$id = stripslashes($id);

				$id = trim($id);
				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

				$this->model->deleteAssociatedModel($id);

				$flashmsg->info('A data has been deleted', URL . 'manageAssociatedWith', 'INFO');

	    }
			else
			{
				header('location: ' . URL . 'manageAssociatedWith');
			}

		}

		public function editAssociated()
		{
			$page_purpose = "Edit";
			$button_name = "Update";
			$form_url = URL . 'manageAssociatedWith/editAssociated';
			$formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
			if (isset($_GET["submit_to_edit"]))
			{
				$id = $_GET['submit_to_edit'];
				$id = stripslashes($id);
				$id = trim($id);
				$AssociatedDetail = $this->model->getAssociatedDetailRow($id);
				$icons = $this->model->getallIcon();
				if($AssociatedDetail)
				{
					$label = $AssociatedDetail->label;
					$icon = $AssociatedDetail->icon;
					$detail = $AssociatedDetail->detail;
					$browser_title = "Trip2Kailash | Add Associated";
					$ActivePage = "Manage_Associated";
					$PageBranch = "";

					require APP . 'view/AdminPanel/template/header.php';
					require APP . 'view/AdminPanel/UpdateAssociated_view.php';
					require APP . 'view/AdminPanel/template/footer.php';
					unset($_SESSION['flash_messages']);
				}
				else{
					header('location: ' . URL . 'manageAssociatedWith');
				}
      }
			else if (isset($_POST["submit_Associateddetail"]))
			{
				$id = $_POST['submit_Associateddetail'];
				$label = $_POST['label'];
				$icon = $_POST['icon'];
				$detail = $_POST['detail'];

				$id = stripslashes($id);
				$label = stripslashes($label);
				$icon = stripslashes($icon);
				$detail = stripslashes($detail);

				$id = trim($id);
				$label = trim($label);
				$icon= trim($icon);
				$detail= trim($detail);

				$this->model->editAssociatedDetail($id, $label, $icon, $detail);

				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
				$flashmsg->info($label.' has been edited', URL . 'manageAssociatedWith', 'INFO');

      }
			else
			{
				header('location: ' . URL . 'manageAssociatedWith');
			}
		}



}
