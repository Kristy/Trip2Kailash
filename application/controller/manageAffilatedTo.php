<?php

class ManageAffilatedTo extends Controller
{
  function __construct()
	{
    parent::__construct();
	  session_start();
	  if(isset($_SESSION['logged_in']) == null)
		{
      header('location: ' . URL . 'Errorsite');
			exit;
		}
	}

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $Affilates = $this->model->getAffilated();
        $browser_title = "Trip2Kailash | List Affilated";
		$ActivePage = "Manage_Affilated";
		$PageBranch = "Manage_Affilated";

        require APP . 'view/AdminPanel/template/header.php';
		require APP . 'view/AdminPanel/ListAffilated_view.php';
		require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
	  }else{
        require APP . 'view/error/index.php';
	  }
	}
	else{
		header('location: ' . URL . 'cmslogin');
	}
  }

  public function addAffilatedTo()
  {
    $msg = "";
    $msgtype = "";
    //$file_name = "";
    $url = "";
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();

    if(isset($_POST["addAffilated"]))
    {
      $url = $_POST['url'];
      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
              $url = stripslashes($url);

              $file_name = $_FILES['userfile']['name'];
              $ext = new SplFileInfo($file_name);
              $file_name = 'affilated_'.time().'.'.$ext->getExtension();
              $file_tmp = $_FILES['userfile']['tmp_name'];
              $target_dir = "uploads/".$file_name;

              $url = trim($url);

                move_uploaded_file($file_tmp, $target_dir);
                $this->model->addAffilatedToModel($file_name, $url);
                $msg = "Successfully added";
                $msgtype = "success";

          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
                Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }

      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageAffilatedTo/addAffilatedTo', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Affilated";
    $ActivePage = "Manage_Affilated";
    $PageBranch = "Manage_Affilated";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddAffilatedTo_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteAffilated()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $Affilated = $this->model->getAffilatedRow($id);

      if($Affilated){
        unlink("uploads/".$Affilated->image);
        $this->model->deleteAffilatedModel($id);

        $flashmsg->info('A data has been deleted', URL . 'manageAffilatedTo', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageAffilatedTo', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageAffilatedTo');
    }

  }

  public function editAffilated()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $AffilatedDetail = $this->model->getAffilatedRow($id);
      if($AffilatedDetail)
      {
        $url = $AffilatedDetail->url;
        $browser_title = "Trip2Kailash | Edit Actiity";
        $ActivePage = "Manage_Affilated";
        $PageBranch = "Manage_Affilated";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditAffilated_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageAffilatedTo');
      }
    }
    else if (isset($_POST["editAffilated_submit"]))
    {
      $id = $_POST['editAffilated_submit'];
      $url = $_POST['url'];

      $id = stripslashes($id);
      $url = stripslashes($url);

      $id = trim($id);
      $url = trim($url);

      $titleCount = $this->model->getotherAffilatedHeading($url, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageAffilatedTo/editAffilated?submit_to_edit='.$id, 'error');
      }

      $this->model->editAffilatedModel($id, $url);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageAffilatedTo', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageAffilatedTo');
    }
  }

}
