<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Home extends Controller
{
	function __construct()
	{
	   parent::__construct();
	}
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
		$Page = "Home_Page";
    	$highlight = $this->model->gethighlight(1);
		$latest_activities = $this->model->get_latest_activities_model();
		$latest_kailashs = $this->model->getlatestKailashPackages();
		$popular_packages = $this->model->get_popular_service();
		$highlightedKailashp = $this->model->gethighlightedkailashpackageModel();
		$testimonials = $this->model->getallTestimonial();
		$banners = $this->model->getallBanners();

		require APP . 'view/website/templates/header.php';
		require APP . 'view/website/Home_view.php';
		require APP . 'view/website/templates/footer.php';

    }

}
