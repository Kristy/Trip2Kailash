<?php

class ManageKailashPackage extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $packages = $this->model->getallKailashPackages();

        $browser_title = "Trip2Kailash | Manage Kailash Package";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListKailashPackages_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addPackage()
  {
    $msg = "";
    $msgtype = "";
    //$file_name = "";
    $title = "";
    $tour_length_outline = "";
    $price = "";
    $physical_demand = "";
    $tour_length_outline = "";
    $summary = "";
    $description = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addPackage"]))
    {
      $title = $_POST['title'];
      $tour_length_outline = $_POST['tour_length_outline'];
      $best_time_to_visit = $_POST['best_time_to_visit'];
      $physical_demand = $_POST['physical_demand'];
      $price = $_POST['price'];
      $summary = $_POST['summary'];
      $description = $_POST['description'];

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
              $title = stripslashes($title);
              $tour_length_outline = stripslashes($tour_length_outline);
              $best_time_to_visit= stripslashes($best_time_to_visit);
              $physical_demand = stripslashes($physical_demand);
              $price = stripslashes($price);
              $summary = stripslashes($summary);
              $description= stripslashes($description);

              $file_name = $_FILES['userfile']['name'];
              $ext = new SplFileInfo($file_name);
              $file_name = 'kailash_package_'.time().'.'.$ext->getExtension();
              $file_tmp = $_FILES['userfile']['tmp_name'];
              $target_dir = "uploads/".$file_name;

              $title = trim($title);
              $tour_length_outline = trim($tour_length_outline);
              $best_time_to_visit= trim($best_time_to_visit);
              $physical_demand = trim($physical_demand);
              $price = trim($price);
              $summary = trim($summary);
              $description= trim($description);

              $count_heading = $this->model->getKailashPackageRowbyheading($title);

              if($count_heading->RowCount() > 0)
              {
                $msg = "Please enter unique heading.";
                $msgtype = "error";
              }
              else
              {
                move_uploaded_file($file_tmp, $target_dir);
                $this->model->addKailashPackageModel($title, $file_name, $tour_length_outline, $best_time_to_visit, $physical_demand, $price, $summary, $description);
                $msg = "Successfully added";
                $msgtype = "success";
              }
          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
                Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }

      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageKailashPackage/addPackage', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Package";
    $ActivePage = "Kailash_Page";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddKailashPackage_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deletePackage()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->getkailashPackageRow($id);
      if($package){
        unlink("uploads/".$package->image);
        $this->model->deletekailashpackageModel($id);

        $flashmsg->info('A data has been deleted', URL . 'manageKailashPackage', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageKailashPackage', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageKailashPackage');
    }

  }

  public function editPackage()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $package = $this->model->getkailashPackageRow($id);
      if($package)
      {
          $title = $package->title;
          $tour_length_outline = $package->tour_length_outline;
          $best_time_to_visit = $package->best_time_to_visit;
          $physical_demand = $package->physical_demand;
          $summary = $package->summary;
          $price = $package->price;
          $description = $package->description;
          $browser_title = "Trip2Kailash | Edit Package";
          $ActivePage = "Kailash_Page";
          $PageBranch = "Manage_Package";

          require APP . 'view/AdminPanel/template/header.php';
          require APP . 'view/AdminPanel/EditKailashPackage_view.php';
          require APP . 'view/AdminPanel/template/footer.php';
          unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageKailashPackage');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $tour_length_outline = $_POST['tour_length_outline'];
      $best_time_to_visit = $_POST['best_time_to_visit'];
      $physical_demand = $_POST['physical_demand'];
      $price = $_POST['price'];
      $summary = $_POST['summary'];
      $description = $_POST['description'];

      $id = stripslashes($id);
      $title = stripslashes($title);
      $tour_length_outline = stripslashes($tour_length_outline);
      $best_time_to_visit = stripslashes($best_time_to_visit);
      $physical_demand = stripslashes($physical_demand);
      $price = stripslashes($price);
      $summary = stripslashes($summary);
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $tour_length_outline = trim($tour_length_outline);
      $best_time_to_visit = trim($best_time_to_visit);
      $physical_demand = trim($physical_demand);
      $price = trim($price);
      $summary = trim($summary);
      $description = trim($description);

      $count_heading = $this->model->getotherKailashPackageRowheading($id, $title);

      if($count_heading > 0)
      {
        $formmsg->info('Title already Exists', URL . 'manageKailashPackage/editPackage?submit_to_edit='.$id, 'error');
      }

      $this->model->editKailashPackageModel($id, $title, $tour_length_outline, $best_time_to_visit, $physical_demand, $price, $summary, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageKailashPackage', 'INFO');

    }
    else if (isset($_POST["editImage_submit"]))
    {
      $id = $_POST['editImage_submit'];

      $id = stripslashes($id);

      $id = trim($id);

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'kailash_package_'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $itenary = $this->model->getkailashPackageRow($id);


              unlink("uploads/".$itenary->image);
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->editKPImage($id, $file_name);

              $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
              $flashmsg->info($title.' has been edited', URL . 'manageKailashPackage', 'INFO');

            }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }
      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    else
    {
      header('location: ' . URL . 'manageKailashPackage');
    }
  }

  public function highlightPackage()
  {
    if (isset($_POST["SubmitHighlight"]))
    {
      $id = $_POST['SubmitHighlight'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->getkailashPackageRow($id);
      if($package){
         $this->model->unhighlightkailashpackageModel();
        $this->model->highlightkailashpackageModel($id);

        $flashmsg->info('A data has been deleted', URL . 'manageKailashPackage', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageKailashPackage', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageKailashPackage');
    }

  }
    public function package_image()
    {
        if(isset($_SESSION['logged_in'])){
            if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
                $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
                $package_id = $_GET['package'];
                $browser_title = "Trip2Kailash | Manage Package Image";
                $ActivePage = "Kailash_Page";
                $PageBranch = "Manage_Package";

                $gallery_images = $this->model->getallKailashImages($package_id);

                require APP . 'view/AdminPanel/template/header.php';
                require APP . 'view/AdminPanel/ListKailashPackageImage_view.php';
                require APP . 'view/AdminPanel/template/footer.php';
                unset($_SESSION['flash_messages']);
            }
            else{
                require APP . 'view/error/index.php';
            }
        }
        else{
            header('location: ' . URL . 'cmslogin');
        }
    }
    public function addPackageImages()
    {
      $msg = "";
      $msgtype = "";
      $package_id = $_GET['package'];
      $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      if (!empty($_FILES))
      {
        $targetDir = "uploads/";
        $file_name = $_FILES['file']['name'];
        $ext = new SplFileInfo($file_name);
        $file_name = 'kailash_package_'.microtime().'.'.$ext->getExtension();
        $file_tmp = $_FILES['file']['tmp_name'];
        $target_dir = "uploads/".$file_name;

        if(move_uploaded_file($file_tmp, $target_dir)){
          $this->model->addKailashImages($file_name, $package_id);
        }

      }

      $browser_title = "Trip2Kailash | Add Gallery";
      $ActivePage = "Kailash_Page";
      $PageBranch = "Manage_Package";

      require APP . 'view/AdminPanel/template/header.php';
      require APP . 'view/AdminPanel/AddKailashImage_view.php';
      require APP . 'view/AdminPanel/template/footer.php';
      unset($_SESSION['flash_messages']);
    }

    public function deletePackageImage()
    {
      if (isset($_POST["SubmitDelete"]))
      {
        $id = $_POST['SubmitDelete'];
        $package_id = $_GET['package'];
        $id = stripslashes($id);

        $id = trim($id);
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        $GalleryRow = $this->model->getKailashImageRow($id);
        $Imagename = $GalleryRow->image_name;
        if($GalleryRow){
          if(unlink("uploads/".$GalleryRow->image_name)){
            $this->model->deleteKailashImage($GalleryRow->id);
            $flashmsg->info($Imagename.'  has been deleted', URL . 'manageKailashPackage/package_image?package='.$package_id, 'INFO');
          }
          else{
            $flashmsg->info($Imagename.' was not found', URL .  'manageKailashPackage/package_image?package='.$package_id, 'ERROR');
          }
        }
        else {
          $flashmsg->info('Data was not found', URL .  'manageKailashPackage/package_image?package='.$package_id, 'ERROR');
        }
      }
      else
      {
        header('location: ' . URL . 'manageKailashPackage/package_image?package='.$package_id);
      }

    }
}
