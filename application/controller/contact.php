<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Contact extends Controller
{
	function __construct()
	{
	   parent::__construct();
	}
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
	function clean_text($string)
 	{
 		$string = trim($string);
 		$string = stripslashes($string);
 		$string = htmlspecialchars($string);
 		return $string;
 	}

    public function index()
    {
		$Page = "Contact_Page";
		$message = "";
		if(isset($_POST["submit_query"])){
			$fname = $_POST['fname'];
			$lname = $_POST['lname'];
			$con_email = $_POST['con_email'];
			$con_phone = $_POST['con_phone'];
			$con_country = $_POST['con_country'];
			$con_message = $_POST['con_message'];

			$fname = $this->clean_text($fname);
			$lname = $this->clean_text($lname);
			$con_email = $this->clean_text($con_email);
			$con_phone = $this->clean_text($con_phone);
			$con_country = $this->clean_text($con_country);
			$con_message = $this->clean_text($con_message);

			$subject = $fname." is trying to contact";
			$host = 'mail.trip2kailash.com';
			// require APP . 'libs/class.phpmailer.php';
			// require APP . 'libs/class.smtp.php';
			//
			// $mail = new PHPMailer;
			// // $mail->isSMTP();
			// $mail->Host = 'mail.trip2kailash.com';
			// $mail->Port = 25;
			// // $mail->SMTPSecure = 'ssl';
			// $mail->SMTPAuth = true;
			// $mail->Username = 'system@trip2kailash.com';
			// $mail->Password = 'noreply@#167890';
			// $mail->addAddress($con_email);
			// $mail->Subject = $subject;
			// $mail->msgHTML($con_message);
			// if ($mail->send()) {
			try{
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// More headers
				$headers .= 'From: <system@trip2kailash.com>' . "\r\n";
				// $headers .= 'Cc: contact@trip2kailash.com' . "\r\n";
				$message = "";
				$message = '<html><body>';
				$message .= "<strong>Name:</strong> " . $fname . " " . $lname ."<br/>";
				$message .= '<strong>Email:</strong> ' . $con_email ."<br/>";
				$message .= '<strong>Phone Number:</strong> ' . $con_phone ."<br/>";
				$message .= '<strong>Country:</strong> ' . $con_country ."<br/>";
				$message .= '<strong>Message:</strong> <p>' . $con_message ."</p><br/>";
				$message .= '</body></html>';

				ini_set('SMTP', $host);
				if(mail('contact@trip2kailash.com', $subject, $message, $headers)) {
					$message = "Mail sent";
				}else{
					$message = "Error sending mail";
				}
			} catch(Exception $e){
				$message = "Error sending mail".$e->getMessage();
		    }
		}

        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/contact_view.php';
        require APP . 'view/website/templates/footer.php';

    }

}
