<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Team extends Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $Page = "Package_detail_Page";
        $nmembers = $this->model->getNationalTeamMembers();
        $imembers = $this->model->getInternationalTeamMembers();
        // $BannerLists = $this->model->getlatestBanner();
        //     $GloryActivities = $this->model->getallMinistryActivity();
        // $About_Our = "Goal";
        // $About_Summary = "Our goal is to preach the word of God, bring people under the saving knowledge of God through our action [action speak louder than voice] provide an education, to help families rise above survival mode and become self –sufficient.";
        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/team_view.php';
        require APP . 'view/website/templates/footer.php';

    }

}
