<?php

class ManagePackage extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $packages = $this->model->get_service_package_model();
        $browser_title = "Trip2Kailash | Manage Service Package";
        $ActivePage = "Service";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListPackages_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addPackage()
  {
    $msg = "";
    $msgtype = "";
    //$file_name = "";
    $title = "";
    $tour_length_outline = "";
    $best_time_to_visit = "";
    $physical_demand = "";
    $summary = "";
    $price = "";
    $activity = "";
    $description = "";
    $activities = $this->model->get_activities_model();
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addPackage"]))
    {
      $title = $_POST['title'];
      $tour_length_outline = $_POST['tour_length_outline'];
      $best_time_to_visit = $_POST['best_time_to_visit'];
      $physical_demand = $_POST['physical_demand'];
      $summary = $_POST['summary'];
      $price = $_POST['price'];
      $activity = $_POST['activity'];
      $description = $_POST['description'];

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
              $title = stripslashes($title);
              $tour_length_outline = stripslashes($tour_length_outline);
              $best_time_to_visit= stripslashes($best_time_to_visit);
              $physical_demand = stripslashes($physical_demand);
              $summary = stripslashes($summary);
              $price = stripslashes($price);
              $activity = stripslashes($activity);
              $description= stripslashes($description);

              $file_name = $_FILES['userfile']['name'];
              $ext = new SplFileInfo($file_name);
              $file_name = 'service_package_'.time().'.'.$ext->getExtension();
              $file_tmp = $_FILES['userfile']['tmp_name'];
              $target_dir = "uploads/".$file_name;

              $title = trim($title);
              $tour_length_outline = trim($tour_length_outline);
              $best_time_to_visit= trim($best_time_to_visit);
              $physical_demand = trim($physical_demand);
              $price = trim($price);
              $summary = trim($summary);
              $activity = trim($activity);
              $description= trim($description);

              $count_heading = $this->model->getPackageRowbyheading($title, $activity);

              if($count_heading->RowCount() > 0)
              {
                $msg = "Please enter unique heading.";
                $msgtype = "error";
              }
              else
              {
                move_uploaded_file($file_tmp, $target_dir);
                $this->model->addPackageModel($title, $file_name, $tour_length_outline, $best_time_to_visit, $physical_demand, $summary, $price, $activity, $description);
                $msg = "Successfully added";
                $msgtype = "success";
              }
          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
                Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }

      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'managePackage/addPackage', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Package";
    $ActivePage = "Service";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddPackage_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deletePackage()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->getPackageRow($id);
      if($package){
        unlink("uploads/".$package->image);
        $this->model->deletePackageModel($id);

        $flashmsg->info('A data has been deleted', URL . 'managePackage', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'managePackage', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'managePackage');
    }

  }

  public function editPackage()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $PackageDetail = $this->model->getPackageRow($id);
      $activities = $this->model->get_activities_model();

      if($PackageDetail)
      {
        $title = $PackageDetail->title;
        $activity = $PackageDetail->a_title;
        $tour_length_outline = $PackageDetail->tour_length_outline;
        $best_time_to_visit = $PackageDetail->best_time_to_visit;
        $physical_demand = $PackageDetail->physical_demand;
        $price = $PackageDetail->price;
        $summary = $PackageDetail->summary;
        $description = $PackageDetail->description;
        $show_on_home = $PackageDetail->show_on_home;
        $is_favourite = $PackageDetail->is_favourite;
        $added_on = $PackageDetail->added_on;
        $no_of_visit = $PackageDetail->no_of_visit;
        $recently_viewed = $PackageDetail->recently_viewed;

        $browser_title = "Trip2Kailash | Edit Package";
        $ActivePage = "Service";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditPackage_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'managePackage');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $tour_length_outline = $_POST['tour_length_outline'];
      $best_time_to_visit = $_POST['best_time_to_visit'];
      $physical_demand = $_POST['physical_demand'];
      $price = $_POST['price'];
      $summary = $_POST['summary'];
      $activity = $_POST['activity'];
      $description = $_POST['description'];

      $id = stripslashes($id);
      $title = stripslashes($title);
      $tour_length_outline = stripslashes($tour_length_outline);
      $best_time_to_visit= stripslashes($best_time_to_visit);
      $physical_demand = stripslashes($physical_demand);
      $summary = stripslashes($summary);
      $price = stripslashes($price);
      $activity = stripslashes($activity);
      $description= stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $tour_length_outline = trim($tour_length_outline);
      $best_time_to_visit= trim($best_time_to_visit);
      $physical_demand = trim($physical_demand);
      $summary = trim($summary);
      $price = trim($price);
      $activity = trim($activity);
      $description= trim($description);

      $titleCount = $this->model->getotherPackageRowheading($title, $id);


      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'managePackage/editPackage?submit_to_edit='.$id, 'error');
      }

      $this->model->editServicePackage($id, $title, $tour_length_outline, $best_time_to_visit, $physical_demand, $price, $summary, $activity, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'managePackage', 'INFO');

    }
    else if (isset($_POST["editImage_submit"]))
    {
      $id = $_POST['editImage_submit'];

      $id = stripslashes($id);

      $id = trim($id);

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'service_package_'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $itenary = $this->model->getPackageRow($id);


              unlink("uploads/".$itenary->image);
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->editServicepackageImage($id, $file_name);

              $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
              $flashmsg->info($title.' has been edited', URL . 'managePackage', 'INFO');

            }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }
      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    else
    {
      header('location: ' . URL . 'managePackage');
    }
  }

    public function package_image()
    {
      if(isset($_SESSION['logged_in'])){
          if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
              $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
              $package_id = $_GET['package'];
              $browser_title = "Trip2Kailash | Manage Package Image";
              $ActivePage = "Service";
              $PageBranch = "Manage_Package";

              $gallery_images = $this->model->getallPackageImages($package_id);

              require APP . 'view/AdminPanel/template/header.php';
              require APP . 'view/AdminPanel/ListPackageImage_view.php';
              require APP . 'view/AdminPanel/template/footer.php';
              unset($_SESSION['flash_messages']);
          }
          else{
              require APP . 'view/error/index.php';
          }
      }
      else{
          header('location: ' . URL . 'cmslogin');
      }
    }
    public function addPackageImages()
    {
    $msg = "";
    $msgtype = "";
    $package_id = $_GET['package'];
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (!empty($_FILES))
    {
      $targetDir = "uploads/";
      $file_name = $_FILES['file']['name'];
      $ext = new SplFileInfo($file_name);
      $file_name = 'service_package_'.microtime().'.'.$ext->getExtension();
      $file_tmp = $_FILES['file']['tmp_name'];
      $target_dir = "uploads/".$file_name;

      if(move_uploaded_file($file_tmp, $target_dir)){
        $this->model->addPackageImages($file_name, $package_id);
      }

    }

    $browser_title = "Trip2Kailash | Manage Package Image";
    $ActivePage = "Service";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddPackageImage_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
    }

    public function deletePackageImage()
    {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];
      $package_id = $_GET['package'];
      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $GalleryRow = $this->model->getPackageImageRow($id);
      $Imagename = $GalleryRow->image_name;
      if($GalleryRow){
        if(unlink("uploads/".$GalleryRow->image_name)){
          $this->model->deletePackageImage($GalleryRow->id);
          $flashmsg->info($Imagename.'  has been deleted', URL . 'managePackage/package_image?package='.$package_id, 'INFO');
        }
        else{
          $flashmsg->info($Imagename.' was not found', URL .  'managePackage/package_image?package='.$package_id, 'ERROR');
        }
      }
      else {
        $flashmsg->info('Data was not found', URL .  'managePackage/package_image?package='.$package_id, 'ERROR');
      }
    }
    else
    {
      header('location: ' . URL . 'managePackage/package_image?package='.$package_id);
    }

    }
}
