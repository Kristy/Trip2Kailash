<?php

class ManageKailashInclusionExclusion extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        $kp_id = $_GET['kp_id'];
        $requirements = $this->model->getKailashTourRequirements($kp_id);
        $browser_title = "Trip2Kailash | Kailash Inclusion Exclusion";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListKailashI_E_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addInclusionExclusion()
  {
    $msg = "";
    $msgtype = "";
    $kp_id = $_GET['kp_id'];
    $title = "";
    $description = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addRequirement"]))
    {
      $title = $_POST['title'];
      $description = $_POST['description'];

      $title = stripslashes($title);
      $description= stripslashes($description);

      $title = trim($title);
      $description= trim($description);

      $this->model->addKailashTourRequirementModel($title, $kp_id, $description);
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageKailashInclusionExclusion/addInclusionExclusion?kp_id='.$kp_id, $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Inclusion Exclusion";
    $ActivePage = "Kailash_Page";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddKailashTour_I_E_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteKInclusionExclusion()
  {
     $kp_id = $_GET['kp_id'];
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $this->model->deleteKailashTourRequirementModel($id);

      $flashmsg->info('A data has been deleted', URL . 'manageKailashInclusionExclusion?kp_id='.$kp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageKailashInclusionExclusion?kp_id='.$kp_id);
    }

  }

  public function editIncExc()
  {
    $kp_id = $_GET['kp_id'];
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $IE = $this->model->getKailashTourRequirementRow($id);

      if($IE)
      {
        $title = $IE->title;
        $description = $IE->description;
        $browser_title = "Trip2Kailash | Edit Package";
        $ActivePage = "Manage_TeamMember";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditKailashTour_I_E_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageKailashInclusionExclusion');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {

      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $description = $_POST['description'];

      
      $id = stripslashes($id);
      $title = stripslashes($title);
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $description = trim($description);


      $titleCount = $this->model->getotherKailashTourRequirementRowheading($title, $id);


      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageKailashInclusionExclusion/editIncExc?submit_to_edit='.$id, 'error');
      }

      $this->model->editKailashTourRequirement($id, $title, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageKailashInclusionExclusion?kp_id='.$kp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageKailashInclusionExclusion');
    }
  }
}
