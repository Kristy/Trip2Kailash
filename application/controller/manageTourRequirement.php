<?php

class ManageTourRequirement extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        $sp_id = $_GET['sp_id'];
        $requirements = $this->model->getTourRequirements($sp_id);
        $browser_title = "Trip2Kailash | Tour Requirement";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListTourRequirement_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addTourRequirement()
  {
    $msg = "";
    $msgtype = "";

    $title = "";
    $description = "";
    $sp_id = $_GET['sp_id'];
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addRequirement"]))
    {
      $title = $_POST['title'];
      $description = $_POST['description'];

      $title = stripslashes($title);
      $description= stripslashes($description);

      $title = trim($title);
      $description= trim($description);

      $this->model->addTourRequirementModel($title, $sp_id, $description);
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageTourRequirement/addTourRequirement?sp_id='.$sp_id, $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Requirement";
    $ActivePage = "Kailash";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddTourRequirement_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteTourRequirement()
  {
    $sp_id = $_GET['sp_id'];
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $this->model->deleteTourRequirementModel($id);

      $flashmsg->info('A data has been deleted', URL . 'manageTourRequirement?sp_id='.$sp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageTourRequirement?sp_id='.$sp_id);
    }

  }
}
