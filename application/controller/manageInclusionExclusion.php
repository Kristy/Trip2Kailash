<?php

class ManageInclusionExclusion extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        $sp_id = $_GET['sp_id'];
        $requirements = $this->model->getTourRequirements($sp_id);
        $browser_title = "Trip2Kailash | Inclusion Exclusion";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/List_I_E_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addInclusionExclusion()
  {
    $msg = "";
    $msgtype = "";

    $title = "";
    $description = "";
    $sp_id = $_GET['sp_id'];
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addRequirement"]))
    {
      $title = $_POST['title'];
      $description = $_POST['description'];

      $title = stripslashes($title);
      $description= stripslashes($description);

      $title = trim($title);
      $description= trim($description);

      $this->model->addTourRequirementModel($title, $sp_id, $description);
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageInclusionExclusion/addInclusionExclusion?sp_id='.$sp_id, $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Inclusion Exclusion";
    $ActivePage = "Kailash";
    $PageBranch = "Manage_Package";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/Add_I_E_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteInclusionExclusion()
  {
    $sp_id = $_GET['sp_id'];
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $this->model->deleteTourRequirementModel($id);

      $flashmsg->info('A data has been deleted', URL . 'manageInclusionExclusion?sp_id='.$sp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageInclusionExclusion?sp_id='.$sp_id);
    }  
  }

  public function editIncExc()
  {
    $sp_id = $_GET['sp_id'];
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $IE = $this->model->getTourRequirementRow($id);

      if($IE)
      {
        $title = $IE->title;
        $description = $IE->description;
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditKailashTour_I_E_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageInclusionExclusion');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {

      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $description = $_POST['description'];
      print_r($title);
      die;
      
      $id = stripslashes($id);
      $title = stripslashes($title);
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $description = trim($description);


      $titleCount = $this->model->getotherTourRequirementRowheading($title, $id);


      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageInclusionExclusion/editIncExc?submit_to_edit='.$id, 'error');
      }

      $this->model->editTourRequirement($id, $title, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageInclusionExclusion?sp_id='.$sp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageInclusionExclusion');
    }
  }
}
