<?php

class ManageBanner extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $banners = $this->model->getallBanners();
        $browser_title = "Trip2Kailash | Manage Banner";
        $ActivePage = "Manage_Banner";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListBanner_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addBanner()
  {
    $msg = "";
    $msgtype = "";
    //$file_name = "";
    $title = "";
    $highlight = "";
    $url = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addBanner"]))
    {
      $title = $_POST['title'];
      $highlight = $_POST['highlight'];
      $url = $_POST['url'];

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
              $title = stripslashes($title);
              $highlight= stripslashes($highlight);
              $url= stripslashes($url);

              $file_name = $_FILES['userfile']['name'];
              $ext = new SplFileInfo($file_name);
              $file_name = 'banner_'.time().'.'.$ext->getExtension();
              $file_tmp = $_FILES['userfile']['tmp_name'];
              $target_dir = "uploads/".$file_name;

              $title = trim($title);
              $highlight= trim($highlight);
              $url= trim($url);

              $count_heading = $this->model->getBannerRowbyheading($title);

              if($count_heading->RowCount() > 0)
              {
                $msg = "Please enter unique heading.";
                $msgtype = "error";
              }
              else
              {
                move_uploaded_file($file_tmp, $target_dir);
                $this->model->add_banner($title, $file_name, $highlight, $url);
                $msg = "Successfully added";
                $msgtype = "success";
                
              }
          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
                Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }

      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageBanner/addBanner', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Banner";
    $ActivePage = "Manage_Banner";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddBanner_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteBanner()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->get_bannerRow($id);
      if($package){
        unlink("uploads/".$package->image);
        $this->model->deleteBanner($id);

        $flashmsg->info('A data has been deleted', URL . 'manageBanner', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageBanner', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageBanner');
    }

  }

  public function editBanner()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $Banner = $this->model->get_bannerRow($id);

      if($Banner)
      {
        $title = $Banner->title;
        $highlight = $Banner->highlight;
        $url = $Banner->url;
        $browser_title = "Trip2Kailash | Edit Banner";
        $ActivePage = "Manage_Banner";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditBanner_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageBanner');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $highlight = $_POST['highlight'];
      $url = $_POST['url'];

      $id = stripslashes($id);
      $title = stripslashes($title);
      $highlight= stripslashes($highlight);
      $url= stripslashes($url);

      $id = trim($id);
      $title = trim($title);
      $highlight= trim($highlight);
      $url= trim($url);

      $titleCount = $this->model->getotherBannerRowheading($id, $title);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageBanner/editBanner?submit_to_edit='.$id, 'error');
      }

      $this->model->edit_banner($id, $title, $highlight, $url);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageBanner', 'INFO');

    }
    else if (isset($_POST["editImage_submit"]))
    {
      $id = $_POST['editImage_submit'];

      $id = stripslashes($id);

      $id = trim($id);

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'banner_'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $banner = $this->model->get_bannerRow($id);

              
              unlink("uploads/".$banner->image);
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->updateBannerImage($id, $file_name);
                
              $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
              $flashmsg->info($title.' has been edited', URL . 'manageBanner', 'INFO');
              
            }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }
      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    else
    {
      header('location: ' . URL . 'manageBanner');
    }
  }
}
