<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Manage_essential_info extends Controller
{
		function __construct()
		{
		   parent::__construct();
		   session_start();
				if(isset($_SESSION['logged_in']) == null)
				{
					header('location: ' . URL . 'Errorsite');
					exit;
				}
		}
    /***
    * PAGE: index
    * This method handles the error page that will be shown when a page is not found
    */
    public function index()
    {
    	if(isset($_SESSION['logged_in'])){
      		if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
	        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

	        $infos = $this->model->getallEssentialInfos();
	        $browser_title = "Trip2Kailash | List Activities";
			$ActivePage = "Manage_Essential_Info";

	        require APP . 'view/AdminPanel/template/header.php';
			require APP . 'view/AdminPanel/ListEssentialInfo_view.php';
			require APP . 'view/AdminPanel/template/footer.php';
	        unset($_SESSION['flash_messages']);
	  	}else{
        	require APP . 'view/error/index.php';
	  	}
	}
		else{
			header('location: ' . URL . 'cmslogin');
		}
    }

    public function addEssentialInfo()
  {
    $msg = "";
    $msgtype = "";
    //$file_name = "";
    $title = "";
    $description = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addinfo"]))
    {
      $title = $_POST['title'];
      $description = $_POST['description'];

	  $title = stripslashes($title);
	  $description= stripslashes($description);

	  $title = trim($title);
	  $description= trim($description);

	  $count_heading = $this->model->getEssentialInfoRowbyheading($title);

	  if($count_heading->RowCount() > 0)
	  {
		$msg = "Please enter unique heading.";
		$msgtype = "error";
	  }
	  else
	  {
		  $file_name = "";
		$this->model->addEssentialInfo($title, $file_name, $description);
		$msg = "Successfully added";
		$msgtype = "success";
	  }

    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manage_essential_info/addEssentialInfo', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Essential Info";
    $ActivePage = "Manage_Essential_Info";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/addEssentialInfo_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

	public function deleteInfo()
  	{
	    if (isset($_POST["SubmitDelete"]))
	    {
	      $id = $_POST['SubmitDelete'];

	      $id = stripslashes($id);

	      $id = trim($id);
	      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
	      $package = $this->model->getEssentialInfoRow($id);
	      if($package){
	        unlink("uploads/".$package->image);
	        $this->model->deleteEssentialInfo($id);

	        $flashmsg->info('A data has been deleted', URL . 'manage_essential_info', 'INFO');
	      }
	      $flashmsg->info('A data was not deleted', URL . 'manage_essential_info', 'ERROR');
	    }
	    else
	    {
	      header('location: ' . URL . 'manage_essential_info');
	    }

	  }

  public function editInfo()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();

    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $info = $this->model->getEssentialInfoRow($id);
      if($info)
      {
        $title = $info->title;
        $description = $info->description;
        $browser_title = "Trip2Kailash | Edit Essential Info";
        $ActivePage = "Manage_Essential_Info";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditEssentialInfo_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manage_essential_info');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $description = $_POST['description'];
      $id = stripslashes($id);
      $title = stripslashes($title);
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $description = trim($description);

      $titleCount = $this->model->getotherEssentialInfoRowheading($title, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manage_essential_info/editGuide?submit_to_edit='.$id, 'error');
      }

      $this->model->editEssentialInfo($id, $title, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manage_essential_info', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manage_essential_info');
    }
  }

}
