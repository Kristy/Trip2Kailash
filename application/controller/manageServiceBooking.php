<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ManageServiceBooking extends Controller
{
		function __construct()
		{
		   parent::__construct();
		   session_start();
				if(isset($_SESSION['logged_in']) == null)
				{
					header('location: ' . URL . 'Errorsite');
					exit;
				}
		}
    /***
    * PAGE: index
    * This method handles the error page that will be shown when a page is not found
    */
    public function index()
    {
    	if(isset($_SESSION['logged_in'])){
      		if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
	        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

	        $bookings = $this->model->getallServiceBooking();
	        $browser_title = "Trip2Kailash | List Kailash Booking";
			    $ActivePage = "Booking";
        		$PageBranch = "Service_booking";

	        require APP . 'view/AdminPanel/template/header.php';
			require APP . 'view/AdminPanel/ListServiceBooking_view.php';
			require APP . 'view/AdminPanel/template/footer.php';
	        unset($_SESSION['flash_messages']);
	  	}else{
        	require APP . 'view/error/index.php';
	  	}
	}
		else{
			header('location: ' . URL . 'cmslogin');
		}
  }

	public function deleteInfo()
  	{
	    if (isset($_POST["SubmitDelete"]))
	    {
	      $id = $_POST['SubmitDelete'];

	      $id = stripslashes($id);

	      $id = trim($id);
	      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
	      $package = $this->model->getEssentialInfoRow($id);
	      if($package){
	        unlink("uploads/".$package->image);
	        $this->model->deleteEssentialInfo($id);

	        $flashmsg->info('A data has been deleted', URL . 'manageServiceBooking', 'INFO');
	      }
	      $flashmsg->info('A data was not deleted', URL . 'manageServiceBooking', 'ERROR');
	    }
	    else
	    {
	      header('location: ' . URL . 'manageServiceBooking');
	    }

	  }

	  public function detailBooking()
  	{
	    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
	    if (isset($_GET["submit_to_detail"]))
	    {
	      $id = $_GET['submit_to_detail'];
	      $id = stripslashes($id);
	      $id = trim($id);
	      $Booking = $this->model->getSBookingRow($id);
	      if($Booking)
	      {
	        $full_name = $Booking->full_name;
	        $gender = $Booking->gender;
	        $nationality = $Booking->nationality;
	        $dob = $Booking->dob;
	        $passport_no = $Booking->passport_no;
	        $date_of_issue = $Booking->date_of_issue;
	        $place_of_issue = $Booking->place_of_issue;
	        $date_of_expiry = $Booking->date_of_expiry;
	        $permanent_address = $Booking->permanent_address;
	        $profession = $Booking->profession;
	        $mobile_no = $Booking->mobile_no;
	        $telephone = $Booking->telephone;
	        $email_id = $Booking->email_id;
	        $e_name = $Booking->e_name;
	        $e_relation = $Booking->e_relation;
	        $e_telephone = $Booking->e_telephone;
	        $e_email_id = $Booking->e_email_id;
	        $preffered_date = $Booking->preffered_date;
	        // $adv_deposit = $Booking->adv_deposit;

	        $browser_title = "Trip2Kailash | Detail Service Package Booking";
	        $ActivePage = "Booking";
        	$PageBranch = "Service_booking";

	        require APP . 'view/AdminPanel/template/header.php';
	        require APP . 'view/AdminPanel/listServiceBookingDetails_view.php';
	        require APP . 'view/AdminPanel/template/footer.php';
	        unset($_SESSION['flash_messages']);
	      }
	      else{
	        header('location: ' . URL . 'manageServiceBooking');
	      }
	    }
	    else
	    {
	      header('location: ' . URL . 'manageServiceBooking');
	    }
	}
}
