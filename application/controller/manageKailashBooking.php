<?php

class ManageKailashBooking extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
        $kp_id = $_GET['kp_id'];
        $bookings = $this->model->getKailashBooking($kp_id);
        $browser_title = "Trip2Kailash | Manage Kailash Booking";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Booking";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListKailashBooking_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addBooking()
  {
    $msg = "";
    $msgtype = "";
    $kp_id = $_GET['kp_id'];
    $from = "";
    $to = "";
    $status = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addBooking"]))
    {
      $from = $_POST['from'];
      $to = $_POST['to'];
      $status = $_POST['status'];

      $from = stripslashes($from);
      $status = stripslashes($status);
      $to= stripslashes($to);

      $from = trim($from);
      $status = trim($status);
      $to= trim($to);

      $this->model->addBooking($kp_id, $from, $to, $status);
      
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageKailashBooking/addBooking?kp_id='.$kp_id, $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Booking";
    $ActivePage = "Kailash_Page";
    $PageBranch = "Manage_Booking";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddKailashBooking_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteBooking()
  {
     $kp_id = $_GET['kp_id'];
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $this->model->deletekailashBookingModel($id);

      $flashmsg->info('A data has been deleted', URL . 'manageKailashBooking?kp_id='.$kp_id, 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageKailashBooking?kp_id='.$kp_id);
    }

  }

  public function editBooking()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $Booking = $this->model->getKailashBookingRow($id);
      if($Booking)
      {
        $from_date = $Booking->from_date;
        $to_date = $Booking->to_date;
        $status = $Booking->status;

        $browser_title = "Trip2Kailash | Edit Kailash Booking";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Booking";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditKailashBooking_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageKailashBooking');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $from_date = $_POST['from'];
      $to_date = $_POST['to'];
      $status = $_POST['status'];

      $id = stripslashes($id);
      $from_date = stripslashes($from_date);
      $to_date = stripslashes($to_date);
      $status = stripslashes($status);

      $id = trim($id);
      $from_date = trim($from_date);
      $to_date = trim($to_date);
      $status = trim($status);
      print_r($from_date);
      die;

      // $titleCount = $this->model->getotherKPBookingRowheading($title, $id);

      // if($titleCount > 0)
      // {
      //   $formmsg->info('Heading already Exists', URL . 'manageKailashBooking/editFAQ?submit_to_edit='.$id, 'error');
      // }

      $this->model->editKailashBooking($id, $from_date, $to_date, $status);


      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageKailashBooking', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageKailashBooking');
    }
  }
}
