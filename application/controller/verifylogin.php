<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Verifylogin extends Controller
{

	function __construct()
	{
	   parent::__construct();
	   session_start();
	}

    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
		$UserName=$_POST['UserName'];
		$Password=$_POST['Password'];

		$UserName = stripslashes($UserName);
		$Password = stripslashes($Password);

		$UserName = trim($UserName);
		$Password = trim($Password);

		$result = $this->model->Login($UserName, $Password);

			if($result)
			{
				$this->model->resetAttempts($UserName);
				$sess_array = array();
				foreach($result as $row)
				{
					$sess_array['UserID'] = $row->UserID;
					$sess_array['UserName'] = $row->UserName;
					$sess_array['UserFirstName'] = $row->UserFirstName;
					$sess_array['UserLastName'] = $row->UserLastName;
					$sess_array['UserRole'] = $row->UserRole;

					$_SESSION['logged_in'] = $sess_array;
				}

				if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin"))
				{
					if (!session_id()) @session_start();
					$msg = new \Plasticbrain\FlashMessages\FlashMessages();
					// $msg->info('Welcome to Panel', URL . 'DashBoardPanel', 'success');
					$msg->info('Welcome to Panel', URL . 'manage_kailash_detail', 'success');
					// $msg->sticky('This is "success" sticky message', '', $msg::INFO);
					// header('location: ' . URL . 'DashBoardPanel');
				}

				else
				{
					require APP . 'view/error/index.php';
				}
		   }

		   else
		   {
				$this->model->updateLoginFailed($UserName);
				$LoginAs = $this->model->getLoginAttempts($UserName);
				if($LoginAs)
				{
					if($LoginAs->LoginAttempts > 3)
					{
						require APP . 'view/error/LoginError.php';
					}

					else
					{
						header('location: ' . URL . 'cmslogin?login=error');
						//require APP . 'view/error/index.php';
					}
				}
				else
				{
					header('location: ' . URL . 'cmslogin?login=error');
				}
		   }
    }

}
