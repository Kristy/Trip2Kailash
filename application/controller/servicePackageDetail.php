<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ServicePackageDetail extends Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $Page = "Package_detail_Page";

        $package = $_GET['package'];

        $package_id = $_GET['package'];
        $this->model->update_package_id($package_id);
        $package = $this->model->getPackageRow($package_id);
        $itenaries = $this->model->getServiceItenary($package_id);
		$I_Es = $this->model->getTourRequirements($package_id);
        $recent_packages = $this->model->get_recently_viewed_service();
        $PackageImages = $this->model->getallPackageImages($package_id);

        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/package_detail_view.php';
        require APP . 'view/website/templates/footer.php';

    }

}
