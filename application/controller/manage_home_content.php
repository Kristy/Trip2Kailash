<?php

class Manage_home_content extends Controller
{
  function __construct()
	{
    parent::__construct();
	  session_start();
	  if(isset($_SESSION['logged_in']) == null)
		{
      header('location: ' . URL . 'Errorsite');
			exit;
		}
	}

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
          $home_content = $this->model->getHomeContent(1);
        if($home_content){
          $description = $home_content->description;
          $status = True;
        }
        else{
          $description = "";
          $status = False;
        }
        if(isset($_POST["updateHomeContent"]))
  			{
  				$description = $_POST['description'];
  				$description = stripslashes($description);
  				$description= trim($description);

          $this->model->updateHomeContent($description, $status);
  				$msg = "Successfully Updated";
  				$msgtype = "success";
          $formmsg->info($msg, URL . 'manageHomeContent', $msgtype);
          exit;
  			}
        $title = "Glory | Home Content";
				$ActivePage = "Customize_Home";
				$PageBranch = "Home_Content";

        require APP . 'view/AdminPanel/template/header.php';
				require APP . 'view/AdminPanel/AddHomeContent_view.php';
				require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
			}
			else{
        require APP . 'view/error/index.php';
			}
		}
		else{
			header('location: ' . URL . 'cmslogin');
		}
  }
}
