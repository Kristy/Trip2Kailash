<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ManageContact extends Controller
{
	function __construct()
	{
	   parent::__construct();
	   session_start();
			if(isset($_SESSION['logged_in']) == null)
			{
				header('location: ' . URL . 'Errorsite');
				exit;
			}

	}
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
      if(isset($_SESSION['logged_in']))
      {
        if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin"))
  			{
					$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    			$title = "Feel Himalaya | Manage Contact";
    			$ActivePage = "Manage_Contact";
    			$PageBranch = "";

					$contact_details = $this->model->getallContact();

  				require APP . 'view/AdminPanel/template/header.php';
  				require APP . 'view/AdminPanel/ListContact_view.php';
  				require APP . 'view/AdminPanel/template/footer.php';
					unset($_SESSION['flash_messages']);
  			}
  			else
  			{
  				require APP . 'view/error/index.php';
  			}
		   }
		  else
		  {
        header('location: ' . URL . 'cmslogin');
		   }
    }

		public function addContact()
		{
			$page_purpose = "Add";
			$button_name = "Save";
			$form_url = URL . 'manageContact/addContact';
			$msg = "";
			$msgtype = "";

			$id = "";
			$label = "";
			$icon = "";
			$detail = "";
			$formmsg = new \Plasticbrain\FlashMessages\FlashMessages();

			$icons = $this->model->getallIcon();

			if(isset($_POST["submit_contactdetail"]))
			{
				$label = $_POST['label'];
				$icon = $_POST['icon'];
				$detail = $_POST['detail'];

				$label = stripslashes($label);
				$icon = stripslashes($icon);
				$detail = stripslashes($detail);

				$label = trim($label);
				$icon= trim($icon);
				$detail= trim($detail);
				$this->model->addContactDetail($label, $icon, $detail);
				$msg = "Successfully added";
				$msgtype = "success";

			}
			if($msg != "" && $msgtype != ""){
				if($msgtype == "success"){
					$formmsg->info($msg, $form_url, $msgtype);
				}
				else{
					$formmsg->info($msg, '', $msgtype);
				}
			}
			$title = "Feel Himalaya | Add Contact";
			$ActivePage = "Customize_Content";
			$PageBranch = "Manage_Contact";

			require APP . 'view/AdminPanel/template/header.php';
			require APP . 'view/AdminPanel/UpdateContact_view.php';
			require APP . 'view/AdminPanel/template/footer.php';
			unset($_SESSION['flash_messages']);
		}

		public function deleteContact()
		{
			if (isset($_POST["SubmitDelete"]))
			{
				$id = $_POST['SubmitDelete'];

				$id = stripslashes($id);

				$id = trim($id);
				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

				$this->model->deleteContact($id);

				$flashmsg->info('A data has been deleted', URL . 'manageContact', 'INFO');

	    }
			else
			{
				header('location: ' . URL . 'manageContact');
			}

		}

		public function editContact()
		{
			$page_purpose = "Edit";
			$button_name = "Update";
			$form_url = URL . 'manageContact/editContact';
			$formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
			if (isset($_GET["submit_to_edit"]))
			{
				$id = $_GET['submit_to_edit'];
				$id = stripslashes($id);
				$id = trim($id);
				$ContactDetail = $this->model->getContactDetailRow($id);
				$icons = $this->model->getallIcon();
				if($ContactDetail)
				{
					$label = $ContactDetail->label;
					$icon = $ContactDetail->icon;
					$detail = $ContactDetail->detail;
					$title = "Feel Himalaya | Add Contact";
					$ActivePage = "Contact";
					$PageBranch = "";

					require APP . 'view/AdminPanel/template/header.php';
					require APP . 'view/AdminPanel/UpdateContact_view.php';
					require APP . 'view/AdminPanel/template/footer.php';
					unset($_SESSION['flash_messages']);
				}
				else{
					header('location: ' . URL . 'manageContact');
				}
      }
			else if (isset($_POST["submit_contactdetail"]))
			{
				$id = $_POST['submit_contactdetail'];
				$label = $_POST['label'];
				$icon = $_POST['icon'];
				$detail = $_POST['detail'];

				$id = stripslashes($id);
				$label = stripslashes($label);
				$icon = stripslashes($icon);
				$detail = stripslashes($detail);

				$id = trim($id);
				$label = trim($label);
				$icon= trim($icon);
				$detail= trim($detail);

				$this->model->editContactDetail($id, $label, $icon, $detail);

				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
				$flashmsg->info($label.' has been edited', URL . 'manageContact', 'INFO');

      }
			else
			{
				header('location: ' . URL . 'manageContact');
			}
		}

		public function showinHeader()
		{
			if (isset($_POST["SubmitInHeader"]))
			{
				$id = $_POST['SubmitInHeader'];
				$id = stripslashes($id);
				$id = trim($id);
				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

				$this->model->changeshowInHeader($id, True);

				$flashmsg->info('Successfully showed in header', URL . 'manageContact', 'INFO');

	    }
			else if (isset($_POST["RemoveInHeader"]))
			{
				$id = $_POST['RemoveInHeader'];
				$id = stripslashes($id);
				$id = trim($id);
				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

				$this->model->changeshowInHeader($id, False);

				$flashmsg->info('Successfully removed from header', URL . 'manageContact', 'INFO');

	    }
			else
			{
				header('location: ' . URL . 'manageContact');
			}

		}

		public function showinFooter()
		{
			if (isset($_POST["SubmitInFooter"]))
			{
				$id = $_POST['SubmitInFooter'];
				$id = stripslashes($id);
				$id = trim($id);
				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

				$this->model->changeshowInFooter($id, True);

				$flashmsg->info('Successfully showed in footer', URL . 'manageContact', 'INFO');

	    }
			else if (isset($_POST["RemoveInFooter"]))
			{
				$id = $_POST['RemoveInFooter'];
				$id = stripslashes($id);
				$id = trim($id);
				$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

				$this->model->changeshowInFooter($id, False);

				$flashmsg->info('Successfully removed from footer', URL . 'manageContact', 'INFO');

	    }
			else
			{
				header('location: ' . URL . 'manageContact');
			}

		}

}
