<?php

class ManageKailashGuide extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $guides = $this->model->getallKailashGuides();
        $browser_title = "Trip2Kailash | Manage Kailash Guide";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Guide";


        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListKailashGuide_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addGuide()
  {
    $msg = "";
    $msgtype = "";
    //$file_name = "";
    $title = "";
    $description = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addGuide"]))
    {
      $title = $_POST['title'];
      $description = $_POST['description'];

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
              $title = stripslashes($title);
              $description= stripslashes($description);

              $file_name = $_FILES['userfile']['name'];
              $ext = new SplFileInfo($file_name);
              $file_name = 'kailash_guide_'.time().'.'.$ext->getExtension();
              $file_tmp = $_FILES['userfile']['tmp_name'];
              $target_dir = "uploads/".$file_name;

              $title = trim($title);
              $description= trim($description);

              $count_heading = $this->model->getKailashGuideRowbyheading($title);

              if($count_heading->RowCount() > 0)
              {
                $msg = "Please enter unique heading.";
                $msgtype = "error";
              }
              else
              {
                move_uploaded_file($file_tmp, $target_dir);
                $this->model->addKailashGuidel($title, $file_name, $description);
                $msg = "Successfully added";
                $msgtype = "success";
              }
          }
          else
          {
            // if the file is not less than the maximum allowed, print an error
            $msgtype = "error";
            $msg='File exceeds the Maximum File limit<br/>
                Maximum File limit is '.$maxsize.' bytes<br/>
            File '.$_FILES['userfile']['name'].' is '.$_FILES['userfile']['size'].
            ' bytes';
          }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }

      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageKailashGuide/addGuide', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add Package";
    $ActivePage = "Kailash_Page";
    $PageBranch = "Manage_Guide";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddKailashGuide_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deletePackage()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->getkailashGuideRow($id);
      if($package){
        unlink("uploads/".$package->image);
        $this->model->deletekailashGuide($id);

        $flashmsg->info('A data has been deleted', URL . 'manageKailashGuide', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageKailashGuide', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageKailashGuide');
    }

  }

  public function editGuide()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $guide = $this->model->getkailashGuideRow($id);
      if($guide)
      {
        $title = $guide->title;
        $description = $guide->description;
        $browser_title = "Trip2Kailash | Edit Guide";
        $ActivePage = "Kailash_Page";
        $PageBranch = "Manage_Package";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditKailashGuide_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageKailashGuide');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $description = $_POST['description'];

      $id = stripslashes($id);
      $title = stripslashes($title);
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $description = trim($description);
      $titleCount = $this->model->getotherKailashGuideRowheading($title, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageKailashGuide/editGuide?submit_to_edit='.$id, 'error');
      }

      $this->model->editKailashGuide($id, $title, $description);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageKailashGuide', 'INFO');

    }
    else if (isset($_POST["editImage_submit"]))
    {
      $id = $_POST['editImage_submit'];

      $id = stripslashes($id);

      $id = trim($id);

      $maxsize = 10000000; //set to approx 10 MB

      if($_FILES['userfile']['error']==UPLOAD_ERR_OK) {
        //check whether file is uploaded with HTTP POST
        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
          //checks size of uploaded image on server side
          if( $_FILES['userfile']['size'] < $maxsize)
          {
            $file_name = $_FILES['userfile']['name'];
            $ext = new SplFileInfo($file_name);
            $file_name = 'kailash_guide_'.time().'.'.$ext->getExtension();
            $file_tmp = $_FILES['userfile']['tmp_name'];
            $target_dir = "uploads/".$file_name;

            $itenary = $this->model->editKailashGuideImage($id);


              unlink("uploads/".$itenary->image);
              move_uploaded_file($file_tmp, $target_dir);
              $this->model->editItenary($id, $file_name);

              $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
              $flashmsg->info($title.' has been edited', URL . 'manageKailashGuide', 'INFO');

            }
        }
        else
        {
          $msgtype = "error";
          $msg="File not uploaded successfully.";
        }
      }
      else
      {
        $msgtype = "error";
        $error_code = $_FILES['userfile']['error'];
        if($error_code == UPLOAD_ERR_INI_SIZE)
        {
          $msg =  'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        }
        else if($error_code == UPLOAD_ERR_FORM_SIZE)
        {
          $msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        }
        else if($error_code == UPLOAD_ERR_PARTIAL)
        {
          $msg = 'The uploaded file was only partially uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_FILE)
        {
          $msg = 'No file was uploaded';
        }
        else if($error_code == UPLOAD_ERR_NO_TMP_DIR)
        {
          $msg = 'Missing a temporary folder';
        }
        else if($error_code == UPLOAD_ERR_CANT_WRITE)
        {
          $msg = 'Failed to write file to disk';
        }
        else if($error_code == UPLOAD_ERR_EXTENSION)
        {
          $msg = 'File upload stopped by extension';
        }
        else
        {
          $msg = 'Unknown upload error';
        }
      }
    }
    else
    {
      header('location: ' . URL . 'manageKailashGuide');
    }
  }
}
