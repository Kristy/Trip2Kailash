<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Package extends Controller
{
	function __construct()
	{
	   parent::__construct();
	}
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $Page = "Service_Package_Page";

        $activity_id = $_GET['activity'];
		$activity = $this->model->getActivityRow($activity_id);
        $packages = $this->model->get_activity_packages($activity_id);
		$recent_packages = $this->model->get_recently_viewed_service();
        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/package_view.php';
        require APP . 'view/website/templates/footer.php';

    }

}
