<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Cmslogin extends Controller
{
	function __construct()
	{
	   parent::__construct();
	   session_start();
	}
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
  public function index()
  {
      // load views
			$title = "Login";
      require APP . 'view/login_view.php';
  }

	function logout()
	{
		if(isset($_SESSION['logged_in']))
		{
			$newdata = $_SESSION['logged_in'];
			foreach ($newdata as $key)
			{
				unset($key);
			}
		}
		session_destroy();
		header('location: ' . URL . 'cmslogin');
	}
}
