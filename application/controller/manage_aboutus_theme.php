<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Manage_aboutus_theme extends Controller
{
		function __construct()
		{
		   parent::__construct();
		   session_start();
				if(isset($_SESSION['logged_in']) == null)
				{
					header('location: ' . URL . 'Errorsite');
					exit;
				}
		}
    /***
    * PAGE: index
    * This method handles the error page that will be shown when a page is not found
    */
    public function index()
    {
      if(isset($_SESSION['logged_in']))
      {
        if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin"))
  			{
					$page_purpose = "Update";
					$button_name = "Update";
					$form_url = URL . 'manage_aboutus_theme';
					$msg = "";
					$msgtype = "";
					//$file_name = "";
					$id = 1;
					$title = "";
					$quote = "";
					$left_title = "";
					$left_description = "";
					$right_title = "";
					$right_description  = "";

					$image_theme = "";
					$exists = "False";
					$about_us_theme = $this->model->get_about_theme_model($id);
					if($about_us_theme){
						$exists = "True";
						$title = $about_us_theme->title;
						$quote = $about_us_theme->quote;
						$left_title = $about_us_theme->left_title;
						$left_description = $about_us_theme->left_description;
						$right_title = $about_us_theme->right_title;
						$right_description = $about_us_theme->right_description;
						if($about_us_theme->image){
							$image_theme = 'uploads/'.$about_us_theme->image;
						}
					}
					$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
					$formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
					if(isset($_POST["submit_theme"]))
					{
						$title = $_POST['title'];
						$quote = $_POST['quote'];
						$left_title = $_POST['left_title'];
						$left_description = $_POST['left_description'];
						$right_title = $_POST['right_title'];
						$right_description = $_POST['right_description'];

						$title = stripslashes($title);
						$quote = stripslashes($quote);
						$left_title = stripslashes($left_title);
						$left_description = stripslashes($left_description);
						$right_title = stripslashes($right_title);
						$right_description = stripslashes($right_description);

						$title = trim($title);
						$quote= trim($quote);
						$left_title = trim($left_title);
						$left_description= trim($left_description);
						$right_title = trim($right_title);
						$right_description= trim($right_description);

						if($exists == "True")
						{
							$this->model->edit_about_theme_model($id, $title, $quote, $left_title, $left_description, $right_title, $right_description);
						}
						else
						{
							$this->model->add_about_theme_model($id, $title, $quote, $left_title, $left_description, $right_title, $right_description);
						}
						$msg = "Updated Successfully.";
						$msgtype = "success";
					}

					if(isset($_POST["submit_theme_image"]))
					{
						$targetDir = "uploads/";
				        $file_name = $_FILES['file']['name'];
				        $ext = new SplFileInfo($file_name);
				        $file_name = 'about_theme_'.microtime().'.'.$ext->getExtension();
				        $file_tmp = $_FILES['file']['tmp_name'];
				        $target_dir = "uploads/".$file_name;
						if($about_us_theme){
							if($about_us_theme->image){
								if(unlink("uploads/".$about_us_theme->image)){
						          $this->model->updateImage($id, "");
						        }
							}
						}
				        if(move_uploaded_file($file_tmp, $target_dir)){
				          $this->model->updateImage($id, $file_name);
				        }
						$flashmsg->info('Image updated', URL . 'manage_aboutus_theme', 'SUCCESS');
					}
					if($msg != "" && $msgtype != ""){
						if($msgtype == "success"){
							$formmsg->info($msg, URL . 'manage_aboutus_theme', $msgtype);
						}
						else{
							$formmsg->info($msg, '', $msgtype);
						}
					}
				$browser_title = "Trip2Kailash | About Us";
    			$ActivePage = "Manage_Aboutus_Theme";
    			$PageBranch = "";
  				require APP . 'view/AdminPanel/template/header.php';
  				require APP . 'view/AdminPanel/update_aboutus_theme_view.php';
  				require APP . 'view/AdminPanel/template/footer.php';
					unset($_SESSION['flash_messages']);
  			}
  			else
  			{
  				require APP . 'view/error/index.php';
  			}
		  }
		  else
		  {
        header('location: ' . URL . 'cmslogin');
		  }
    }


}
