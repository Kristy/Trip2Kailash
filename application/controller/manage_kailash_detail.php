<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Manage_kailash_detail extends Controller
{
		function __construct()
		{
		   parent::__construct();
		   session_start();
				if(isset($_SESSION['logged_in']) == null)
				{
					header('location: ' . URL . 'Errorsite');
					exit;
				}
		}
    /***
    * PAGE: index
    * This method handles the error page that will be shown when a page is not found
    */
    public function index()
    {
      if(isset($_SESSION['logged_in']))
      {
        if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin"))
  			{
					$page_purpose = "Update";
					$button_name = "Update";
					$form_url = URL . 'manage_kailash_detail';
					$msg = "";
					$msgtype = "";
					//$file_name = "";
					$id = 1;
					$title = "";
					$description = "";
					$image_theme = "";
					$exists = "False";
					$kailash_detail = $this->model->get_kailash_detail_model($id);

					if($kailash_detail){
						$exists = "True";
						$title = $kailash_detail->title;
						$description = $kailash_detail->description;
						$image_theme = 'uploads/'.$kailash_detail->image;
					}
					$flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
					$formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
					if(isset($_POST["submit_theme"]))
					{
						$title = $_POST['title'];
						$description = $_POST['description'];

						$title = stripslashes($title);
						$description = stripslashes($description);

						$title = trim($title);
						$description= trim($description);

						if($exists == "True")
						{
							$this->model->edit_kailash_detail_model($id, $title, $description);
						}
						else
						{
							$this->model->add_kailash_detail_model($id, $title, $description);
						}
						$msg = "Updated Successfully.";
						$msgtype = "success";
					}

					if(isset($_POST["submit_theme_image"]))
					{
						$targetDir = "uploads/";
				        $file_name = $_FILES['file']['name'];
				        $ext = new SplFileInfo($file_name);
				        $file_name = 'kailash_detail_'.microtime().'.'.$ext->getExtension();
				        $file_tmp = $_FILES['file']['tmp_name'];
				        $target_dir = "uploads/".$file_name;
						if($kailash_detail){
							if(unlink("uploads/".$kailash_detail->image)){
					          $this->model->updatekailashImage($id, "");
					        }
						}
				        if(move_uploaded_file($file_tmp, $target_dir)){
				          $this->model->updatekailashImage($id, $file_name);
				        }
						$flashmsg->info('Image updated', URL . 'manage_kailash_detail', 'SUCCESS');
					}
					if($msg != "" && $msgtype != ""){
						if($msgtype == "success"){
							$formmsg->info($msg, URL . 'manage_kailash_detail', $msgtype);
						}
						else{
							$formmsg->info($msg, '', $msgtype);
						}
					}
				$browser_title = "Trip2Kailash | Kailash Detail";
    			$ActivePage = "Kailash_Page";
    			$PageBranch = "Kailash_Detail_Page";

  				require APP . 'view/AdminPanel/template/header.php';
  				require APP . 'view/AdminPanel/update_kailash_detail_view.php';
  				require APP . 'view/AdminPanel/template/footer.php';
					unset($_SESSION['flash_messages']);
  			}
  			else
  			{
  				require APP . 'view/error/index.php';
  			}
		  }
		  else
		  {
        title('location: ' . URL . 'cmslogin');
		  }
    }


}
