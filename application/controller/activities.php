<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Activities extends Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $Page = "Tour_Destination";

        $destination_id = $_GET['destination'];

        $destination_detail = $this->model->getDestinationRow($destination_id);
        $activities = $this->model->get_destination_activities($destination_id);
        $guides = $this->model->get_destination_guides($destination_id);

        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/activities_view.php';
        require APP . 'view/website/templates/footer.php';

    }

}
