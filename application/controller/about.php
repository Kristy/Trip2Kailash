<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class About extends Controller
{
	function __construct()
	{
	   parent::__construct();
	}
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
		$Page = "Gallery_Page";
		$id = 1;
		$title = "";
		$quote = "";
		$left_title = "";
		$left_description = "";
		$right_title = "";
		$right_description  = "";

		$image_theme = "";
		$about_us_theme = $this->model->get_about_theme_model($id);
		if($about_us_theme){
			$title = $about_us_theme->title;
			$quote = $about_us_theme->quote;
			$left_title = $about_us_theme->left_title;
			$left_description = $about_us_theme->left_description;
			$right_title = $about_us_theme->right_title;
			$right_description = $about_us_theme->right_description;
			if($about_us_theme->image){
				$image_theme = 'uploads/'.$about_us_theme->image;
			}
		}

		$national_members = $this->model->getNationalTeamMembers();
        $international_members = $this->model->getInternationalTeamMembers();

        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/about_view.php';
        require APP . 'view/website/templates/footer.php';


    }

}
