<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Places_to_see extends Controller
{
	function __construct()
	{
	   parent::__construct();
	}
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
		$Page = "Places_To_See";

		$id = $_GET['place'];
		$image_theme = "";
		$place_to_see = $this->model->getAttractionRow($id);
		if($place_to_see->image){
			$image_theme = "uploads/".$place_to_see->image;
		}
        require APP . 'view/website/templates/header.php';
        require APP . 'view/website/places_to_see_view.php';
        require APP . 'view/website/templates/footer.php';
    }

}
