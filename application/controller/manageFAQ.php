<?php

class ManageFAQ extends Controller
{
  function __construct()
  {
    parent::__construct();
    session_start();
    if(isset($_SESSION['logged_in']) == null)
    {
      header('location: ' . URL . 'Errorsite');
      exit;
    }
  }

  public function index()
  {
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in']['UserRole'] == MD5("HeadAdmin")){
        $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();

        $FAQs = $this->model->getallFAQ();
        $browser_title = "Trip2Kailash | Manage Kailash Guide";
        $ActivePage = "Manage_FAQ";



        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/ListFAQ_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        require APP . 'view/error/index.php';
      }
    }
    else{
      header('location: ' . URL . 'cmslogin');
    }
  }

  public function addFAQ()
  {
    $msg = "";
    $msgtype = "";
    $title = "";
    $description = "";

    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(isset($_POST["addFAQ"]))
    {
      $title = $_POST['title'];
      $description = $_POST['description'];

      $title = stripslashes($title);
      $description= stripslashes($description);

      $title = trim($title);
      $description= trim($description);

      $this->model->addFAQ($title, $description);
      $msg = "Successfully added";
      $msgtype = "success";


    }
    if($msg != "" && $msgtype != ""){

      if($msgtype == "success"){
        $formmsg->info($msg, URL . 'manageFAQ/addFAQ', $msgtype);
      }
      else{

        $formmsg->info($msg, '', $msgtype);
      }
    }
    $browser_title = "Trip2Kailash | Add FAQ";
    $ActivePage = "Manage_FAQ";

    require APP . 'view/AdminPanel/template/header.php';
    require APP . 'view/AdminPanel/AddFAQ_view.php';
    require APP . 'view/AdminPanel/template/footer.php';
    unset($_SESSION['flash_messages']);
  }

  public function deleteFAQ()
  {
    if (isset($_POST["SubmitDelete"]))
    {
      $id = $_POST['SubmitDelete'];

      $id = stripslashes($id);

      $id = trim($id);
      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $package = $this->model->getFAQRow($id);
      if($package){
        $this->model->deleteFAQ($id);

        $flashmsg->info('A data has been deleted', URL . 'manageFAQ', 'INFO');
      }
      $flashmsg->info('A data was not deleted', URL . 'manageFAQ', 'ERROR');
    }
    else
    {
      header('location: ' . URL . 'manageFAQ');
    }

  }

  public function editFAQ()
  {
    $formmsg = new \Plasticbrain\FlashMessages\FlashMessages();
    if (isset($_GET["submit_to_edit"]))
    {
      $id = $_GET['submit_to_edit'];
      $id = stripslashes($id);
      $id = trim($id);
      $Faq = $this->model->getFAQRow($id);
      if($Faq)
      {
        $title = $Faq->title;
        $description = $Faq->description;
        $browser_title = "Trip2Kailash | Edit Package";
        $ActivePage = "Manage_FAQ";

        require APP . 'view/AdminPanel/template/header.php';
        require APP . 'view/AdminPanel/EditFAQ_view.php';
        require APP . 'view/AdminPanel/template/footer.php';
        unset($_SESSION['flash_messages']);
      }
      else{
        header('location: ' . URL . 'manageFAQ');
      }
    }
    else if (isset($_POST["editActivity_submit"]))
    {
      $id = $_POST['editActivity_submit'];
      $title = $_POST['title'];
      $description = $_POST['description'];

      $id = stripslashes($id);
      $title = stripslashes($title);
      $description = stripslashes($description);

      $id = trim($id);
      $title = trim($title);
      $description = trim($description);

      $titleCount = $this->model->getotherFAQRowheading($title, $id);

      if($titleCount > 0)
      {
        $formmsg->info('Heading already Exists', URL . 'manageFAQ/editFAQ?submit_to_edit='.$id, 'error');
      }

      $this->model->editFAQ($id, $title, $description);

      $flashmsg = new \Plasticbrain\FlashMessages\FlashMessages();
      $flashmsg->info($title.' has been edited', URL . 'manageFAQ', 'INFO');

    }
    else
    {
      header('location: ' . URL . 'manageFAQ');
    }
  }
}
