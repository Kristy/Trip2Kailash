<?php

class Model
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

//------------------------------------------------------------------------------------------//
// This is the model of login  and users

    public function Login($username, $password)
    {
        $sql = "SELECT * FROM users WHERE UserName = :UserName and Password=MD5(:Password) LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':UserName' => $username, ':Password' => $password);
        $query->execute($parameters);

        if($query->rowcount() == 1)
           {
             return $query->fetchAll();
           }
           else
           {
             return false;
           }
    }

    public function updateLoginFailed($UserName)
    {
        $sql = "UPDATE users SET UserFailedLogins = UserFailedLogins+1,
                                LoginAttempts = LoginAttempts+1 WHERE UserName = :UserName";
        $query = $this->db->prepare($sql);
        $parameters = array(':UserName' => $UserName);

        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

    public function getLoginAttempts($UserName)
    {
        $sql = "SELECT LoginAttempts, UserRole FROM users WHERE UserName = :UserName LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':UserName' => $UserName);
        $query->execute($parameters);

        return $query->Fetch();

    }

    public function resetAttempts($UserName)
    {
        $sql = "UPDATE users SET LoginAttempts = 0 WHERE UserName = :UserName";
        $query = $this->db->prepare($sql);
        $parameters = array(':UserName' => $UserName);
        $query->execute($parameters);
    }

    public function getAllUsers()
    {
        $sql = "SELECT UserID, UserName, UserFirstName, UserLastName, Address, PhoneNo, Email, UserFailedLogins
                FROM users
                WHERE UserRole = MD5('Admin');";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getUserName($UserName)
    {
        $sql = "SELECT UserName FROM users WHERE UserName = :UserName";
        $query = $this->db->prepare($sql);
        $parameters = array(':UserName' => $UserName);

        $query->execute($parameters);

        return $query->RowCount();
    }

    public function ResetPass($UserID, $Password)
    {
        $sql = "UPDATE users SET Password = MD5(:Password) WHERE UserID = :UserID";
        $query = $this->db->prepare($sql);
        $parameters = array(':Password' => $Password, ':UserID' => $UserID);

        $query->execute($parameters);
    }

//This is the end of the login model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
//This is the model of banner

    public function getallBanners()
    {
        $sql = "SELECT * FROM banner ORDER BY id DESC;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function get_bannerRow($id)
    {
          $sql = "SELECT * FROM banner
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getBannerRowbyheading($title)
    {
          $sql = "SELECT * FROM banner
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function getotherBannerRowheading($id, $title)
    {
          $sql = "SELECT * FROM banner
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function edit_banner($id, $title, $highlight, $url)
    {
      $sql = "UPDATE banner SET title = :title, highlight = :highlight, url = :url WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':highlight' => $highlight, ':url' => $url, ':id' => $id);

      $query->execute($parameters);
    }

    public function add_banner($title, $file_name, $highlight, $url)
    {
        $sql = "INSERT INTO banner (image, title, highlight, url) VALUES (:file_name, :title, :highlight, :url)";
        $query = $this->db->prepare($sql);
        $parameters = array(':file_name' => $file_name, ':title' => $title, ':highlight' => $highlight, ':url' => $url);
        $query->execute($parameters);
    }

    public function deleteBanner($id)
    {
      $sql = "DELETE FROM banner WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

    public function updateBannerImage($id, $file_name)
    {
        $sql = "UPDATE banner SET image = :image WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':image' => $file_name, ':id' => $id);

        $query->execute($parameters);
    }
// This is the end of the banner
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
//This is the model of kailash_detail

    public function get_kailash_detail_count_model($id)
    {
      if($id != "")
      {
        $sql = "SELECT * FROM kailash_detail WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->RowCount();
      }
      else
      {
        return 0;
      }
    }

    public function get_kailash_detail_model($id)
    {
        $sql = "SELECT * FROM kailash_detail WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function edit_kailash_detail_model($id, $name, $description)
    {
      $sql = "UPDATE kailash_detail SET title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $name, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function add_kailash_detail_model($id, $name, $description)
    {
        $sql = "INSERT INTO kailash_detail (id, title, description) VALUES (:id, :title, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id, ':title' => $name, ':description' => $description);
        $query->execute($parameters);
    }

    public function updatekailashImage($id, $file_name)
    {
        $sql = "UPDATE kailash_detail SET image = :image WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':image' => $file_name, ':id' => $id);

        $query->execute($parameters);
    }
// This is the end of the kailash detail Model
//--------------------------------------------------------------------------------------------------------------//
    //This is the model of highlight

    public function get_highlight_count_model($id)
    {
      if($id != "")
      {
        $sql = "SELECT * FROM highlight WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->RowCount();
      }
      else
      {
        return 0;
      }
    }

    public function gethighlight($id)
    {
        $sql = "SELECT * FROM highlight WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function edit_highlight_model($id, $description)
    {
      $sql = "UPDATE highlight SET description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function add_highlight_model($id, $description)
    {
        $sql = "INSERT INTO highlight (id, description) VALUES (:id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id, ':description' => $description);
        $query->execute($parameters);
    }
// This is the end of the highlight
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash Activity
    public function addKailashPackageModel($title, $file_name, $tour_length_outline, $best_time_to_visit, $physical_demand, $price, $summary, $description)
    {
        $sql = "INSERT INTO kailash_packages (title, image, tour_length_outline, best_time_to_visit, physical_demand, price, summary, description) VALUES (:title, :image, :tour_length_outline, :best_time_to_visit, :physical_demand, :price, :summary, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':image' => $file_name, ':tour_length_outline' => $tour_length_outline, ':best_time_to_visit' => $best_time_to_visit, ':physical_demand' => $physical_demand, ':price' => $price, ':summary' => $summary, ':description' => $description);
        $query->execute($parameters);
    }

    public function getallKailashPackages()
    {
        $sql = "SELECT * FROM kailash_packages;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getlatest4KailashPackages()
    {
        $sql = "SELECT * FROM kailash_packages ORDER BY id DESC LIMIT 5;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getlatestKailashPackages()
    {
        $sql = "SELECT * FROM kailash_packages ORDER BY added_on DESC LIMIT 5;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getKailashPackageRowbyheading($title)
    {
          $sql = "SELECT * FROM kailash_packages
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function getkailashPackageRow($id)
    {
          $sql = "SELECT * FROM kailash_packages
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getotherKailashPackageRowheading($id, $title)
    {
          $sql = "SELECT * FROM kailash_packages
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function deletekailashpackageModel($id)
    {
      $sql = "DELETE FROM kailash_packages WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

    public function editKailashPackageModel($id, $title, $tour_length_outline, $best_time_to_visit, $physical_demand, $price, $summary, $description)
    {
      $sql = "UPDATE kailash_packages SET title = :title, tour_length_outline = :tour_length_outline, best_time_to_visit = :best_time_to_visit, physical_demand = :physical_demand, price = :price, summary = :summary, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':tour_length_outline' => $tour_length_outline, ':best_time_to_visit' => $best_time_to_visit, ':physical_demand' => $physical_demand, ':price' => $price, ':summary' => $summary, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function editKPImage($id, $file_name)
    {
      $sql = "UPDATE kailash_packages SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function highlightkailashpackageModel($id)
    {
      $sql = "UPDATE kailash_packages SET highlight = '1' WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

    public function gethighlightedkailashpackageModel()
    {
      $sql = "SELECT * FROM kailash_packages
              WHERE highlight = '1'";
      $query = $this->db->prepare($sql);
      $query->execute();

        return $query->FetchAll();
    }

    public function unhighlightkailashpackageModel()
    {
      $sql = "UPDATE kailash_packages SET highlight = '0'";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

// this is the end of kailash package model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash Guide
    public function addKailashGuidel($title, $file_name, $description)
    {
        $sql = "INSERT INTO kailash_guide (title, image, description) VALUES (:title, :image, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':image' => $file_name, ':description' => $description);
        $query->execute($parameters);
    }

    public function getallKailashGuides()
    {
        $sql = "SELECT * FROM kailash_guide;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getKailashGuideRowbyheading($title)
    {
          $sql = "SELECT * FROM kailash_guide
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function getkailashGuideRow($id)
    {
          $sql = "SELECT * FROM kailash_guide
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getotherKailashGuideRowheading($id, $title)
    {
          $sql = "SELECT * FROM kailash_guide
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editKailashGuide($id, $title, $description)
    {
      $sql = "UPDATE kailash_guide SET title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function editKailashGuideImage($id, $file_name)
    {
      $sql = "UPDATE kailash_guide SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function deletekailashGuide($id)
    {
      $sql = "DELETE FROM kailash_guide WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }


// this is the end of kailash package model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash itenary
    public function getKailashItenary($kp_id)
    {
        $sql = "SELECT kailash_itenary.*, kailash_packages.title as kp_title
        FROM kailash_itenary
        JOIN kailash_packages
        ON kailash_packages.id = kailash_itenary.kailash_package_id
        WHERE kailash_itenary.kailash_package_id = :kp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':kp_id' => $kp_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function addKailashItenaryModel($name, $file_name,  $title, $description, $kp_id)
    {
        $sql = "INSERT INTO kailash_itenary (name, image, title, kailash_package_id, description) VALUES (:name, :image,  :title, :kailash_package_id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name' => $name, ':image' => $file_name, ':title' => $title, ':kailash_package_id' => $kp_id, ':description' => $description);
        $query->execute($parameters);
    }

    public function getItenaryRow($id)
    {
          $sql = "SELECT * FROM kailash_itenary
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getItenaryRowbyheading($title)
    {
          $sql = "SELECT * FROM kailash_itenary
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function getotherItenaryRowheading($id, $title)
    {
          $sql = "SELECT * FROM kailash_itenary
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editItenary($id, $file_name)
    {
      $sql = "UPDATE kailash_itenary SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function editKailashItenary($id, $name, $title, $description)
    {
      $sql = "UPDATE kailash_itenary SET name = :name, title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':name' => $name, ':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deletekailashItenaryModel($id)
    {
      $sql = "DELETE FROM kailash_itenary WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Kailash itenary model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash booking
    public function getKailashBooking($kp_id)
    {
        $sql = "SELECT kailash_booking.*, kailash_packages.title as kp_title
        FROM kailash_booking
        JOIN kailash_packages
        ON kailash_packages.id = kailash_booking.k_id
        WHERE kailash_booking.k_id = :kp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':kp_id' => $kp_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getKailashBookingRow($id)
    {
          $sql = "SELECT kailash_booking.*, kailash_packages.title as kp_title
          FROM kailash_booking
          JOIN kailash_packages
          ON kailash_packages.id = kailash_booking.k_id
          WHERE kailash_booking.id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function addBooking($kp_id, $from, $to, $status)
    {
        $sql = "INSERT INTO kailash_booking (from_date, to_date, status, k_id) VALUES (:from, :to,  :status, :k_id)";
        $query = $this->db->prepare($sql);
        $parameters = array(':from' => $from, ':to' => $to, ':status' => $status, ':k_id' => $kp_id);
        $query->execute($parameters);
    }

    public function getKailashBookingRowbyheading($title)
    {
          $sql = "SELECT * FROM kailash_booking
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function getotherKPBookingRowheading($id, $title)
    {
          $sql = "SELECT * FROM kailash_booking
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editKailashBooking($id, $from_date, $to_date, $status)
    {
      $sql = "UPDATE kailash_booking SET from_date = :from_date, to_date = :to_date, status = :status WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':from_date' => $from_date, ':to_date' => $to_date, ':status' => $status, ':id' => $id);

      $query->execute($parameters);
    }

    public function deletekailashBookingModel($id)
    {
      $sql = "DELETE FROM kailash_booking WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Kailash booking model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash Tour Requirement
    public function getKailashTourRequirements($kp_id)
    {
        $sql = "SELECT kailash_tour_requirement.*, kailash_packages.title as kp_title
        FROM kailash_tour_requirement
        JOIN kailash_packages
        ON kailash_packages.id = kailash_tour_requirement.kailash_package_id
        WHERE kailash_tour_requirement.kailash_package_id = :kp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':kp_id' => $kp_id);
        $query->execute($parameters);


        return $query->FetchAll();
    }

    public function getKailashTourRequirementRow($id)
    {
          $sql = "SELECT * FROM kailash_tour_requirement
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function addKailashTourRequirementModel($title, $kp_id, $description)
    {
        $sql = "INSERT INTO kailash_tour_requirement (title, kailash_package_id, description) VALUES (:title, :kailash_package_id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':kailash_package_id' => $kp_id, ':description' => $description);
        $query->execute($parameters);
    }

    public function getotherKailashTourRequirementRowheading($id, $title)
    {
          $sql = "SELECT * FROM kailash_tour_requirement
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editKailashTourRequirement($id, $title, $description)
    {
      $sql = "UPDATE kailash_tour_requirement SET title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteKailashTourRequirementModel($id)
    {
      $sql = "DELETE FROM kailash_tour_requirement WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Tour Requirement model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Destination
    public function getDestinations()
    {
        $sql = "SELECT * FROM destination";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getDestinationRowByName($name)
    {
          $sql = "SELECT * FROM destination
          WHERE name = :name";
          $query = $this->db->prepare($sql);
          $parameters = array(':name' => $name);

          $query->execute($parameters);

          return $query;
    }

    public function getDestinationRow($id)
    {
          $sql = "SELECT * FROM destination
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function addDestinationModel($name, $file_name, $description, $summary)
    {
        $sql = "INSERT INTO destination (name, image, summary, description) VALUES (:name, :image, :summary, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name' => $name, ':image' => $file_name, ':summary' => $summary, ':description' => $description);
        $query->execute($parameters);
    }

    public function getotherDestinationRowheading($id, $name)
    {
          $sql = "SELECT * FROM destination
          WHERE name = :name
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':name' => $name);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editDestination($id, $name, $description, $summary)
    {
      $sql = "UPDATE destination SET name = :name, summary = :summary, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':name' => $name, ':summary' => $summary, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteDestinationModel($id)
    {
      $sql = "DELETE FROM destination WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Destination model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
//This is the model of Destination Guide

    public function get_destination_guide_model()
    {
        $sql = "SELECT destination_guide.*, destination.name as d_name
        FROM destination_guide
        JOIN destination
        ON destination.id = destination_guide.d_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function get_destination_guides($d_id)
    {
        $sql = "SELECT destination_guide.*, destination.name as d_name
        FROM destination_guide
        JOIN destination
        ON destination.id = destination_guide.d_id
        WHERE destination.id = :d_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':d_id' => $d_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getDestinationGuideRowbyheading($title, $destination)
    {
          $sql = "SELECT * FROM destination_guide
          WHERE title = :title
          AND destination_id = :destination_id";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title, ':destination_id' => $destination);

          $query->execute($parameters);

          return $query;
    }

    public function addDestinationGuide($title, $file_name, $destination, $description)
    {
        $sql = "INSERT INTO destination_guide (title, image, d_id, description) VALUES (:title, :image, :destination_id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':image' => $file_name, ':destination_id' => $destination, ':description' => $description);
        $query->execute($parameters);
    }

    public function getDestinationGuideRow($id)
    {
        $sql = "SELECT destination_guide.*, destination.name as d_name
        FROM destination_guide
        JOIN destination
        ON destination.id = destination_guide.d_id
        WHERE destination_guide.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function getotherDestinationGuideRowheading($id, $title)
    {
          $sql = "SELECT * FROM destination_guide
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editDestinationGuide($id, $title, $destination, $description)
    {
      $sql = "UPDATE destination_guide SET title = :title, d_id = :destination, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':destination' => $destination, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function editDestinationGuideImage($id, $file_name)
    {
      $sql = "UPDATE destination_guide SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteDestinationGuide($id)
    {
      $sql = "DELETE FROM destination_guide WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

// This is the end of the Destination Guide
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
//This is the model of Activity

    public function get_activities_model()
    {
        $sql = "SELECT activity.*, destination.name as d_name
        FROM activity
        JOIN destination
        ON destination.id = activity.destination_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function get_latest_activities_model()
    {
        $sql = "SELECT activity.*, destination.name as d_name
        FROM activity
        JOIN destination
        ON destination.id = activity.destination_id
        ORDER BY id DESC
        LIMIT 6";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function get_destination_activities($d_id)
    {
        $sql = "SELECT activity.*, destination.name as d_name
        FROM activity
        JOIN destination
        ON destination.id = activity.destination_id
        WHERE destination.id = :d_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':d_id' => $d_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getActivityRowbyheading($title, $destination)
    {
          $sql = "SELECT * FROM activity
          WHERE title = :title
          AND destination_id = :destination_id";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title, ':destination_id' => $destination);

          $query->execute($parameters);

          return $query;
    }

    public function addActivityModel($title, $file_name, $destination, $description)
    {
        $sql = "INSERT INTO activity (title, image, destination_id, description) VALUES (:title, :image, :destination_id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':image' => $file_name, ':destination_id' => $destination, ':description' => $description);
        $query->execute($parameters);
    }

    public function getActivityRow($id)
    {
        $sql = "SELECT activity.*, destination.name as d_name
        FROM activity
        JOIN destination
        ON destination.id = activity.destination_id
        WHERE activity.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function getotherActivityRowheading($id, $title)
    {
          $sql = "SELECT * FROM activity
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editActivity($id, $title, $destination, $description)
    {
      $sql = "UPDATE activity SET title = :title, destination_id = :destination, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':destination' => $destination, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteActivityModel($id)
    {
      $sql = "DELETE FROM activity WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

// This is the end of the activity Model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
//This is the model of service package

    public function get_service_package_model()
    {
        $sql = "SELECT service_package.*, activity.title as a_title
        FROM service_package
        JOIN activity
        ON activity.id = service_package.activity_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function update_package_id($id)
    {
      $sql = "UPDATE service_package SET recently_viewed = now(), no_of_visit = no_of_visit + 1 WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

    public function get_recently_viewed_service()
    {
        $sql = "SELECT service_package.*, activity.title as a_title
        FROM service_package
        JOIN activity
        ON activity.id = service_package.activity_id
        ORDER BY service_package.recently_viewed DESC
        LIMIT 6";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function get_popular_service()
    {
        $sql = "SELECT service_package.*, activity.title as a_title
        FROM service_package
        JOIN activity
        ON activity.id = service_package.activity_id
        ORDER BY service_package.no_of_visit DESC
        LIMIT 5";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function get_activity_packages($a_id)
    {
        $sql = "SELECT service_package.*, activity.title as a_title
        FROM service_package
        JOIN activity
        ON activity.id = service_package.activity_id
        WHERE activity.id = :a_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':a_id' => $a_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getPackageRowbyheading($title, $activity)
    {
          $sql = "SELECT * FROM service_package
          WHERE title = :title
          AND activity_id = :activity_id";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title, ':activity_id' => $activity);

          $query->execute($parameters);

          return $query;
    }

    public function addPackageModel($title, $file_name, $tour_length_outline, $best_time_to_visit, $physical_demand, $summary, $price, $activity, $description)
    {
        $sql = "INSERT INTO service_package (title, image, tour_length_outline, best_time_to_visit, physical_demand, price, summary, activity_id, description) VALUES (:title, :image, :tour_length_outline, :best_time_to_visit, :physical_demand, :price, :summary, :activity_id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':image' => $file_name, ':tour_length_outline' => $tour_length_outline, ':best_time_to_visit' => $best_time_to_visit, ':physical_demand' => $physical_demand, ':price' => $price, ':summary' => $summary, ':activity_id' => $activity, ':description' => $description);
        $query->execute($parameters);
    }

    public function getPackageRow($id)
    {
        $sql = "SELECT service_package.*, activity.title as a_title
        FROM service_package
        JOIN activity
        ON activity.id = service_package.activity_id
        WHERE service_package.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function getotherPackageRowheading($id, $title)
    {
          $sql = "SELECT * FROM service_package
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editServicePackage($id, $title, $tour_length_outline, $best_time_to_visit, $physical_demand, $price, $summary, $activity, $description)
    {
      $sql = "UPDATE service_package SET title = :title, description = :description, tour_length_outline = :tour_length_outline, best_time_to_visit = :best_time_to_visit, physical_demand = :physical_demand, price = :price, summary = :summary, activity_id = :activity WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':tour_length_outline' => $tour_length_outline, ':best_time_to_visit' => $best_time_to_visit, ':physical_demand' => $physical_demand, ':description' => $description, ':price' => $price,':summary' => $summary, ':activity' => $activity, ':id' => $id);

      $query->execute($parameters);
    }

    public function editServicepackageImage($id, $file_name)
    {
      $sql = "UPDATE service_package SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function deletePackageModel($id)
    {
      $sql = "DELETE FROM service_package WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }

// This is the end of the Service package Model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Service itenary
    public function getServiceItenary($sp_id)
    {
        $sql = "SELECT service_itenary.*, service_package.title as sp_title
        FROM service_itenary
        JOIN service_package
        ON service_package.id = service_itenary.service_package_id
        WHERE service_itenary.service_package_id = :sp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':sp_id' => $sp_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getServiceItenaryRow($id)
    {
        $sql = "SELECT *
        FROM service_itenary
        WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function addServiceItenaryModel($name, $title, $description, $sp_id)
    {
        $sql = "INSERT INTO service_itenary (name, title, service_package_id, description) VALUES (:name, :title, :service_package_id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name' => $name, ':title' => $title, ':service_package_id' => $sp_id, ':description' => $description);
        $query->execute($parameters);
    }

    public function getotherServiceItenaryRowheading($id, $title)
    {
          $sql = "SELECT * FROM service_itenary
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editServiceItenary($id, $file_name)
    {
      $sql = "UPDATE service_itenary SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function editSItenary($id, $name, $title, $description)
    {
      $sql = "UPDATE service_itenary SET name = :name, title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':name' => $name, ':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteServiceItenaryModel($id)
    {
      $sql = "DELETE FROM service_itenary WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Service itenary model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Tour Requirement
    public function getTourRequirements($sp_id)
    {
        $sql = "SELECT tour_requirement.*, service_package.title as sp_title
        FROM tour_requirement
        JOIN service_package
        ON service_package.id = tour_requirement.service_package_id
        WHERE tour_requirement.service_package_id = :sp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':sp_id' => $sp_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function addTourRequirementModel($title, $sp_id, $description)
    {
        $sql = "INSERT INTO tour_requirement (title, service_package_id, description) VALUES (:title, :service_package_id, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':service_package_id' => $sp_id, ':description' => $description);
        $query->execute($parameters);
    }

    public function getTourRequirementRow($id)
    {
          $sql = "SELECT * FROM tour_requirement
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

 public function getotherTourRequirementRowheading($id, $title)
    {
          $sql = "SELECT * FROM tour_requirement
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editTourRequirement($id, $title, $description)
    {
      $sql = "UPDATE tour_requirement SET title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteTourRequirementModel($id)
    {
      $sql = "DELETE FROM tour_requirement WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Tour Requirement model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Tour Association
    public function getAssociated()
    {
        $sql = "SELECT *
        FROM associated_with";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function addAssociatedModel($title, $url)
    {
        $sql = "INSERT INTO associated_with (title, url) VALUES (:title, :url)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':url' => $url);
        $query->execute($parameters);
    }

    public function deleteAssociatedModel($id)
    {
      $sql = "DELETE FROM associated_with WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Tour Association model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Tour Requirement
    public function getAffilated()
    {
        $sql = "SELECT *
        FROM affilated_to ORDER BY added_date";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getAffilatedRow($id)
    {
        $sql = "SELECT *
        FROM affilated_to
        WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function addAffilatedToModel($image, $url)
    {
        $sql = "INSERT INTO affilated_to (image, url) VALUES (:image, :url)";
        $query = $this->db->prepare($sql);
        $parameters = array(':image' => $image, ':url' => $url);
        $query->execute($parameters);
    }

    public function getotherAffilatedHeading($url, $id)
    {
          $sql = "SELECT * FROM affilated_to
          WHERE url = :url
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':url' => $url);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editAffilatedModel($id, $url)
    {
      $sql = "UPDATE affilated_to SET url = :url WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':url' => $url, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteAffilatedModel($id)
    {
      $sql = "DELETE FROM affilated_to WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Tour Requirement model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
//This is the model of essential info

    public function addEssentialInfo($title, $file_name, $description)
    {
        $sql = "INSERT INTO essential_info (title, image, description) VALUES (:title, :image, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':image' => $file_name, ':description' => $description);
        $query->execute($parameters);
    }

    public function getallEssentialInfos()
    {
        $sql = "SELECT * FROM essential_info;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getEssentialInfoRowbyheading($title)
    {
          $sql = "SELECT * FROM essential_info
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function getEssentialInfoRow($id)
    {
          $sql = "SELECT * FROM essential_info
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getotherEssentialInfoRowheading($id, $title)
    {
          $sql = "SELECT * FROM essential_info
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editEssentialInfo($id, $title, $description)
    {
      $sql = "UPDATE essential_info SET title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteEssentialInfo($id)
    {
      $sql = "DELETE FROM essential_info WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// This is the end of the essential_info Model
//--------------------------------------------------------------------------------------------------------------//
    //--------------------------------------------------------------------------------------------------------------//
// This is the model of Team Member
    public function addTeamMember($name, $file_name, $post, $type)
    {
        $sql = "INSERT INTO team_member (name, image, post, type) VALUES (:name, :image, :post, :type)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name' => $name, ':image' => $file_name, ':post' => $post, ':type' => $type);
        $query->execute($parameters);
    }

    public function getallTeamMembers()
    {
        $sql = "SELECT * FROM team_member;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getNationalTeamMembers()
    {
        $sql = "SELECT * FROM team_member WHERE type = 'national';";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getInternationalTeamMembers()
    {
        $sql = "SELECT * FROM team_member WHERE type = 'international';";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getTeamMemberRowbyheading($name)
    {
          $sql = "SELECT * FROM team_member
          WHERE name = :name";
          $query = $this->db->prepare($sql);
          $parameters = array(':name' => $name);

          $query->execute($parameters);

          return $query;
    }

    public function getTeamMemberRow($id)
    {
          $sql = "SELECT * FROM team_member
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getotherTeamMemberRowheading($id, $name)
    {
          $sql = "SELECT * FROM team_member
          WHERE name = :name
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':name' => $name);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editPackageModel($id, $name, $post, $type)
    {
      $sql = "UPDATE team_member SET name = :name, post = :post, type = :type WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':name' => $name, ':post' => $post, ':type' => $type, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteTeamMember($id)
    {
      $sql = "DELETE FROM team_member WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }


// this is the end of Team Member
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of FAQ
    public function addFAQ($title, $description)
    {
        $sql = "INSERT INTO faq (title, description) VALUES (:title, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':description' => $description);
        $query->execute($parameters);
    }

    public function getallFAQ()
    {
        $sql = "SELECT * FROM faq ORDER BY id DESC;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getFAQRowbyheading($title)
    {
          $sql = "SELECT * FROM faq
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function getFAQRow($id)
    {
          $sql = "SELECT * FROM faq
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getotherFAQRowheading($id, $title)
    {
          $sql = "SELECT * FROM faq
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editFAQ($id, $title, $description)
    {
      $sql = "UPDATE faq SET title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteFAQ($id)
    {
      $sql = "DELETE FROM faq WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }


// this is the end of FAQ
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of icon
    public function addIcon($icon, $icon_name)
    {
        $sql = "INSERT INTO icon (icon, name) VALUES (:icon, :icon_name)";
        $query = $this->db->prepare($sql);
        $parameters = array(':icon' => $icon, ':icon_name' => $icon_name);
        $query->execute($parameters);
    }

    public function getallIcon()
    {
        $sql = "SELECT * FROM icon;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getIconRowbyheading($icon)
    {
          $sql = "SELECT * FROM icon
          WHERE icon = :icon";
          $query = $this->db->prepare($sql);
          $parameters = array(':icon' => $icon);

          $query->execute($parameters);

          return $query;
    }

    public function getIconRow($id)
    {
          $sql = "SELECT * FROM icon
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function deleteIcon($id)
    {
      $sql = "DELETE FROM icon WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }


// this is the end of Icon
//--------------------------------------------------------------------------------------------------------------//
    //--------------------------------------------------------------------------------------------------------------//
// This is the model of Attraction
    public function getallAttraction()
    {
        $sql = "SELECT * FROM attraction;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getlimitedAttraction()
    {
        $sql = "SELECT * FROM attraction limit 5;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getAttractionRowbyheading($title)
    {
          $sql = "SELECT * FROM attraction
          WHERE title = :title";
          $query = $this->db->prepare($sql);
          $parameters = array(':title' => $title);

          $query->execute($parameters);

          return $query;
    }

    public function addAttraction($file_name, $title, $description)
    {
        $sql = "INSERT INTO attraction (title, image, description) VALUES (:title, :file_name, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':title' => $title, ':file_name' => $file_name, ':description' => $description);
        $query->execute($parameters);
    }

    public function getotherAttractionRowheading($id, $title)
    {
          $sql = "SELECT * FROM attraction
          WHERE title = :title
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':title' => $title);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editAttraction($id, $title, $description)
    {
      $sql = "UPDATE attraction SET title = :title, description = :description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':description' => $description, ':id' => $id);

      $query->execute($parameters);
    }

    public function editAttractionImage($id, $file_name)
    {
      $sql = "UPDATE attraction SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function getAttractionRow($id)
    {
        $sql = "SELECT attraction.*
        FROM attraction
        WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function deleteAttraction($id)
    {
      $sql = "DELETE FROM attraction WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }


// this is the end of Icon
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Testimonial
    public function addTestimonial($name, $file_name, $feedback)
    {
        $sql = "INSERT INTO testimonial (name, image, feedback) VALUES (:name, :image, :feedback)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name' => $name, ':image' => $file_name, ':feedback' => $feedback);
        $query->execute($parameters);
    }

    public function getallTestimonial()
    {
        $sql = "SELECT * FROM testimonial ORDER BY id DESC;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getTestimonialRowbyheading($name)
    {
          $sql = "SELECT * FROM testimonial
          WHERE name = :name";
          $query = $this->db->prepare($sql);
          $parameters = array(':name' => $name);

          $query->execute($parameters);

          return $query;
    }

    public function getTestimonialRow($id)
    {
          $sql = "SELECT * FROM testimonial
          WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function getotherTestimonialRowheading($id, $name)
    {
          $sql = "SELECT * FROM testimonial
          WHERE name = :name
          AND id != :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id, ':name' => $name);

          $query->execute($parameters);

          return $query->RowCount();
    }

    public function editTestimonial($id, $name, $feedback)
    {
      $sql = "UPDATE testimonial SET name = :name, feedback = :feedback WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':name' => $name, ':feedback' => $feedback, ':id' => $id);

      $query->execute($parameters);
    }

    public function editTestimonialImage($id, $file_name)
    {
      $sql = "UPDATE testimonial SET image = :image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':image' => $file_name, ':id' => $id);

      $query->execute($parameters);
    }

    public function deleteTestimonial($id)
    {
      $sql = "DELETE FROM testimonial WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }


// this is the end of Testimonial
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash Package booking
    public function getKPBooking($kailash_id)
    {
        $sql = "SELECT booking.*, kailash_packages.title as kp_title
        FROM booking
        JOIN kailash_packages
        ON kailash_packages.id = booking.kp_id
        WHERE booking.kp_id = :kp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':kp_id' => $kailash_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getallKPBooking()
    {
        $sql = "SELECT booking.*, kailash_packages.title as kp_title
        FROM booking
        JOIN kailash_packages
        ON kailash_packages.id = booking.kp_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getKPBookingRow($id)
    {
          $sql = "SELECT booking.*, kailash_packages.title as kp_title
          FROM booking
          JOIN kailash_packages
          ON kailash_packages.id = booking.kp_id
          WHERE booking.id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function addKPBooking($full_name, $file_name, $gender, $kailash_id, $kailash_date_id, $nationality, $dob, $passport_no, $date_of_issue, $place_of_issue, $date_of_expiry, $permanent_address, $profession, $mobile_no, $telephone, $email_id, $e_name, $e_relation, $e_telephone, $e_email_id, $preffered_arrival_date)
    {
        $sql = "INSERT INTO booking (full_name, image, gender, kp_id, package_date_id, nationality, dob, passport_no, date_of_issue, place_of_issue, date_of_expiry, permanent_address, profession, mobile_no, telephone, email_id, e_name, e_relation, e_telephone, e_email_id, preffered_arrival_date) VALUES (:full_name, :image,  :gender, :kp_id, :package_date_id, :nationality, :dob,  :passport_no, :date_of_issue, :place_of_issue, :date_of_expiry,  :permanent_address, :profession, :mobile_no, :telephone,  :email_id, :e_name, :e_relation, :e_telephone,  :e_email_id, :preffered_arrival_date)";
        $query = $this->db->prepare($sql);
        $parameters = array(':full_name' => $full_name, ':image' => $file_name, ':gender' => $gender, ':kp_id' => $kailash_id, ':package_date_id' => $kailash_date_id, ':nationality' => $nationality, ':dob' => $dob, ':passport_no' => $passport_no, ':date_of_issue' => $date_of_issue, ':place_of_issue' => $place_of_issue, ':date_of_expiry' => $date_of_expiry, ':permanent_address' => $permanent_address, ':profession' => $profession, ':mobile_no' => $mobile_no, ':telephone' => $telephone, ':email_id' => $email_id, ':e_name' => $e_name, ':e_relation' => $e_relation, ':e_telephone' => $e_telephone, ':e_email_id' => $e_email_id, ':preffered_arrival_date' => $preffered_arrival_date);
        $query->execute($parameters);
    }

    public function getKPBookingRowbyheading($full_name, $preffered_arrival_date)
    {
          $sql = "SELECT * FROM booking
          WHERE full_name = :full_name
          AND preffered_arrival_date = :preffered_arrival_date ";
          $query = $this->db->prepare($sql);
          $parameters = array(':full_name' => $full_name, ':preffered_arrival_date' => $preffered_arrival_date);

          $query->execute($parameters);

          return $query;
    }

    public function deleteKPBookingModel($id)
    {
      $sql = "DELETE FROM booking WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Kailash booking model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash Package Trailor
    public function getKPTrailor($kailash_id)
    {
        $sql = "SELECT kailash_trailor.*, kailash_packages.title as kp_title
        FROM kailash_trailor
        JOIN kailash_packages
        ON kailash_packages.id = kailash_trailor.kp_id
        WHERE kailash_trailor.kp_id = :kp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':kp_id' => $kailash_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getallKPTrailor()
    {
        $sql = "SELECT kailash_trailor.*, kailash_packages.title as kp_title
        FROM kailash_trailor
        JOIN kailash_packages
        ON kailash_packages.id = kailash_trailor.kp_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function addKPTrailor($full_name, $file_name, $gender, $kailash_id, $nationality, $dob, $passport_no, $date_of_issue, $place_of_issue, $date_of_expiry, $permanent_address, $profession, $mobile_no, $telephone, $email_id, $e_name, $e_relation, $e_telephone, $e_email_id, $preffered_arrival_date, $description)
    {
        $sql = "INSERT INTO kailash_trailor (full_name, image, gender, kp_id, nationality, dob, passport_no, date_of_issue, place_of_issue, date_of_expiry, permanent_address, profession, mobile_no, telephone, email_id, e_name, e_relation, e_telephone, e_email_id, preffered_arrival_date, description) VALUES (:full_name, :image, :gender, :kp_id, :nationality, :dob,  :passport_no, :date_of_issue, :place_of_issue, :date_of_expiry,  :permanent_address, :profession, :mobile_no, :telephone,  :email_id, :e_name, :e_relation, :e_telephone,  :e_email_id, :preffered_arrival_date, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':full_name' => $full_name, ':image' => $file_name, ':gender' => $gender, ':kp_id' => $kailash_id, ':nationality' => $nationality, ':dob' => $dob, ':passport_no' => $passport_no, ':date_of_issue' => $date_of_issue, ':place_of_issue' => $place_of_issue, ':date_of_expiry' => $date_of_expiry, ':permanent_address' => $permanent_address, ':profession' => $profession, ':mobile_no' => $mobile_no, ':telephone' => $telephone, ':email_id' => $email_id, ':e_name' => $e_name, ':e_relation' => $e_relation, ':e_telephone' => $e_telephone, ':e_email_id' => $e_email_id, ':preffered_arrival_date' => $preffered_arrival_date, ':description' => $description);
        $query->execute($parameters);
    }

    public function getKPTrailorRowbyheading($full_name, $preffered_arrival_date)
    {
          $sql = "SELECT * FROM kailash_trailor
          WHERE full_name = :full_name
          AND preffered_arrival_date = :preffered_arrival_date ";
          $query = $this->db->prepare($sql);
          $parameters = array(':full_name' => $full_name, ':preffered_arrival_date' => $preffered_arrival_date);

          $query->execute($parameters);

          return $query;
    }

    public function deleteKPTrailorModel($id)
    {
      $sql = "DELETE FROM kailash_trailor WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of Kailash trailor model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
//This is the model of about us

    public function get_about_theme_count_model($id)
    {
      if($id != "")
      {
        $sql = "SELECT * FROM about_us_theme WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->RowCount();
      }
      else
      {
        return 0;
      }
    }

    public function get_about_theme_model($id)
    {
        $sql = "SELECT * FROM about_us_theme WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->Fetch();
    }

    public function edit_about_theme_model($id, $title, $quote, $left_title, $left_description, $right_title, $right_description)
    {
      $sql = "UPDATE about_us_theme SET title = :title, quote = :quote, left_title = :left_title, left_description = :left_description, right_title = :right_title, right_description = :right_description WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':title' => $title, ':quote' => $quote, ':left_title' => $left_title, ':left_description' => $left_description, ':right_title' => $right_title, ':right_description' => $right_description, ':id' => $id);

      $query->execute($parameters);
    }

    public function add_about_theme_model($id, $title, $quote, $left_title, $left_description, $right_title, $right_description)
    {
        $sql = "INSERT INTO about_us_theme (id, title, quote, left_title, left_description, right_title, right_description) VALUES (:id, :title, :quote, :left_title, :left_description, :right_title, :right_description)";
        $query = $this->db->prepare($sql);
         $parameters = array(':title' => $title, ':quote' => $quote, ':left_title' => $left_title, ':left_description' => $left_description, ':right_title' => $right_title, ':right_description' => $right_description, ':id' => $id);
        $query->execute($parameters);
    }

    public function updateImage($id, $file_name)
    {
        $sql = "UPDATE about_us_theme SET image = :image WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':image' => $file_name, ':id' => $id);

        $query->execute($parameters);
    }
// This is the end of the about us Model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Package Booking
    public function get_package_booking($package_id)
    {
        $sql = "SELECT package_booking.*, service_package.title as sp_title
        FROM package_booking
        JOIN service_package
        ON service_package.id = package_booking.p_id
        WHERE package_booking.p_id = :sp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':sp_id' => $package_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getallServiceBooking()
    {
        $sql = "SELECT package_booking.*, service_package.title as sp_title
        FROM package_booking
        JOIN service_package
        ON service_package.id = package_booking.p_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getSBookingRow($id)
    {
          $sql = "SELECT package_booking.*, service_package.title as sp_title
          FROM package_booking
          JOIN service_package
          ON service_package.id = package_booking.p_id
          WHERE package_booking.id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function addPackageBooking($full_name, $file_name, $gender, $package_id, $nationality, $dob, $passport_no, $date_of_issue, $place_of_issue, $date_of_expiry, $permanent_address, $profession, $mobile_no, $telephone, $email_id, $e_name, $e_relation, $e_telephone, $e_email_id, $preffered_date)
    {
        $sql = "INSERT INTO package_booking (full_name, image, gender, p_id, nationality, dob, passport_no, date_of_issue, place_of_issue, date_of_expiry, permanent_address, profession, mobile_no, telephone, email_id, e_name, e_relation, e_telephone, e_email_id, preffered_date) VALUES (:full_name, :image, :gender, :p_id, :nationality, :dob,  :passport_no, :date_of_issue, :place_of_issue, :date_of_expiry,  :permanent_address, :profession, :mobile_no, :telephone,  :email_id, :e_name, :e_relation, :e_telephone,  :e_email_id, :preffered_date)";
        $query = $this->db->prepare($sql);
        $parameters = array(':full_name' => $full_name, ':image' => $file_name, ':gender' => $gender, ':p_id' => $package_id, ':nationality' => $nationality, ':dob' => $dob, ':passport_no' => $passport_no, ':date_of_issue' => $date_of_issue, ':place_of_issue' => $place_of_issue, ':date_of_expiry' => $date_of_expiry, ':permanent_address' => $permanent_address, ':profession' => $profession, ':mobile_no' => $mobile_no, ':telephone' => $telephone, ':email_id' => $email_id, ':e_name' => $e_name, ':e_relation' => $e_relation, ':e_telephone' => $e_telephone, ':e_email_id' => $e_email_id, ':preffered_date' => $preffered_date);
        $query->execute($parameters);
    }

    public function getPackageBookingRowbyheading($full_name, $preffered_date)
    {
          $sql = "SELECT * FROM package_booking
          WHERE full_name = :full_name
          AND preffered_date = :preffered_date ";
          $query = $this->db->prepare($sql);
          $parameters = array(':full_name' => $full_name, ':preffered_date' => $preffered_date);

          $query->execute($parameters);

          return $query;
    }

    public function deletePackageBookingModel($id)
    {
      $sql = "DELETE FROM package_booking WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of package booking model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Package Booking
    public function get_package_trailor($package_id)
    {
        $sql = "SELECT package_trailor.*, service_package.title as sp_title
        FROM package_booking
        JOIN service_package
        ON service_package.id = package_booking.p_id
        WHERE package_booking.p_id = :sp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':sp_id' => $package_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }


    public function getallServiceTrailor()
    {
        $sql = "SELECT package_trailor.*, service_package.title as sp_title
        FROM package_trailor
        JOIN service_package
        ON service_package.id = package_trailor.p_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function addPackageTrailor($full_name, $file_name, $gender, $package_id, $nationality, $dob, $passport_no, $date_of_issue, $place_of_issue, $date_of_expiry, $permanent_address, $profession, $mobile_no, $telephone, $email_id, $e_name, $e_relation, $e_telephone, $e_email_id, $preffered_date, $description)
    {
        $sql = "INSERT INTO package_trailor (full_name, image, gender, p_id, nationality, dob, passport_no, date_of_issue, place_of_issue, date_of_expiry, permanent_address, profession, mobile_no, telephone, email_id, e_name, e_relation, e_telephone, e_email_id, preffered_date, description) VALUES (:full_name, :image, :gender, :p_id, :nationality, :dob,  :passport_no, :date_of_issue, :place_of_issue, :date_of_expiry,  :permanent_address, :profession, :mobile_no, :telephone,  :email_id, :e_name, :e_relation, :e_telephone,  :e_email_id, :preffered_date, :description)";
        $query = $this->db->prepare($sql);
        $parameters = array(':full_name' => $full_name, ':image' => $file_name, ':gender' => $gender, ':p_id' => $package_id, ':nationality' => $nationality, ':dob' => $dob, ':passport_no' => $passport_no, ':date_of_issue' => $date_of_issue, ':place_of_issue' => $place_of_issue, ':date_of_expiry' => $date_of_expiry, ':permanent_address' => $permanent_address, ':profession' => $profession, ':mobile_no' => $mobile_no, ':telephone' => $telephone, ':email_id' => $email_id, ':e_name' => $e_name, ':e_relation' => $e_relation, ':e_telephone' => $e_telephone, ':e_email_id' => $e_email_id, ':preffered_date' => $preffered_date, ':description' => $description);
        $query->execute($parameters);
    }

    public function getPackageTrailorRowbyheading($full_name, $preffered_date)
    {
          $sql = "SELECT * FROM package_trailor
          WHERE full_name = :full_name
          AND preffered_date = :preffered_date ";
          $query = $this->db->prepare($sql);
          $parameters = array(':full_name' => $full_name, ':preffered_date' => $preffered_date);

          $query->execute($parameters);

          return $query;
    }

    public function deletePackageTrailorModel($id)
    {
      $sql = "DELETE FROM package_trailor WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
    }
// this is the end of package trailor model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Gallery
    public function addImages($image_name)
    {
        $sql = "INSERT INTO gallery (image_name) VALUES (:image_name)";
        $query = $this->db->prepare($sql);
        $parameters = array(':image_name' => $image_name);
        $query->execute($parameters);
    }

    public function getallImages()
    {
        $sql = "SELECT * FROM gallery ORDER BY uploadtime DESC;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getlatestImages()
    {
        $sql = "SELECT * FROM gallery ORDER BY uploadtime DESC LIMIT 6;";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->FetchAll();
    }

    public function getGalleryRow($id)
    {
          $sql = "SELECT * FROM gallery
  				WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function deleteGalleryImage($id)
  	{
  		$sql = "DELETE FROM gallery WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
  	}
// This is the end of the Gallery Model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Kailash Images
    public function addKailashImages($image_name, $package_id)
    {
        $sql = "INSERT INTO kailash_package_image (image_name, kailash_package_id) VALUES (:image_name, :kailash_package_id)";
        $query = $this->db->prepare($sql);
        $parameters = array(':image_name' => $image_name, ':kailash_package_id' => $package_id);
        $query->execute($parameters);
    }

    public function getallKailashImages($package_id)
    {
        $sql = "SELECT * FROM kailash_package_image WHERE kailash_package_id = :kailash_package_id ORDER BY uploadtime DESC;";
        $query = $this->db->prepare($sql);
        $parameters = array(':kailash_package_id' => $package_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getlatestKailashImages($package_id)
    {
        $sql = "SELECT * FROM kailash_package_image WHERE kailash_package_id = :kailash_package_id ORDER BY uploadtime DESC LIMIT 6;";
        $query = $this->db->prepare($sql);
        $parameters = array(':kailash_package_id' => $package_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getKailashImageRow($id)
    {
          $sql = "SELECT * FROM kailash_package_image
  				WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function deleteKailashImage($id)
  	{
  		$sql = "DELETE FROM kailash_package_image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
  	}
// This is the end of the Kailash Image Model
//--------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------//
// This is the model of Service Images
    public function addPackageImages($image_name, $package_id)
    {
        $sql = "INSERT INTO service_package_image (image_name, service_package_id) VALUES (:image_name, :service_package_id)";
        $query = $this->db->prepare($sql);
        $parameters = array(':image_name' => $image_name, ':service_package_id' => $package_id);
        $query->execute($parameters);
    }

    public function getallPackageImages($package_id)
    {
        $sql = "SELECT * FROM service_package_image WHERE service_package_id = :service_package_id ORDER BY uploadtime DESC;";
        $query = $this->db->prepare($sql);
        $parameters = array(':service_package_id' => $package_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getlatestPackageImages($package_id)
    {
        $sql = "SELECT * FROM service_package_image WHERE service_package_id = :service_package_id ORDER BY uploadtime DESC LIMIT 6;";
        $query = $this->db->prepare($sql);
        $parameters = array(':service_package_id' => $package_id);
        $query->execute($parameters);

        return $query->FetchAll();
    }

    public function getPackageImageRow($id)
    {
          $sql = "SELECT * FROM service_package_image
  				WHERE id = :id";
          $query = $this->db->prepare($sql);
          $parameters = array(':id' => $id);

          $query->execute($parameters);

          return $query->Fetch();
    }

    public function deletePackageImage($id)
  	{
  		$sql = "DELETE FROM service_package_image WHERE id = :id";
      $query = $this->db->prepare($sql);
      $parameters = array(':id' => $id);

      $query->execute($parameters);
  	}
// This is the end of the Service Image Model
//--------------------------------------------------------------------------------------------------------------//
}
