<!DOCTYPE HTML>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trip to Kailash</title>
    <!-- <meta name="robots" content="noindex" /> -->
    <!-- <META NAME="geo.position" CONTENT="27.72422; 85.322">
    <META NAME="geo.placename" CONTENT="Lazimpat, Kathmandu">
    <META NAME="geo.region" CONTENT="Nepal"> -->
    <meta name="keywords" content="kailash-travel, kailash-manasarovar, trip2kailash, trip-to-kailash, kailash-yattra, kailash-yattra-2018, kailash-tour, explore-kailash, kailash-journey, visit-kailash, mount-kailash, mount-kailash-yattra, mount-kailash-tour"/>
    <meta name="description" content="Trip to kailash Pvt. Ltd. with a team lead by the young professional in inbound and outbound tourism field lead with expert Tours and Trekking guide working more than decades and done more than 100 trips with Excellency."/>

    <!--    <!-- favion -->
    <!--    <link rel="icon" type="image/png" sizes="16x16" href="frontend/img/favicon-16x16.png">-->
    <!-- link to font awesome -->
    <link media="all" rel="stylesheet" href="frontend/vendors/font-awesome/css/font-awesome.css">
    <!-- link to custom icomoon fonts -->
    <link rel="stylesheet" type="text/css" href="frontend/css/fonts/icomoon/icomoon.css">
    <!-- link to wow js animation -->
    <link media="all" rel="stylesheet" href="frontend/vendors/animate/animate.css">
    <!-- include bootstrap css -->
    <link media="all" rel="stylesheet" href="frontend/css/bootstrap.css">
    <!-- include owl css -->
    <link media="all" rel="stylesheet" href="frontend/vendors/owl-carousel/owl.carousel.css">
    <link media="all" rel="stylesheet" href="frontend/vendors/owl-carousel/owl.theme.css">
    <!-- link to revolution css  -->
    <link rel="stylesheet" type="text/css" href="frontend/vendors/revolution/css/settings.css">
    <!-- include main css -->
    <link media="all" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css">
    <link media="all" rel="stylesheet" href="frontend/css/main.css">
    <style>
    body, html {
        height: 100%;
        margin:0;
        padding:0;
    }
    .bg{
        background-image: url("back.jpg");
        /* Full height */
        height: 100%;
        margin:0;
        padding:0;

        /* Center and scale the image nicely */
        background-position:top center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    </style>
</head>
<body>
<!-- main wrapper -->
<div id="wrapper">
    <div class="page-wrapper">
        <!-- main header -->
        <header id="header" class="default-white-header top-header header-v1">
            <div class="header-bottom">
                <div class="container">
                    <div class="nav-frame">
                        <!-- logo -->
                        <div class="logo">
                            <a >
                                <img class="normal" style="margin-top:-10%" src="logo.jpg" alt="Tript2Kailash">
                                <img class="gray-logo" style="margin-top:-10%;"  src="logo.jpg" alt="Tript2Kailash">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main id="main">
            Trip to kailash Pvt. Ltd. with a team lead by the young professional in inbound and outbound tourism field lead with expert Tours and Trekking guide working more than decades and done more than 100 trips with Excellency.
        </main>
    </div>
</div>
</body>
</html>
