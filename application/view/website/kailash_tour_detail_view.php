
<style>
    #scrollable {
        height: auto;
        overflow: hidden;
        position: relative;
    }

    .info-left .title {
        color: #0d9fb7;
    }

    .tab {
        border-top: 3px solid #333;
        /*padding-top: 20px;*/
        padding: 10em 0;
    }

    .heading {
        color: #0d9fb7;
        text-align: center;
    }

    .nav > li > a .active {
        border-bottom: 2px solid darkred;
    }

    @media print {
        .noprint {
            display: none !important;
        }

        .print {
            display: block !important;
        }
    }

    .printable span {
        background: url('frontend/img/intro/print1.ico') no-repeat;
    }
    table.table {
        min-width: 0;
        font-size:12px;
    }
</style>
<style>
    body {
    padding: 0;
    margin: 0
}

/* header {
    background: transparent url('http://adventure.nationalgeographic.com/2008/11/ecotourism/lodge-jungle.jpg') 0 0;
    height: 100px;
} */

h4 {
   font-weight: bold;

}

p {
    margin: 20px 0;
}

section {
    padding: 20px 0;
}

.wrapper {
    margin: 0 auto;
    position: relative;
    padding: 28px 0 0 0;
}

.scrollnav {
    position: absolute;
    left: 0;
    background: #252525;
    display: flex;
    width: 100%;
    padding: 13px 0 50px 0;
    height: 30px;
    z-index: 2;
    text-align: center;
    vertical-align: middle;
}

.scrollnav a {
    width: 326px;
    font-size: 20px;
    margin: 0 auto;
    color: #959595;
    padding: 2px;
    display: block;
    float: left;
    text-decoration: none;
    margin-right: 6px;
    text-transform: uppercase;
    position: relative;
}

.scrollnav a:hover,
.scrollnav a.active {
    color: #109fb7;
    font-weight: 600;
}

/* .scrollnav a:hover:after,
.scrollnav a.active:after {
    content: '';
    width: 50px;
    height: 5px;
    background: #109fb7;
    display: block;
    position: absolute;
    right: 20px;
    top: 17px;
} */

.fixed {
    position: fixed;
    top: 69px;
}
.s-p-t-b {
    border-top: 3px solid #333;
}
.m-t {
    margin-top: 1.5em;
}
</style>
<div>
    <header></header>




</div>


<main id="main">
    <!-- main tour information -->
    <div class="noprint">
        <section class="container-fluid trip-info">
            <div class="same-height two-columns row" style="padding-top: 140px">
                <div class="height col-md-6">
                    <!-- top image slideshow -->
                    <div id="tour-slide">
                        <div class="slide">
                            <div class="bg-stretch">
                                <!-- <img src="<?php echo $package_detail->image; ?>" alt="image descriprion" height="1104"
                                     width="966"> -->
                                <img src="<?php echo URL; echo 'uploads/'.$package_detail->image; ?>" alt="image description" height="1104"
                                     width="966">
                            </div>
                        </div>
                        <?php foreach($kailashImages as $image){ ?>
                        <div class="slide">
                            <div class="bg-stretch">
                                <img src="<?php echo URL; echo 'uploads/'.$image->image_name; ?>" alt="image descriprion" height="1104"
                                     width="966">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="height col-md-6 text-col">
                    <div class="holder">
                        <h1 class="small-size"><?php echo $package_detail->title; ?></h1>
                        <!--                        <div class="price">-->
                        <!--                            from <strong>US $979</strong>-->
                        <!--                        </div>-->
                        <div class="description">
                            <p><?php echo $package_detail->summary; ?></p>
                        </div>
                        <ul class="reviews-info">
                            <li>
                                <div class="info-left">
                                    <strong class="title">Tour Length and Outline</strong>
                                    <span class="value"><?php echo $package_detail->tour_length_outline; ?></span>
                                </div>
                            </li>
                            <li>
                                <div class="info-left">
                                    <strong class="title">Best Time to Visit:</strong>
                                    <span class="value"><?php echo $package_detail->best_time_to_visit; ?></span>
                                </div>
                            </li>
                            <li>
                                <div class="info-left">
                                    <strong class="title">Physical Demands</strong>
                                    <span class="value"><?php echo $package_detail->physical_demand; ?></span>
                                </div>
                                <div class="info-right">
                                </div>
                            </li>
                            <li>

                            </li>
                        </ul>
                        <div class="btn-holder" style="float: left">
                            <a href="#d-p" class="btn btn-md btn-info">RESERVE THIS TRIP</a>
                        </div>
                        <div class="btn-holder">
                            <a href="<?php echo URL; echo 'kpTrailor?kailash='.$kailash_id?>" target="_blank" class="btn btn-md btn-info">TAILOR THIS TRIP</a>
                        </div>

                        <div class="printable" style="text-align: right;">
                            <a href="#" onclick="myFunction()">
                                <span class="fa fa-print" style="color:#0d9fb7; font-size:25px"> &nbsp; Click to check the printable version</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="tab-container">
        <div class="scrollnav">
            <a href="#" data-scroll="toverview">Trip Overview</a>

            <a href="#" data-scroll="d-d">Day by Day</a>

            <a href="#" data-scroll="d-p">Date and Price</a>
            <a href="#" data-scroll="i-e">Inclusion & Exclusion</a>

            <a href="#" data-scroll="faq">FAQ</a>
        </div>
        <div class="wrapper container">
            <section id="toverview" data-anchor="toverview">
                <div class="row">

                    <div class="col-md-12">
                        <h2 class="heading m-t">Trip Overview</h2>
                        <div class="detail">
                            <?php echo $package_detail->description; ?>
                        </div>
                    </div>
                </div>
            </section>

            <section id="d-d" class="s-p-t-b" data-anchor="d-d">
                <div class="row">
                    <h2 class="heading m-t">Day by Day</h2>
                    <div class="col-md-12">
                        <ol class="detail-accordion">
                            <?php foreach($package_itenaries as $package_itenary){ ?>
                            <li class="active col-sm-12">
                                <a>
                                    <!-- <strong class="title"><?php echo $package_itenary->name; ?></strong> -->
                                    <strong class="title"><b><?php echo $package_itenary->title; ?></b></strong>
                                </a>
                                <div class="">
                                    <div class="slide-holder">

                                        <div <?php if($package_itenary->image){ echo 'class="col-sm-6"'; }else{ echo 'class="col-sm-12"'; } ?> align="justify">
                                            <p><?php echo $package_itenary->description; ?></p>
                                        </div>
                                        <?php if($package_itenary->image){ ?>
                                        <div class="col-sm-6">
                                            <div class="img-wrap" style="max-height:280px; max-width:500px;overflow:hidden;">
                                                <?php if($package_itenary->image){ ?>
                                                <img src="<?php echo URL; echo 'uploads/'.$package_itenary->image; ?>"  height="220" width="350" alt="">
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        </ol>
                    </div>
                </div>
            </section>

            <section id="d-p" class="s-p-t-b" data-anchor="d-p">
                <div class="row">
                    <h2 class="heading m-t">Date and Price</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="calendar"></div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-6" id="date-price-block">
                            <div class="table-responsive" >
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            <strong class="date-text">Departure Dates</strong>
                                        </th>
                                        <th>
                                            <strong class="date-text">Trip Status</strong>
                                        </th>
                                        <th>
                                            &nbsp;
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php
                                                foreach ($bookings as $booking) {
                                                    # code...

                                            ?>
                                            <td>
                                                <div class="cell">
                                                    <div class="middle"><?php echo $booking->from_date; ?> - <?php echo $booking->to_date; ?></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="cell">
                                                    <div class="middle"><?php echo $booking->status; ?></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="cell">
                                                    <div class="middle">
                                                        <a href="<?php echo URL; echo 'kpBooking?kailash_date='.$booking->id; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Booking</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <?php
                                        }
                                        ?>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->
                </div>
            </section>

            <section id="i-e" class="s-p-t-b" data-anchor="i-e">
                <div class="row">
                    <h2 class="heading m-t">Inclusion & Exclusion</h2>
                    <?php foreach($package_IEs as $package_IE){ ?>
                        <div class="col-md-6">
                            <div class="text-box">
                                <div class="holder">
                                    <strong class="title"><?php echo $package_IE->title; ?></strong>
                                    <div>
                                        <?php echo $package_IE->description; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>

            <section id="faq" class="s-p-t-b" data-anchor="faq">
                <div class="row">
                    <h2 class="heading m-t">FAQ</h2>
                    <div class="col-md-6">
                        <ol class="detail-accordion">
                            <?php $i = 0;
                            foreach($FAQs as $FAQ){
                                $i++;
                                if($i % 2 != 0){
                            ?>
                            <li>
                                <a>
                                    <strong class="name">
                                        <?php echo $FAQ->title; ?>
                                    </strong>
                                    <!-- <div class="meta">Reviewed on 14/1/2016</div> -->
                                </a>
                                <div class="slide">
                                    <div class="slide-holder">
                                        <?php echo $FAQ->description; ?>
                                    </div>
                                </div>
                            </li>
                            <?php }} ?>
                        </ol>
                    </div>
                    <div class="col-md-6">
                        <ol class="detail-accordion">
                            <?php $i = 0;
                            foreach($FAQs as $FAQ){
                                $i++;
                                if($i % 2 == 0){
                            ?>
                            <li>
                                <a>
                                    <strong class="name">
                                        <?php echo $FAQ->title; ?>
                                    </strong>
                                    <!-- <div class="meta">Reviewed on 14/1/2016</div> -->
                                </a>
                                <div class="slide">
                                    <div class="slide-holder">
                                        <?php echo $FAQ->description; ?>
                                    </div>
                                </div>
                            </li>
                            <?php }} ?>

                        </ol>
                    </div>
                </div>
            </section>

        </div>
        <!-- tab panes -->
        <div class="print">
            <div class="container tab-content trip-detail" style="text-align: justify">
                <div class="container">
                    <h2 class="text-center text-uppercase">RECENTLY VIEWED</h2>
                    <div class="row" id="guide-slide">
                        <?php foreach($kailash_packages as $kailash_package){ ?>
                        <article class="article">
                            <div class="thumbnail">
                                <h3 class="no-space"><a href="<?php echo URL; echo 'KailashTourDetail?kailash='.$kailash_package->id; ?>"><?php echo $kailash_package->title; ?></a></h3>
                                <strong class="info-title"></strong>
                                <div class="img-wrap" style="max-height:210px; height:210px;overflow:hidden;">
                                    <img src="<?php echo URL; echo 'uploads/'.$kailash_package->image; ?>" height="210"
                                         alt="image description">
                                </div>
                                <footer>
                                    <span class="price">Price:<span><?php echo $kailash_package->price; ?></span></span>
                                </footer>
                            </div>
                        </article>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- reserve trip modal -->
    <?php include 'modal/reserve_trip_form.php'; ?>
    <?php include 'modal/tailor_trip_form.php'; ?>
</main>






<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    function myFunction() {
        window.print();
    }
    $('.scrollnav a').on('click', function() {
        var scrollAnchor = $(this).attr('data-scroll'),
            scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 28;
        $('body,html').animate({
            scrollTop: scrollPoint
        }, 500);
        return false;
    })
    $(window).scroll(function() {
        var windscroll = $(window).scrollTop();
        if (windscroll >= 700) {
            $('.scrollnav').addClass('fixed');
            $('.wrapper section').each(function(i) {
                if ($(this).position().top <= windscroll - 20) {
                    $('.scrollnav a.active').removeClass('active');
                    $('.scrollnav a').eq(i).addClass('active');
                }
            });

        } else {

            $('.scrollnav').removeClass('fixed');
            $('.scrollnav a.active').removeClass('active');
            $('.scrollnav a:first').addClass('active');
        }

    }).scroll();
</script>
