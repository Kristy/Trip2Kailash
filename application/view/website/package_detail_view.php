<style>
    #scrollable {
        height: auto;
        overflow: hidden;
        position: relative;
    }

    .info-left .title {
        color: #0d9fb7;
    }

    .tab {
        border-top: 3px solid #333;
        /*padding-top: 20px;*/
        padding: 10em 0;
    }

    .heading {
        color: #0d9fb7;
        text-align: center;
    }

    .nav > li > a .active {
        border-bottom: 2px solid darkred;
    }
    section {
    padding: 20px 0;
    }

.wrapper {
    margin: 0 auto;
    position: relative;
    padding: 28px 0 0 0;
}

.scrollnav {
    position: absolute;
    left: 0;
    background: #252525;
    display: flex;
    width: 100%;
    padding: 13px 0 50px 0;
    height: 30px;
    z-index: 1;
    text-align: center;
    vertical-align: middle;
}

.scrollnav a {
    width: 326px;
    font-size: 20px;
    margin: 0 auto;
    color: #959595;
    padding: 2px;
    display: block;
    float: left;
    text-decoration: none;
    margin-right: 6px;
    text-transform: uppercase;
    position: relative;
}

.scrollnav a:hover,
.scrollnav a.active {
    color: #109fb7;
    font-weight: 600;
}

/* .scrollnav a:hover:after,
.scrollnav a.active:after {
    content: '';
    width: 50px;
    height: 5px;
    background: #109fb7;
    display: block;
    position: absolute;
    right: 20px;
    top: 17px;
} */

.fixed {
    position: fixed;
    top: 69px;
}
.s-p-t-b {
    border-top: 3px solid #333;
}
.m-t {
    margin-top: 1.5em;
}
.scroll-btn {
    height: 33px;
}

    @media print {
        .noprint {
            display: none !important;
        }

        .print {
            display: block !important;
        }
    }

    .printable span{
        background: url('frontend/img/intro/print1.ico') no-repeat;
    }
    table.table {
        min-width: 0;
        font-size:12px;
    }
</style>
<main id="main">
    <!-- main tour information -->
    <div class="noprint">
        <section class="container-fluid trip-info">
            <div class="same-height two-columns row" style="padding-top: 140px">
                <div class="height col-md-6">
                    <!-- top image slideshow -->
                    <div id="tour-slide">
                        <div class="slide">
                            <div class="bg-stretch">
                                <img src="<?php echo URL; echo 'uploads/'.$package->image; ?>" height="1104"
                                     width="966" alt="image description">
                            </div>
                        </div>
                        <?php foreach($PackageImages as $image){ ?>
                        <div class="slide">
                            <div class="bg-stretch">
                                <img src="<?php echo URL; echo 'uploads/'.$image->image_name; ?>" alt="image descriprion" height="1104"
                                     width="966">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="height col-md-6 text-col">
                    <div class="holder">
                        <h1 class="small-size"><?php echo $package->title; ?> </h1>
<!--                        <div class="price">-->
<!--                            from <strong>US $979</strong>-->
<!--                        </div>-->
                        <div class="description">
                            <p><?php echo $package->summary; ?></p>
                        </div>
                        <ul class="reviews-info">
                            <li>
                                <div class="info-left">
                                    <strong class="title">Tour Length and Outline</strong>
                                    <span class="value"><?php echo $package->tour_length_outline; ?> </span>
                                </div>
                            </li>
                            <li>
                                <div class="info-left">
                                    <strong class="title">Best Time to Visit:</strong>
                                    <span class="value"><?php echo $package->best_time_to_visit; ?></span>
                                </div>
                            </li>
                            <li>
                                <div class="info-left">
                                    <strong class="title">Physical Demands</strong>
                                    <span class="value"><?php echo $package->physical_demand; ?></span>
                                </div>
                                <div class="info-right">
                                </div>
                            </li>
                            <li>

                            </li>
                        </ul>
                        <div class="btn-holder" style="float: left">
                            <a href="<?php echo URL; echo 'package_booking?package='.$package->id; ?>" target="_blank" class="btn btn-md btn-info">RESERVE THIS TRIP</a>
                        </div>
                        <div class="btn-holder">
                            <a href="<?php echo URL; echo 'package_trailor?package='.$package->id; ?>" target="_blank" class="btn btn-md btn-info">TAILOR THIS TRIP</a>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="tab-container">
            <!-- <nav class="nav-wrap" id="sticky-tab">
                <div class="container">
                    <ul class="nav nav-tabs text-center" role="tablist">
                        <li><a class="active" href="#tab01" aria-controls="tab01">Trip Overview</a></li>
                        <li><a href="#tab02" aria-controls="tab02">Day by Day</a>
                        </li>
                        <li><a href="#tab03" aria-controls="tab03">Inclusion & Exclusion</a>
                        <li>
                            <div class="btn-holder" data-toggle="modal" data-target="#reserve_trip">
                                <a href="#" class="btn btn-info">RESERVE THIS TRIP</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav> -->
            <div class="scrollnav">
                <a href="#" data-scroll="toverview">Trip Overview</a>

                <a href="#" data-scroll="d-d">Day by Day</a>

                <a href="#" data-scroll="d-p">Inclusion & Exclusion</a>

                <a href="<?php echo URL; echo 'package_booking?package='.$package->id; ?>" target="_blank" class="btn btn-info scroll-btn">RESERVE THIS TRIP</a>
            </div>


            <div class="wrapper container">
            <section id="toverview" data-anchor="toverview">
                <div class="row">
                    <h2 class="heading m-t">Trip Overview</h2>
                    <div class="col-md-12">
                        <div class="detail">
                            <?php echo $package->description; ?>
                        </div>
                    </div>
                </div>
            </section>

            <section id="d-d" class="s-p-t-b" data-anchor="d-d">
                <div class="row">
                    <h2 class="heading m-t">Day by Day</h2>
                    <div class="col-md-12">
                        <ol class="detail-accordion">
                            <?php foreach($itenaries as $package_itenary){?>
                                <li class="active col-sm-12">
                                    <a>
                                        <!-- <strong class="title"><?php echo $package_itenary->name; ?></strong> -->
                                        <strong class="title"><b><?php echo $package_itenary->title; ?></b></strong>
                                    </a>
                                    <div class="">
                                        <div class="slide-holder">

                                            <div <?php if($package_itenary->image){ echo 'class="col-sm-6"'; }else{ echo 'class="col-sm-12"'; } ?> align="justify">
                                                <p><?php echo $package_itenary->description; ?></p>
                                            </div>
                                            <?php if($package_itenary->image){ ?>
                                            <div class="col-sm-6">
                                                <div class="img-wrap" style="max-height:280px; max-width:500px;overflow:hidden;">
                                                    <?php if($package_itenary->image){ ?>
                                                    <img src="<?php echo URL; echo 'uploads/'.$package_itenary->image; ?>"  height="220" width="350" alt="">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ol>
                    </div>
                </div>
            </section>

            <section id="d-p" class="s-p-t-b" data-anchor="d-p">
                <div class="row">
                    <h2 class="heading m-t">Inclusion and Exclusion</h2>
                    <div class="row">
                            <!-- included and not-included -->
                            <?php foreach($I_Es as $package_IE){ ?>
                            <div class="col-md-6">
                                <div class="text-box">
                                    <div class="holder">
                                        <strong class="title"><?php echo $package_IE->title; ?></strong>
                                        <div>
                                            <?php echo $package_IE->description; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>




        <!-- tab panes -->
        <div class="print">
            <div class="container tab-content trip-detail" style="text-align: justify">
                <div class="container">
                    <h2 class="text-center text-uppercase">RECENTLY VIEWED</h2>
                    <div class="row" id="guide-slide">
                        <?php foreach($recent_packages as $recent_package){ ?>
                        <article class="article">
                            <div class="thumbnail">
                                <h3 class="no-space"><a href="<?php echo URL; echo 'servicePackageDetail?package='.$recent_package->id; ?>"><?php echo $recent_package->title; ?></a></h3>
                                <strong class="info-title"></strong>
                                <div class="img-wrap">
                                    <a>
                                    <img src="<?php echo URL; echo 'uploads/'.$recent_package->image; ?>" height="228" alt="image description">
                                    </a>
                                </div>
                                <footer>
                                    <span class="price">Price:<span><?php echo $recent_package->price; ?></span></span>
                                </footer>
                            </div>
                        </article>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- reserve trip modal -->
    <?php include 'modal/reserve_trip_form.php'; ?>
    <?php include 'modal/tailor_trip_form.php'; ?>
</main>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    function myFunction() {
        window.print();
    }
    $('.scrollnav a').on('click', function() {
        var scrollAnchor = $(this).attr('data-scroll'),
            scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 28;
        $('body,html').animate({
            scrollTop: scrollPoint
        }, 500);
        return false;
    })
    $(window).scroll(function() {
        var windscroll = $(window).scrollTop();
        if (windscroll >= 700) {
            $('.scrollnav').addClass('fixed');
            $('.wrapper section').each(function(i) {
                if ($(this).position().top <= windscroll - 20) {
                    $('.scrollnav a.active').removeClass('active');
                    $('.scrollnav a').eq(i).addClass('active');
                }
            });

        } else {

            $('.scrollnav').removeClass('fixed');
            $('.scrollnav a.active').removeClass('active');
            $('.scrollnav a:first').addClass('active');
        }

    }).scroll();
</script>
