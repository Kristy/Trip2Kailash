<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="list-view-detail">
    <div class="banner-text">
        <div class="center-text">
            <div class="container">
                <h1><?php if(isset($destination_detail->name)){ if($destination_detail->name){ echo $destination_detail->name; } } ?></h1>
                <!-- breadcrumb -->
            </div>
        </div>
    </div>
</section>
<!-- main container -->
<main id="main" style="text-align: justify">
    <div class="content-block bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $destination_detail->description; ?>
                </div>
                <div class="content-holder content-sub-holder">
                    <div class="row db-3-col">
                        <?php foreach ($activities as $activity) { ?>
                            <article class="col-md-6 col-lg-4 article has-hover-s1 thumb-full">
                                <div class="thumbnail">
                                    <div class="img-wrap" style="height:200px; overflow:hidden;">
                                        <img src="<?php echo URL; echo 'uploads/'.$activity->image; ?>" height="228" width="350" alt="image description">
                                    </div>
                                    <h3 class="small-space"><a href="<?php echo URL;echo 'package?activity=' . $activity->id ?>"><?php echo $activity->title; ?></a></h3>
                                    <p><?php echo substr($activity->description, 0, 200); ?>...</p>
                                    <a href="<?php echo URL;echo 'package?activity=' . $activity->id ?>" class="btn btn-default">Explore</a>
                                </div>
                            </article>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- recent block -->
    <?php if($guides){ ?>
    <div class="content-holder content-sub-holder">
        <h2 class="heading center" align="center"><?php echo $destination_detail->name; ?> Travel Guide</h2>
        <div class="row db-3-col" id="guide-slide">
            <?php foreach ($guides as $guide) { ?>
                <article class="article">
                    <div class="thumbnail">
                        <h3 class="small-space"><a href="<?php echo URL; echo 'service_guide?guide='.$guide->id; ?>"><?php echo $guide->title; ?></a></h3>
                        <strong class="info-title"></strong>
                        <div class="img-wrap" style="width:350px; height:200px; max-height:200px; max-width:350px; overflow:hidden;">
                            <a>
                            <img src="<?php echo URL; echo 'uploads/'.$guide->image; ?>" height="228" width="350" alt="image description">
                            </a>
                        </div>

                    </div>
                </article>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</main>
