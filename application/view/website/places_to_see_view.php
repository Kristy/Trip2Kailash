<!-- main container -->


    <!-- content with sidebar -->
    <section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" style="background-image:url('<?php echo $image_theme; ?>');">
        <div class="banner-text">
            <div class="center-text">
                <div class="container">
                    <h1><?php if(isset($place_to_see->title)){ if($place_to_see->title){ echo $place_to_see->title; } } ?></h1>
                </div>
            </div>
        </div>
    </section>
    <!-- main container -->
    <main id="main" style="text-align: justify">
        <div class="content-block bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-9 text-holder">
                        <?php if(isset($place_to_see->description)){ if($place_to_see->description){ echo $place_to_see->description; } } ?>
                    </div>
                    <div class="col-sm-4 col-md-3 map-col">
                        <div class="holder">

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </main>
