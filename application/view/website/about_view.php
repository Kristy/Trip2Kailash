<style>
    .parallax {
        overflow: hidden;
        background-repeat: no-repeat;
        background-color: transparent;
        background-attachment: initial;
        background-size: cover;
        background-position: 50% 0;
        z-index: 1;
        position: relative;
    }
</style>
<!-- main banner -->
<?php if($image_theme){ ?>
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" style="background-image:url('<?php echo $image_theme; ?>');">
</section>
<?php } ?>
<main id="main" style="text-align: justify">
    <div class="content-block bg-gray">
        <div class="container">
            <header class="content-heading">
                <h2 class="main-heading"><?php echo $title; ?></h2>
                <strong class="main-subtitle"><?php echo $quote; ?></strong>
                <div class="seperator"></div>
            </header>
            <div class="row">
                <div class="col-sm-6 description-text">
                    <h3><?php echo $left_title; ?></h3>
                    <p><?php echo $left_description; ?></p>
                </div>
                <div class="col-sm-6 description-text">
                    <h3><?php echo $right_title; ?></h3>
                    <p><?php echo $right_description; ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- services block -->
    <!-- <div class="services-block has-bg parallax" id="about-service-block">
        <div class="container">
            <div class="row db-3-col">
                <div class="col-sm-4 ico-article wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                    <div class="ico-holder">
                        <span class="icon-hiking"></span>
                    </div>
                    <div class="description">
                        <strong class="content-title"><a href="<?php echo URL; ?>activities">HIKING</a></strong>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                            Aenean sollicitu</p>
                    </div>
                </div>
                <div class="col-sm-4 ico-article wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">
                    <div class="ico-holder">
                        <span class="icon-boat"></span>
                    </div>
                    <div class="description">
                        <strong class="content-title"><a href="<?php echo URL; ?>activities">RAFTING</a></strong>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                            Aenean sollicitu</p>
                    </div>
                </div>
                <div class="col-sm-4 ico-article wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
                    <div class="ico-holder">
                        <span class="icon-mountain-biking"></span>
                    </div>
                    <div class="description">
                        <strong class="content-title"><a href="<?php echo URL; ?>activities">CYCLING/BIKING</a></strong>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                            Aenean sollicitu</p>
                    </div>
                </div>
                <div class="col-sm-4 ico-article wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms">
                    <div class="ico-holder">
                        <span class="icon-peak"></span>
                    </div>
                    <div class="description">
                        <strong class="content-title"><a href="<?php echo URL; ?>activities">TREKING</a></strong>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                            Aenean sollicitu</p>
                    </div>
                </div>
                <div class="col-sm-4 ico-article wow fadeInUp" data-wow-duration="400ms" data-wow-delay="600ms">
                    <div class="ico-holder">
                        <span class="icon-wildlife"></span>
                    </div>
                    <div class="description">
                        <strong class="content-title"><a href="<?php echo URL; ?>activities">JUNGLE SAFARI</a></strong>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                            Aenean sollicitu</p>
                    </div>
                </div>
                <div class="col-sm-4 ico-article wow fadeInUp" data-wow-duration="400ms" data-wow-delay="700ms">
                    <div class="ico-holder">
                        <span class="glyphicon glyphicon-plane"></span>
                    </div>
                    <div class="description">
                        <strong class="content-title"><a href="<?php echo URL; ?>activities">MOUNTAIN FLIGHTS</a></strong>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                            Aenean sollicitu</p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- content block with guide info -->

    <section class="content-block guide-sub guide-add bg-white">
        <?php if($national_members){ ?>
        <div class="inner-main container btn-demo-wrapper">
            <div class="container">
                <header class="content-heading">
                    <h2 class="main-heading">MEET OUR TEAMS</h2>
                    <div class="seperator"></div>
                </header>
                <div class="content-holder">
                    <div class="row">
                        <?php
                        foreach($national_members as $member){
                        ?>
                        <div class="col-sm-6 col-md-4 img-article">
                            <div class="holder">
                                <div class="img-wrap">
                                    <img src="<?php echo URL; echo 'uploads/'.$member->image; ?>" height="436"
                                         alt="image description">
                                    <ul class="social-networks">
                                        <li><a href="#"><span class="icon-email"></span></a></li>
                                    </ul>
                                </div>
                                <div class="caption">
                                    <h3 class="small-space"><?php echo $member->name?></h3>
                                    <span class="designation"><?php echo $member->post?></span>
                                </div>
                            </div>
                        </div>
                        <?php
                        }

                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php }
        if($international_members){ ?>
        <div class="inner-main  container btn-demo-wrapper">
            <div class="container">
                <header class="content-heading">
                    <h2 class="main-heading">INTERNATIONAL REPRESENTATIVE</h2>
                    <div class="seperator"></div>
                </header>
                <div class="content-holder">
                    <div class="row">
                        <?php
                        foreach($international_members as $member){
                        ?>
                        <div class="col-sm-6 col-md-4 img-article">
                            <div class="holder">
                                <div class="img-wrap">
                                    <img src="<?php echo URL; echo 'uploads/'.$member->image; ?>" height="436" width="370"
                                         alt="image description">
                                    <ul class="social-networks">
                                        <li><a href="#"><span class="icon-email"></span></a></li>
                                    </ul>
                                </div>
                                <div class="caption">
                                    <h3 class="small-space"><?php echo $member->name?></h3>
                                    <span class="designation"><?php echo $member->post?></span>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </section>


</main>
