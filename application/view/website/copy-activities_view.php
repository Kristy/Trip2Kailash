<!-- main banner -->
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="list-view-detail">
    <div class="banner-text">
        <div class="center-text">
            <div class="container">
                <h1>Hiking</h1>
                <strong class="subtitle">The most detailed and modern Adventure</strong>
                <!-- breadcrumb -->
                <nav class="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo URL; ?>home">HOME</a></li>
                        <li><a href="<?php echo URL; ?>about">About Us</a></li>
                        <li><a href="<?php echo URL; ?>activities">Activities</a></li>
                        <li><a href="#">Hiking</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- main container -->
<main id="main">
    <div class="content-intro">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-9 text-holder"style="text-align: justify">
                    <h2 class="title-heading">Discover Nepal with Hiking</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
                <div class="col-sm-4 col-md-3 map-col">
                    <div class="holder">
                        <div class="info">
                            <div class="slot">
                                <strong>Best Season:</strong>
                                <span class="sub">Jan-Feb, Mar-Jun, Oct-Dec</span>
                            </div>
                            <div class="slot">
                                <strong>Popular Location:</strong>
                                <span class="sub">Ghandruk, Poon Hill, Shivapuri</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content block -->
    <div class="content-block content-sub">
        <div class="container">
            <!-- list view -->
            <div class="content-holder list-view">
                <article class="article has-hover-s1">
                    <div class="thumbnail">
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-35.jpg" height="240" alt="image description">
                        </div>
                        <div class="description">
                            <div class="col-left">
                                <header class="heading">
                                    <h3><a href="#">Beach &amp; Resort Holiday in Thailand</a></h3>
                                    <div class="info-day">17 Days</div>
                                </header>
                                <p style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                <div class="reviews-holder">
                                    <div class="star-rating">
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span class="disable"><span class="icon-star"></span></span>
                                    </div>
                                    <div class="info-rate">Based on 75 Reviews</div>
                                </div>
                                <footer class="info-footer">
                                    <ul class="ico-list">
                                        <li class="pop-opener">
                                            <span class="icon-beach"></span>
                                            <div class="popup">
                                                Beach
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-boat"></span>
                                            <div class="popup">
                                                Boat
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-food-wine"></span>
                                            <div class="popup">
                                                Food Wine
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-water"></span>
                                            <div class="popup">
                                                Water
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="ico-action">
                                        <li>
                                            <a href="#"><span class="icon-share"></span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="icon-favs"></span></a>
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                            <aside class="info-aside">
                                <span class="price">from <span>$2749</span></span>
                                <div class="activity-level">
                                    <div class="ico">
                                        <span class="icon-level2"></span>
                                    </div>
                                    <span class="text">EASY - LEISURLY</span>
                                </div>
                                <a href="#" class="btn btn-default">explore</a>
                            </aside>
                        </div>
                    </div>
                </article>
                <article class="article has-hover-s1">
                    <div class="thumbnail">
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-36.jpg" height="240" width="350" alt="image description">
                        </div>
                        <div class="description">
                            <div class="col-left">
                                <header class="heading">
                                    <h3><a href="#">Beach &amp; Resort Holiday in Thailand</a></h3>
                                    <div class="info-day">17 Days</div>
                                </header>
                                <p style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                <div class="reviews-holder">
                                    <div class="star-rating">
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span class="disable"><span class="icon-star"></span></span>
                                    </div>
                                    <div class="info-rate">Based on 75 Reviews</div>
                                </div>
                                <footer class="info-footer">
                                    <ul class="ico-list">
                                        <li class="pop-opener">
                                            <span class="icon-beach"></span>
                                            <div class="popup">
                                                Beach
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-boat"></span>
                                            <div class="popup">
                                                Boat
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-food-wine"></span>
                                            <div class="popup">
                                                Food Wine
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-water"></span>
                                            <div class="popup">
                                                Water
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="ico-action">
                                        <li>
                                            <a href="#"><span class="icon-share"></span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="icon-favs"></span></a>
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                            <aside class="info-aside">
                                <span class="price">from <span>$2749</span></span>
                                <div class="activity-level">
                                    <div class="ico">
                                        <span class="icon-level4"></span>
                                    </div>
                                    <span class="text">EASY - LEISURLY</span>
                                </div>
                                <a href="#" class="btn btn-default">explore</a>
                            </aside>
                        </div>
                    </div>
                </article>
                <article class="article has-hover-s1">
                    <div class="thumbnail">
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-37.jpg" height="240" width="350" alt="image description">
                        </div>
                        <div class="description">
                            <div class="col-left">
                                <header class="heading">
                                    <h3><a href="#">Beach &amp; Resort Holiday in Thailand</a></h3>
                                    <div class="info-day">17 Days</div>
                                </header>
                                <p style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                <div class="reviews-holder">
                                    <div class="star-rating">
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span class="disable"><span class="icon-star"></span></span>
                                    </div>
                                    <div class="info-rate">Based on 75 Reviews</div>
                                </div>
                                <footer class="info-footer">
                                    <ul class="ico-list">
                                        <li class="pop-opener">
                                            <span class="icon-beach"></span>
                                            <div class="popup">
                                                Beach
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-boat"></span>
                                            <div class="popup">
                                                Boat
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-food-wine"></span>
                                            <div class="popup">
                                                Food Wine
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-water"></span>
                                            <div class="popup">
                                                Water
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="ico-action">
                                        <li>
                                            <a href="#"><span class="icon-share"></span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="icon-favs"></span></a>
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                            <aside class="info-aside">
                                <span class="price">from <span>$2749</span></span>
                                <div class="activity-level">
                                    <div class="ico">
                                        <span class="icon-level5"></span>
                                    </div>
                                    <span class="text">EASY - LEISURLY</span>
                                </div>
                                <a href="#" class="btn btn-default">explore</a>
                            </aside>
                        </div>
                    </div>
                </article>
                <article class="article has-hover-s1">
                    <div class="thumbnail">
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-38.jpg" height="240" width="350" alt="image description">
                        </div>
                        <div class="description">
                            <div class="col-left">
                                <header class="heading">
                                    <h3><a href="#">Beach &amp; Resort Holiday in Thailand</a></h3>
                                    <div class="info-day">17 Days</div>
                                </header>
                                <p style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                <div class="reviews-holder">
                                    <div class="star-rating">
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span class="disable"><span class="icon-star"></span></span>
                                    </div>
                                    <div class="info-rate">Based on 75 Reviews</div>
                                </div>
                                <footer class="info-footer">
                                    <ul class="ico-list">
                                        <li class="pop-opener">
                                            <span class="icon-beach"></span>
                                            <div class="popup">
                                                Beach
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-boat"></span>
                                            <div class="popup">
                                                Boat
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-food-wine"></span>
                                            <div class="popup">
                                                Food Wine
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-water"></span>
                                            <div class="popup">
                                                Water
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="ico-action">
                                        <li>
                                            <a href="#"><span class="icon-share"></span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="icon-favs"></span></a>
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                            <aside class="info-aside">
                                <span class="price">from <span>$2749</span></span>
                                <div class="activity-level">
                                    <div class="ico">
                                        <span class="icon-level7"></span>
                                    </div>
                                    <span class="text">EASY - LEISURLY</span>
                                </div>
                                <a href="#" class="btn btn-default">explore</a>
                            </aside>
                        </div>
                    </div>
                </article>
                <article class="article has-hover-s1">
                    <div class="thumbnail">
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-39.jpg" height="240" width="350" alt="image description">
                        </div>
                        <div class="description">
                            <div class="col-left">
                                <header class="heading">
                                    <h3><a href="#">Beach &amp; Resort Holiday in Thailand</a></h3>
                                    <div class="info-day">17 Days</div>
                                </header>
                                <p style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                <div class="reviews-holder">
                                    <div class="star-rating">
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span><span class="icon-star"></span></span>
                                        <span class="disable"><span class="icon-star"></span></span>
                                    </div>
                                    <div class="info-rate">Based on 75 Reviews</div>
                                </div>
                                <footer class="info-footer">
                                    <ul class="ico-list">
                                        <li class="pop-opener">
                                            <span class="icon-beach"></span>
                                            <div class="popup">
                                                Beach
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-boat"></span>
                                            <div class="popup">
                                                Boat
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-food-wine"></span>
                                            <div class="popup">
                                                Food Wine
                                            </div>
                                        </li>
                                        <li class="pop-opener">
                                            <span class="icon-water"></span>
                                            <div class="popup">
                                                Water
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="ico-action">
                                        <li>
                                            <a href="#"><span class="icon-share"></span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="icon-favs"></span></a>
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                            <aside class="info-aside">
                                <span class="price">from <span>$2749</span></span>
                                <div class="activity-level">
                                    <div class="ico">
                                        <span class="icon-level8"></span>
                                    </div>
                                    <span class="text">EASY - LEISURLY</span>
                                </div>
                                <a href="#" class="btn btn-default">explore</a>
                            </aside>
                        </div>
                    </div>
                </article>
            </div>
            <!-- pagination wrap -->
            <nav class="pagination-wrap">
                <div class="btn-prev">
                    <a href="#" aria-label="Previous">
                        <span class="icon-angle-right"></span>
                    </a>
                </div>
                <ul class="pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li class="active"><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>...</li>
                    <li><a href="#">7</a></li>
                </ul>
                <div class="btn-next">
                    <a href="#" aria-label="Previous">
                        <span class="icon-angle-right"></span>
                    </a>
                </div>
            </nav>
        </div>
    </div>
    <!-- recent block -->
    <aside class="recent-block recent-list recent-wide-thumbnail">
        <div class="container">
            <h2 class="text-center text-uppercase">RECENTLY VIEWED</h2>
            <div class="row">
                <article class="col-sm-6 col-md-3 article">
                    <div class="thumbnail">
                        <h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
                        <strong class="info-title">Everest Region, Nepal</strong>
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-31.jpg" height="210" width="250" alt="image description">
                        </div>
                        <footer>
                            <div class="sub-info">
                                <span>5 Days</span>
                                <span>$299</span>
                            </div>
                            <ul class="ico-list">
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-hiking"></span>
                                        <span class="popup">
														Hiking
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-mountain"></span>
                                        <span class="popup">
														Mountain
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-level5"></span>
                                        <span class="popup">
														Level 5
													</span>
                                    </a>
                                </li>
                            </ul>
                        </footer>
                    </div>
                </article>
                <article class="col-sm-6 col-md-3 article">
                    <div class="thumbnail">
                        <h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
                        <strong class="info-title">Everest Region, Nepal</strong>
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-32.jpg" height="210" width="250" alt="image description">
                        </div>
                        <footer>
                            <div class="sub-info">
                                <span>5 Days</span>
                                <span>$299</span>
                            </div>
                            <ul class="ico-list">
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-hiking"></span>
                                        <span class="popup">
														Hiking
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-mountain"></span>
                                        <span class="popup">
														Mountain
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-level5"></span>
                                        <span class="popup">
														Level 5
													</span>
                                    </a>
                                </li>
                            </ul>
                        </footer>
                    </div>
                </article>
                <article class="col-sm-6 col-md-3 article">
                    <div class="thumbnail">
                        <h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
                        <strong class="info-title">Everest Region, Nepal</strong>
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-33.jpg" height="210" width="250" alt="image description">
                        </div>
                        <footer>
                            <div class="sub-info">
                                <span>5 Days</span>
                                <span>$299</span>
                            </div>
                            <ul class="ico-list">
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-hiking"></span>
                                        <span class="popup">
														Hiking
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-mountain"></span>
                                        <span class="popup">
														Mountain
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-level5"></span>
                                        <span class="popup">
														Level 5
													</span>
                                    </a>
                                </li>
                            </ul>
                        </footer>
                    </div>
                </article>
                <article class="col-sm-6 col-md-3 article">
                    <div class="thumbnail">
                        <h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
                        <strong class="info-title">Everest Region, Nepal</strong>
                        <div class="img-wrap">
                            <img src="frontend/img/listing/img-34.jpg" height="210" width="250" alt="image description">
                        </div>
                        <footer>
                            <div class="sub-info">
                                <span>5 Days</span>
                                <span>$299</span>
                            </div>
                            <ul class="ico-list">
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-hiking"></span>
                                        <span class="popup">
														Hiking
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-mountain"></span>
                                        <span class="popup">
														Mountain
													</span>
                                    </a>
                                </li>
                                <li class="pop-opener">
                                    <a href="#">
                                        <span class="icon-level5"></span>
                                        <span class="popup">
														Level 5
													</span>
                                    </a>
                                </li>
                            </ul>
                        </footer>
                    </div>
                </article>
            </div>
        </div>
    </aside>
</main>