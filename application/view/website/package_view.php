<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar">
    <div class="banner-text">
        <div class="center-text">
            <div class="container">
                <h1><?php echo $activity->title; ?></h1>
                <strong class="subtitle">Package Lists</strong>
            </div>
        </div>
    </div>
</section>
<!-- main container -->
<main id="main">
    <!-- content with sidebar -->
    <div class="bg-gray content-with-sidebar grid-view-sidebar">
        <div class="container">
            <div id="two-columns" class="row">
                <!-- sidebar -->
                <div id="content" class="col-md-12 col-lg-12">
                    </div>
                    <div class="content-holder content-sub-holder">
                        <div class="row db-3-col">
                            <?php foreach($packages as $package){ ?>
                            <article class="col-md-6 col-lg-4 article has-hover-s1 thumb-full">
                                <div class="thumbnail">
                                    <div class="img-wrap" style="height:200px; overflow:hidden;">
                                        <img src="<?php echo URL; echo 'uploads/'.$package->image; ?>" height="228" width="350" alt="image description">
                                    </div>
                                    <h3 class="small-space"><a href="<?php echo URL;echo 'servicePackageDetail?package=' . $package->id ?>"><?php echo $package->title; ?></a></h3>
                                    <p><?php echo substr($package->summary, 0, 150); ?></p>
                                    <a href="<?php echo URL;echo 'servicePackageDetail?package=' . $package->id ?>" class="btn btn-default">explore</a>
                                    <footer>
                                        <span class="price">Price:<span><?php echo $package->price; ?></span></span>
                                    </footer>
                                </div>
                            </article>
                            <?php } ?>
                        </div>
                    </div>
<!--                    <!-- pagination wrap -->
<!--                    <nav class="pagination-wrap">-->
<!--                        <div class="btn-prev">-->
<!--                            <a href="#" aria-label="Previous">-->
<!--                                <span class="icon-angle-right"></span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <ul class="pagination">-->
<!--                            <li><a href="#">1</a></li>-->
<!--                            <li><a href="#">2</a></li>-->
<!--                            <li><a href="#">3</a></li>-->
<!--                            <li class="active"><a href="#">4</a></li>-->
<!--                            <li><a href="#">5</a></li>-->
<!--                            <li>...</li>-->
<!--                            <li><a href="#">7</a></li>-->
<!--                        </ul>-->
<!--                        <div class="btn-next">-->
<!--                            <a href="#" aria-label="Previous">-->
<!--                                <span class="icon-angle-right"></span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </nav>-->
                </div>
            </div>
        </div>
    </div>
    <!-- recent block -->
    <aside class="recent-block recent-list ">
        <div class="container">
            <h2 class="text-center text-uppercase">RECENTLY VIEWED</h2>
            <div class="row" id="guide-slide">
                <?php foreach($recent_packages as $recent_package){ ?>
                <article class="article">
                    <div class="thumbnail">
                        <h3 class="no-space"><a href="<?php echo URL; echo 'servicePackageDetail?package='.$recent_package->id; ?>"><?php echo $recent_package->title; ?></a></h3>
                        <strong class="info-title"></strong>
                        <div class="img-wrap" style="height:210px; width:250px; overflow:hidden;">
                            <a>
                            <img src="<?php echo URL; echo 'uploads/'.$recent_package->image; ?>" height="210" width="250" alt="image description">
                            </a>
                        </div>
                        <footer>
                            <span class="price">Price:<span><?php echo $recent_package->price; ?></span></span>
                        </footer>

                    </div>
                </article>
                <?php } ?>
            </div>
        </div>
    </aside>
</main>
