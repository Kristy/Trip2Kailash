<!-- main container -->


    <!-- content with sidebar -->
    <section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" style="background-image:url('<?php echo 'uploads/'.$kailash_guide->image; ?>');">
        <div class="banner-text">
            <div class="center-text">
                <div class="container">
                    <h1><?php if(isset($kailash_guide->title)){ if($kailash_guide->title){ echo $kailash_guide->title; } } ?></h1>
                    <!-- breadcrumb -->
                </div>
            </div>
        </div>
    </section>
    <!-- main container -->
    <main id="main" style="text-align: justify">
        <div class="content-block bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-9 text-holder">
                        <?php if(isset($kailash_guide->description)){ if($kailash_guide->description){ echo $kailash_guide->description; } } ?>
                    </div>
                    <div class="col-sm-4 col-md-3 map-col">
                        <div class="holder">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-holder content-sub-holder">
            <h2 class="heading center" align="center">Kailash Travel Guide</h2>
            <div class="row db-3-col" id="guide-slide">
                <?php foreach ($kailash_guides as $kailash_guide) { ?>
                    <article class="article">
                        <div class="thumbnail">
                            <h3 class="small-space"><a href="<?php echo URL; echo 'kailash_guide?guide='.$kailash_guide->id; ?>"><?php echo $kailash_guide->title; ?></a></h3>
                            <strong class="info-title"></strong>
                            <div class="img-wrap" style="height:200px; max-height:200px">
                                <a>
                                <img src="<?php echo URL; echo 'uploads/'.$kailash_guide->image; ?>" height="228" width="350" alt="image description">
                                </a>
                            </div>

                        </div>
                    </article>
                <?php } ?>
            </div>
        </div>

    </main>
