<!DOCTYPE HTML>
<html class="no-js" lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trip to Kailash</title>
    <!-- <meta name="robots" content="noindex" /> -->
    <!-- <META NAME="geo.position" CONTENT="27.72422; 85.322">
    <META NAME="geo.placename" CONTENT="Lazimpat, Kathmandu">
    <META NAME="geo.region" CONTENT="Nepal"> -->
    <meta name="keywords" content="kailash-travel, kailash-manasarovar, trip2kailash, trip-to-kailash, kailash-yattra, kailash-yattra-2018, kailash-tour, explore-kailash, kailash-journey, visit-kailash, mount-kailash, mount-kailash-yattra, mount-kailash-tour"/>
    <meta name="description" content="Trip to kailash Pvt. Ltd. with a team lead by the young professional in inbound and outbound tourism field lead with expert Tours and Trekking guide working more than decades and done more than 100 trips with Excellency."/>

    <!--    <!-- favion -->
    <!--    <link rel="icon" type="image/png" sizes="16x16" href="frontend/img/favicon-16x16.png">-->
    <!-- link to font awesome -->
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>frontend/vendors/font-awesome/css/font-awesome.css">
    <!-- link to custom icomoon fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>frontend/css/fonts/icomoon/icomoon.css">
    <!-- link to wow js animation -->
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>frontend/vendors/animate/animate.css">
    <!-- include bootstrap css -->
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>frontend/css/bootstrap.css">
    <!-- include owl css -->
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>frontend/vendors/owl-carousel/owl.carousel.css">
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>frontend/vendors/owl-carousel/owl.theme.css">
    <!-- link to revolution css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>frontend/vendors/revolution/css/settings.css">
    <!-- include main css -->
    <?php if(isset($is_calender)){ ?>
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>fullcalendar-3.9.0/fullcalendar.min.css">
    <?php } ?>
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>frontend/css/main.css">
    <link media="all" rel="stylesheet" href="<?php echo URL; ?>frontend/css/custom.css">
    <style>
    #limit{
        height:98px;
        overflow:hidden;
    }
    .custom-hide-show{
        text-decoration: underline;
    }
    .custom-hide-show:hover{
        cursor:pointer;
    }
    </style>
    <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class="preloader" id="pageLoad">
   <div class="holder">
       <div class="coffee_cup"></div>
   </div>
</div>

<!-- main wrapper -->
<div id="wrapper">
    <div class="page-wrapper">
        <div class="header-top">
                    <div class="container">
                        <ul class="top-user-panel">
                            <li class="wrap-text"><span class="icon-tel" style="color: #fff;"></span> <a href="tel:9803461444">+977 9803461444</a></li>
                            <li class="wrap-text"><span class="icon-email" style="color: #fff;"></span> <a href="mailto:contact@trip2kailash.com">contact@trip2kailash.com</a></li>
                        </ul>
                        <ul class="top-right-panel">
                            <li class="dropdown hidden-xs hidden-sm last-dropdown v-divider">
                                <div id="google_translate_element"></div>
                                <!-- <a href="#"><span class="text">EN</span></a> -->
                                <!-- <div class="dropdown-menu dropdown-sm">
                                    <div class="drop-wrap lang-wrap">
                                        <div class="lang-row">
                                            <div class="lang-col">
                                                <a href="#">
                                                    <span class="text">English</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="lang-row">
                                            <div class="lang-col">
                                                <a href="#">
                                                    <span class="text">Chinese</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </li>
                        </ul>
                    </div>
                </div>
        <header id="header" class="white-header">

				<div class="header-bottom">

                    <div class="container head-container">
                        <!-- logo -->
                        <div class="logo">
                            <a href="<?php echo URL; ?>home">
                                <img class="normal" style="margin-top:-10%" src="<?php echo URL; ?>img/logo.jpg" alt="Tript2Kailash">
                                <img class="gray-logo" style="margin-top:-10%;"  src="<?php echo URL; ?>img/logo.jpg" alt="Tript2Kailash">

                            </a>
                        </div>
                        <!-- main navigation -->
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle nav-opener" data-toggle="collapse" data-target="#nav">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <!-- main menu items and drop for mobile -->
                            <div class="collapse navbar-collapse" id="nav">
                                <!-- main navbar -->
                                <ul class="nav navbar-nav">
                                    <li><a href="<?php echo URL; ?>home">Home</a></li>
                                    <li>
                                        <a href="<?php echo URL; ?>kailash_tour_list">Kailash Tour</a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Destinations <b class="icon-angle-down"></b></a>
                                        <div class="dropdown-menu">
                                            <ul class="nav  nav-hover" role="">
                                                <?php foreach($this->destinations as $header_destination){ ?>
                                                <li class="active"><a class="title" href="<?php echo URL; echo 'activities?destination='.$header_destination->id; ?>" ><?php echo $header_destination->name; ?></a></li>
                                                <?php } ?>
                                            </ul>

                                        </div>
                                    </li>
                                    <li class="dropdown">
									    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Essential Info<b class="icon-angle-down"></b></a>
                                        <div class="dropdown-menu">
                                            <ul class="nav  nav-hover" role="">
                                                <?php foreach($this->header_essential_infos as $header_essential){ ?>
                                                <li class="active"><a class="title" href="<?php echo URL; echo 'essentialInfo?info='.$header_essential->id; ?>" ><?php echo $header_essential->title; ?></a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a href="<?php echo URL; ?>about">About Us</a></li>
                                    <li><a href="<?php echo URL; ?>contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
				</div>
			</header>
