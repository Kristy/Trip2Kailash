
<div id="wrapper">
    <footer id="footer">
        <div class="container">
            <!-- footer links -->
            <div class="row footer-holder">
                <nav class="col-sm-4 col-lg-2 footer-nav">
                    <h3>Destinations</h3>
                    <ul class="slide">
                        <?php foreach($this->destinations as $destination){ ?>
                        <li><a href="<?php echo URL; echo 'activities?destination='.$destination->id; ?>"><?php echo $destination->name; ?></a></li>
                        <?php } ?>
                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-3 footer-nav">
                    <h3>Affiliated To</h3>
                    <ul class="slide">
                        <div class="accordion-group">
                            <div id="collapse4" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body" style="padding: 15px 15px 15px 0;">
                                    <ul class="side-list gallery-side-list horizontal-list">
                                        <?php foreach($this->affilateds as $affilated){ ?>
                                        <li class="affiliated-item">
                                            <a href="<?php echo $affilated->url; ?>" target="_blank">
                                                <img src="<?php echo URL; echo 'uploads/'.$affilated->image; ?>" height="55" width="55" alt="image">
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-3 footer-nav">
                    <h3>Places To See</h3>
                    <ul class="slide">
                        <?php foreach($this->attractions as $attra){ ?>
                        <li><a href="<?php echo URL; echo 'places_to_see?place='.$attra->id; ?>"><?php echo $attra->title; ?></a></li>
                        <?php } ?>
                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-3 footer-nav last">
                    <h3>contact Trip to Kailash</h3>
                    <ul class="slide address-block">
                        <li class="wrap-text"><span class="icon-tel"></span> <a href>+977 9803461444</a></li>
                        <li class="wrap-text"><span class="icon-email"></span> <a href>contact@trip2kailash.com</a></li>
                        <li><span class="icon-home"></span> <address>Lazimpat, Kathmandu</address></li>
                        <li>
                            <a href="https://www.facebook.com/groups/1246945795339534/" target="_blank" class="follow-us-btn"><span class="icon-facebook"></span>&nbsp; Follow Us On</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- copyright -->
                        <strong class="copyright"><i class="fa fa-copyright"></i> Copyright 2018 - Trip To Kailash <a href="#"></a></strong>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- scroll to top -->

<div class="scroll-holder text-center">
    <a href="javascript:" id="scroll-to-top"><i class="icon-arrow-down"></i></a>
</div>
</div>
</div>

<!-- jquery
<!-- jquery library -->
<script src="<?php echo URL; ?>frontend/vendors/jquery/jquery-2.1.4.min.js"></script>
<!-- <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script> -->
<!-- external scripts -->
<script src="<?php echo URL; ?>frontend/vendors/bootstrap/javascripts/bootstrap.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/jquery-placeholder/jquery.placeholder.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/match-height/jquery.matchHeight.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/wow/wow.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/stellar/jquery.stellar.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/validate/jquery.validate.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/waypoint/waypoints.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/counter-up/jquery.counterup.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/jQuery-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/fancybox/jquery.fancybox.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/jcf/js/jcf.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/jcf/js/jcf.select.js"></script>
<script src="<?php echo URL; ?>frontend/js/mailchimp.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/retina/retina.min.js"></script>
<script src="<?php echo URL; ?>frontend/vendors/bootstrap-datetimepicker-master/dist/js/bootstrap-datepicker.js"></script>
<!-- custom jquery script -->
<script src="<?php echo URL; ?>frontend/js/jquery.main.js?v=2.1"></script>

<!-- revolution slider plugin -->
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!-- rs5.0 core files -->
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/jquery.themepunch.tools.min838f.js?rev=5.0"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/jquery.themepunch.revolution.min838f.js?rev=5.0"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>frontend/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>

<script src="<?php echo URL; ?>frontend/vendors/sticky-kit/sticky-kit.js"></script>
<script src="<?php echo URL; ?>frontend/js/sticky-kit-init.js"></script>

<!-- revolutions slider script -->
<script src="<?php echo URL; ?>frontend/js/revolution.js"></script>\
<?php if(isset($is_calender)){ ?>
<script src="<?php echo URL; ?>moment/min/moment.min.js"></script>
<!-- <script src="<?php echo URL; ?>frontend/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>frontend/js/jquery-ui.custom.min.js"></script> -->

<script src="<?php echo URL; ?>fullcalendar-3.9.0/fullcalendar.min.js"></script>
<?php } ?>
<script>

  $(document).ready(function() {
        $('#calendar').fullCalendar({
        header: {
            left: 'today',
            center: 'title',
            right: 'prev,next'
            // right: 'month,agendaWeek,agendaDay,listWeek'
        },
        defaultDate: '2018-03-12',
        navLinks: true,
        editable: true,
        eventLimit: true,
        events: [
            <?php if(isset($bookings)){
                foreach($bookings as $booking){
            ?>
            {
              title: '<?php echo $booking->from_date; ?>',
              url: '<?php echo URL; echo 'kpBooking?kailash_date='.$booking->id; ?>',
              start: '<?php echo $booking->from_date; ?>',
            //   end: '<?php echo $booking->to_date; ?>'
            }
            <?php }} ?>
      ],
        eventClick: function(event) {
            if (event.url) {
                window.open(event.url, "_blank");
                return false;
            }
        }
    });

    });

</script>
<script>
$(function() {
     $("#slide-less").hide();
     $('#slide-more').click(function(e) {
        $("#slide-less").show();
        $("#slide-more").hide();
        e.stopPropagation();
        $('#limit').css("height", "100%");
        var h = $('#limit').height();
        $('#limit').css("height", "98px");
        $('#limit').animate({height: h}, 700, function() {
        });
        // $('#limit').animate({
        //     'height': newh
        // }, 1000);
    });

    $("#slide-less").click( function() {
        $("#slide-less").hide();
        $("#slide-more").show();
        $('#limit').animate({height: '98px'}, 700, function() {
        });
    });
});

</script>




</body>
</html>
