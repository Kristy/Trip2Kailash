<section class="content-block guide-sub guide-add bg-white">
    <div class="inner-main common-spacing container btn-demo-wrapper">
        <div class="container">
            <header class="content-heading" style="margin-top: 90px">
                <h2 class="main-heading">MEET OUR TEAMS</h2>
            </header>
            <div class="content-holder">
                <div class="row">
                    <?php
                        foreach($nmembers as $member){
                        ?>
                    <div class="col-sm-6 col-md-4 img-article">
                        <div class="holder">
                            <div class="img-wrap">
                                <img src="frontend/img/listing/img-13.jpg" height="436" width="370"
                                     alt="image description">
                                <ul class="social-networks">
                                    <li><a href="#"><span class="icon-email"></span></a></li>
                                </ul>
                            </div>
                            <div class="caption">
                                <h3 class="small-space"><?php echo $member->name?></h3>
                                <span class="designation"><?php echo $member->post?></span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                    
            </div>
        </div>
        <header class="content-heading" style="margin-top: 90px">
                <h2 class="main-heading">INTERNATIONAL REPRESENTATIVE</h2>
            </header>
            <div class="content-holder">
                <div class="row">
                    <?php
                        foreach($imembers as $member){
                        ?>
                    <div class="col-sm-6 col-md-4 img-article">
                        <div class="holder">
                            <div class="img-wrap">
                                <img src="frontend/img/listing/img-13.jpg" height="436" width="370"
                                     alt="image description">
                                <ul class="social-networks">
                                    <li><a href="#"><span class="icon-email"></span></a></li>
                                </ul>
                            </div>
                            <div class="caption">
                                <h3 class="small-space"><?php echo $member->name?></h3>
                                <span class="designation"><?php echo $member->post?></span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                    
            </div>
        </div>
    </div>
</section>
