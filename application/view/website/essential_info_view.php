<!-- main container -->


    <!-- content with sidebar -->
    <section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="list-view-detail">
        <div class="banner-text">
            <div class="center-text">
                <div class="container">
                    <h1><?php if(isset($essential_info->title)){ if($essential_info->title){ echo $essential_info->title; } } ?></h1>
                    <!-- breadcrumb -->
                </div>
            </div>
        </div>
    </section>
    <!-- main container -->
    <main id="main" style="text-align: justify">
        <div class="content-block bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-9 text-holder">
                        <?php if(isset($essential_info->description)){ if($essential_info->description){ echo $essential_info->description; } } ?>
                    </div>
                    <div class="col-sm-4 col-md-3 map-col">
                        <div class="holder">

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </main>
