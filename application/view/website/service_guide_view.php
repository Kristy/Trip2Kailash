<!-- main container -->


    <!-- content with sidebar -->
    <section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" style="background-image:url('<?php echo 'uploads/'.$guide->image; ?>');">
        <div class="banner-text">
            <div class="center-text">
                <div class="container">
                    <h1><?php if(isset($guide->title)){ if($guide->title){ echo $guide->title; } } ?></h1>
                    <!-- breadcrumb -->
                </div>
            </div>
        </div>
    </section>
    <!-- main container -->
    <main id="main" style="text-align: justify">
        <div class="content-block bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-9 text-holder">
                        <?php if(isset($guide->description)){ if($guide->description){ echo $guide->description; } } ?>
                    </div>
                    <div class="col-sm-4 col-md-3 map-col">
                        <div class="holder">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if($guides){ ?>
        <div class="content-holder content-sub-holder">
            <h2 class="heading center" align="center"><?php echo $guide->d_name; ?> Travel Guide</h2>
            <div class="row db-3-col" id="guide-slide">
                <?php foreach ($guides as $guide) { ?>
                    <article class="article">
                        <div class="thumbnail">
                            <h3 class="small-space"><a href="<?php echo URL; echo 'service_guide?guide='.$guide->id; ?>"><?php echo $guide->title; ?></a></h3>
                            <strong class="info-title"></strong>
                            <div class="img-wrap" style="width:350px; height:200px; max-height:200px; max-width:350px; overflow:hidden;">
                                <a>
                                <img src="<?php echo URL; echo 'uploads/'.$guide->image; ?>" height="228" width="350" alt="image description">
                                </a>
                            </div>

                        </div>
                    </article>
                <?php } ?>
            </div>
        </div>
        <?php } ?>

    </main>
