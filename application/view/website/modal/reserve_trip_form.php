<style>
    .demo-wrapper {
         padding-top: 0px;
         padding-bottom: 0px;
    }
</style>
<!-- Large modal -->
<div class="modal fade" id="reserve_trip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#5b99a3;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel" style="color: #f8f8f8"><b>Modal title</b></h3>
            </div>
            <div class="modal-body">
                <div class="form-holder">
                    <div class="wrap">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="tour_image">
                                    <img src="frontend/img/adventure-list/5.jpg" style="height: 150px; width: 150px">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <p><b>Tour Length &amp; Outline:</b>&nbsp;4 Days & 3 Nights</p>
                                <p><b>Best Time to Visit:</b>&nbsp;All year round</p>
                                <p><b>Physical Demands:</b>&nbsp;Very leisure tour; zero physical demand</p>
                            </div>
                        </div>
                        <div class="demo-wrapper">
                            <div class="seperator double-border"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="hold">
                                    <label for="name">First Name</label>
                                    <input type="text" id="name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="hold">
                                    <label for="lname">Last Name</label>
                                    <input type="text" id="lname" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="hold">
                            <label for="cname">Email</label>
                            <input type="email" id="email" class="form-control">
                        </div>
                        <div class="hold">
                            <label for="address">Please Confirm Your Email</label>
                            <input type="email" id="emailconfirm" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="hold">
                                    <label for="sel1">Select your Age</label>
                                    <select class="form-control" id="sel1">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="hold">
                                    <label for="phn">Nationality</label>
                                    <input type="text" id="phn" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="hold">
                                    <label for="phn">Phone Number</label>
                                    <input type="text" id="phn" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div id="datepicker12" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <label for="trip_date">When are you planning to visit?</label>
                                    <input class="form-control" type="text" readonly/>
                                    <span class="input-group-addon"><i class="icon-drop"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="hold">
                                    <label for="phn">How many people are in your group?</label>
                                    <input type="text" id="phn" class="form-control">
                                </div>
                            </div>

                            <div class="hold">
                                <label for="sel1">Adults</label>
                                <select class="form-control" id="sel1">
                                    <option>1</option>
                                    <option>2</option
                                </select>
                            </div>

                            <div class="hold">
                                <label for="sel1">Childern</label>
                                <select class="form-control" id="sel1">
                                    <option>1</option>
                                    <option>2</option
                                </select>
                            </div>
                                    
                        </div>
                        <div class="hold">
                            <ul class="option">
                                <label for="contact_them">What is the best way to contact you?</label>
                                <li>
                                    <header class="title">
                                        <label class="custom-radio">
                                            <input type="radio" name="pay">
                                            <span class="check-input"></span>
                                            <span class="check-label">Phone</span>
                                        </label>
                                    </header>
                                </li>
                                <li>
                                    <header class="title">
                                        <label class="custom-radio">
                                            <input type="radio" name="pay">
                                            <span class="check-input"></span>
                                            <span class="check-label">Email</span>
                                        </label>
                                    </header>
                                </li>
                            </ul>
                        </div>
                        <div class="wrap">
                            <div class="hold">
                                <label for="txt">Anything else we should know?</label>
                                <textarea id="txt" class="form-control" rows="4"
                                          placeholder="Please enter any additional information here, eg. food/drug requirements etc."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Cancle</button>
                <button type="button" class="btn btn-info-sub btn-md">Reserve</button>
            </div>
        </div>
    </div>
</div>