<style>
    .special-block {
        background: #306c76;
        padding: 30px 0;
        border: 20px solid #1c3b5a;
        color: #fff;
    }
    .gallery-list a:after {
        content: '';
        position: absolute;
        left: 0;
        right: -1px;
        top: 0;
        bottom: 0;
        background: #0d9fb78c;
        opacity: 0;
        -webkit-transition: opacity .2s linear;
        transition: opacity .2s linear;
    }
</style>

<div class="banner banner-home">
    <!-- revolution slider starts -->
    <div id="rev_slider_279_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="restaurant-header" style="margin:0px auto;background-color:#474d4b;padding:0px;margin-top:0px;margin-bottom:0px;">
        <div id="rev_slider_70_1" class="rev_slider fullscreenabanner" style="display:none;" data-version="5.1.4">
            <ul>
                <?php $i = 1; foreach($banners as $banner){ ?>
                <li class="slider-color-schema-dark" data-index="rs-<?php echo $i; ?>" data-transition="fade" data-slotamount="7" data-easein="default" data-easeout="default" data-masterspeed="1000" data-rotate="0" data-saveperformance="off" data-title="Slide" data-description="">
                    <!-- main image for revolution slider -->
                    <img src="<?php echo URL; echo 'uploads/'.$banner->image; ?>" alt="image description" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-bgfit="cover" data-no-retina>

                    <div class="tp-caption tp-resizeme" id="slide-897-layer<?php echo $i; ?>-7"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['top','top','middle','middle']"
                         data-voffset="['160','120','-120','-70']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power2.easeInOut;"
                         data-transform_out="opacity:0;s:300;s:300;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-start="1500"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 9; white-space: nowrap; font-size: 60px; line-height: 100px;text-align:center;">
                    </div>

                    <div class="tp-caption banner-heading-sub tp-resizeme rs-parallaxlevel-0"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['top','top','middle','middle']"
                         data-voffset="['280','240','10','20']"
                         data-fontsize="['48','48','44','28']"
                         data-lineheight="['85','85','50','50']"
                         data-width="['1200','1000','750','480']"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:0px;"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 7; letter-spacing: 0; font-weight: 100; text-align: center; color: #ffffff"><?php echo $banner->title; ?>
                    </div>

                    <div class="tp-caption banner-heading-sub tp-resizeme rs-parallaxlevel-10"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['top','top','middle','middle']"
                         data-voffset="['340','290','70','70']"
                         data-fontsize="['60','60','60','40']"
                         data-lineheight="['110','110','100','60']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 8; padding-right: 10px; text-indent: 5px; font-weight: 900; white-space: nowrap;"><?php echo $banner->highlight; ?>
                    </div>

                    <div class="tp-caption rev-btn  rs-parallaxlevel-10" id="slide-163-layer<?php echo $i; ?>-2"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['150','160','180','150']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power3.easeOut;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1250"
                         data-splitin="none"
                         data-splitout="none"
                         data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-164","delay":""}]'
                         data-responsive_offset="on">
                        <a class="btn btn-banner" href="<?php echo $banner->url; ?>">View More</a>
                    </div>
                </li>
                <?php $i ++; } ?>

            </ul>
        </div>
    </div>
</div>

<!-- main container -->
<main id="main">
    <section class="content-block bg-white">
        <div class="container">
            <header class="content-heading">
                <h2 class="main-heading">Trip To Kailash</h2>
<!--                <span class="main-subtitle">Mastering the art of perfect adventure for 10+ years in the wild.</span>-->
                <div class="seperator"></div>
            </header>
            <div class="featured-content adventure-holder">
                <div class="container-fluid">
                    <div class="row same-height">
                            <div class="col-md-6 text-block height wow slideInRight">
                                <div class="centered" align="justify">
                                    <h2 class="intro-heading">WELCOME TO TRIP TO KAILASH</h2>
                                    <p align="justify" class="intro">
                                        Trip to kailash Pvt. Ltd. with a team lead by the young professional in inbound and outbound tourism field lead with expert Tours and Trekking guide working more than decades and done more than 100 trips with Excellency. We give our best efforts to show our travelers the real culture, people, religion and views of your desired journey with taking care of all the details during trip with us.
                                    </p>
                                    <a href="<?php echo URL; ?>about" class="btn btn-primary btn-lg btn-theme">explore</a>
                                </div>
                            </div>
                        <div class="col-md-6 image height wow slideInLeft">

                            <div class="bg-stretch" >
                                <img src="<?php echo URL; ?>img/snow-mt-kailash.jpg" height="228" width="350"
                                             alt="image description">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- special block -->
    <!-- <aside class="special-block">
        <div class="container">
            <p class="special-text"><?php echo $highlight->description; ?></p>
        </div>
    </aside> -->


    <!-- article list with boxed style -->
    <section class="content-block article-boxed">
        <div class="container">
            <header class="content-heading">
                <h2 class="main-heading">JOIN US WITH OUR KAILASH BLESSING</h2>
                <div class="seperator"></div>
            </header>
            <div class="content-holder content-boxed home-slide1" style="text-align: justify">
                <div class="row db-3-col" id="home-guide-slide">
                    <?php foreach($latest_kailashs as $latest_kailash){ ?>
                    <article class="article has-hover-s1">
                        <div class="thumbnail">
                            <div class="img-wrap" style="max-height:200px; overflow:hidden; height:200px;">
                                <img src="<?php echo URL; echo 'uploads/'.$latest_kailash->image; ?>" height="228"
                                     alt="image description">
                            </div>
                            <h3 class="small-space"><a href="<?php echo URL;echo 'KailashTourDetail?kailash=' . $latest_kailash->id ?>"><?php echo $latest_kailash->title; ?></a></h3>
                            <!-- <aside class="meta">
                                <span class="activity">
									<span class="icon-acitivities"> </span>79 Activities
								</span>
                            </aside> -->
                            <p align="justify"><?php echo substr($latest_kailash->summary, 0, 180); ?>...</p>
                            <a href="<?php echo URL;echo 'KailashTourDetail?kailash=' . $latest_kailash->id ?>" class="btn btn-default">explore</a>
                            <footer>
                                <ul class="social-networks">
                                    <!-- <li><a href="#"><span class="icon-facebook"></span></a></li> -->
                                </ul>
                                <span class="price">Price:<span><?php echo $latest_kailash->price; ?></span></span>
                            </footer>
                        </div>
                    </article>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- featured adventure content -->
    <!-- <div class="featured-content adventure-holder">
        <div class="container-fluid">
            <div class="row same-height">
                <?php
                     foreach ($highlightedKailashp as $highlightedKailash) {
                         # code...

                    ?>
                <div class="col-md-6 image height wow slideInLeft">

                    <div class="bg-stretch" style="max-height:228px; overflow:hidden;">
                        <img src="<?php echo URL; echo 'uploads/'.$highlightedKailash->image; ?>" height="228" width="350"
                                     alt="image description">
                    </div>
                </div>
                <div class="col-md-6 text-block height wow slideInRight">
                    <div class="centered">
                        <h2 class="intro-heading"><?php echo $highlightedKailash->title; ?></h2>
                        <p class="intro"><?php echo $highlightedKailash->description; ?></p>
                        <a href="#" class="btn btn-primary btn-lg">explore</a>
                    </div>
                </div>
                <?php
            }
            ?>
            </div>
        </div>
    </div> -->
    <!-- testimonial block -->
    <!-- article list holder -->
    <div class="section-wrap">
        <div class="container">
            <header class="content-heading">
                <h2 class="main-heading">POPULAR PACKAGES</h2>
                <div class="seperator"></div>
            </header>
            <div class="content-holder" style="text-align: justify">
                <div class="row db-3-col" id="home-package-slide">
                    <?php foreach($popular_packages as $popular_package){ ?>
                    <article class="article has-hover-s3">
                        <div class="img-wrap" style="max-height:215px; max-width:370px; overflow:hidden; height:215px;">
                            <a href="<?php echo URL; echo 'servicePackageDetail?package='.$popular_package->id; ?>">
                                <img src="<?php echo URL; echo 'uploads/'.$popular_package->image; ?>" height="215" width="370"
                                     alt="image description">
                            </a>

                            <div class="hover-article">
                                <div class="img-caption text-uppercase"><?php echo $popular_package->title; ?></div>
                                <div class="info-footer">
                                    <span class="price">Price: <span><?php echo $popular_package->price; ?></span></span>
                                    <a href="<?php echo URL; echo 'servicePackageDetail?package='.$popular_package->id; ?>" class="link-more">Explore</a>
                                </div>
                            </div>
                        </div>
                        <h3><a href="<?php echo URL; echo 'servicePackageDetail?package='.$popular_package->id; ?>"><?php echo $popular_package->title; ?></a></h3>
                        <p style="width:95%;" align="justify"><?php echo substr($popular_package->summary, 0, 180); ?>...</p>
                    </article>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="testimonial-holder parallax" data-stellar-background-ratio="0.25" id="testimonial-home-page">
        <div class="container">
            <div id="testimonial-home-slide">
                <?php
                    foreach ($testimonials as $testimonial) {
                ?>
                <div class="slide">
                    <blockquote class="testimonial-quote">
                        <?php if($testimonial->image){
                            ?>
                            <div class="img" style="max-height: 112px; max-width: 112px; overflow: hidden;">
                                <img src="<?php echo URL; echo 'uploads/'.$testimonial->image; ?>" height="112" width="112" alt="image description">
                            </div>
                        <?php
                    }else{
                        ?>
                        <div class="img" style="border:none">

                        </div>
                        <?php } ?>
                        <div class="text">
                            <cite><?php echo $testimonial->name; ?></cite>
                            <q><?php echo $testimonial->feedback; ?></q>
                        </div>

                    </blockquote>
                </div>
                <?php
            }
            ?>
            </div>
        </div>
    </div>


</main>
