<!-- main container -->
<main id="main">

    <!-- content with sidebar -->
    <div class="bg-gray content-with-sidebar grid-view-sidebar">
        <div class="container">
            <div id="two-columns" class="row">
                <div id="content" class="col-sm-12 col-md-12">
                    <div class="blog-holder no-pagination">
                        <!-- blog single -->
                        <article class="blog-single">
                            <div class="img-wrap">
                                <img src="<?php echo URL; echo 'uploads/'.$kailash_detail->image; ?>"  height="480" width="870" alt="image description">
                                <!-- <img src="frontend/img/blog/img-01.jpg" height="480" width="870"
                                     alt="image description"> -->
                            </div>
                            <div class="description" style="text-align: justify">
                                <h1 class="content-main-heading"><?php echo $kailash_detail->title; ?></h1>
                                <div id="limit">
                                    <?php echo $kailash_detail->description; ?>
                                </div>
                                <span class="custom-hide-show" id="slide-more">Show More...</span>
                                <span class="custom-hide-show" id="slide-less">Show Less...</span>
                            </div>

                        </article>
                    </div>
                </div>
                <div class="content-holder content-sub-holder">
                    <h2 class="heading" align="center">Our Popular Kailash Tour</h2>
                    <div class="row db-3-col">
                        <?php foreach ($kailash_packages as $kailash_package) { ?>
                            <article class="col-md-6 col-lg-4 article has-hover-s1 thumb-full">
                                <div class="thumbnail">
                                    <div class="img-wrap" style="max-height:200px; overflow:hidden;">
                                        <img src="<?php echo URL; echo 'uploads/'.$kailash_package->image; ?>"  height="228" alt="image description">
                                    </div>
                                    <h3 class="small-space"><a href="<?php echo URL;echo 'KailashTourDetail?kailash=' . $kailash_package->id ?>"><?php echo $kailash_package->title; ?></a></h3>
                                    <p align="justify"><?php echo substr($kailash_package->summary, 0, 180); ?>...</p>
                                    <a href="<?php echo URL;echo 'KailashTourDetail?kailash=' . $kailash_package->id ?>" class="btn btn-default">Explore</a>
                                    <footer>
                                        <ul class="social-networks">
                                            <!-- <li><a href="#"><span class="icon-facebook"></span></a></li> -->
                                        </ul>
                                        <span class="price">Price:<span><?php echo $kailash_package->price; ?></span></span>
                                    </footer>
                                </div>
                            </article>
                        <?php } ?>
                    </div>
                </div>
                <div class="content-holder content-sub-holder">
                    <h2 class="heading center" align="center">Kailash Travel Guide</h2>
                    <div class="row db-3-col" id="guide-slide">
                        <?php foreach ($kailash_guides as $kailash_guide) { ?>
                            <article class="article">
                                <div class="thumbnail">
                                    <h3 class="small-space"><a href="<?php echo URL; echo 'kailash_guide?guide='.$kailash_guide->id; ?>"><?php echo $kailash_guide->title; ?></a></h3>
                                    <strong class="info-title"></strong>
                                    <div class="img-wrap" style="height:200px; max-height:200px">
                                        <a>
                                        <img src="<?php echo URL; echo 'uploads/'.$kailash_guide->image; ?>" height="228" width="350" alt="image description">
                                        </a>
                                    </div>

                                </div>
                            </article>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- recent block -->

</main>
