<section class="content-block guide-sub guide-add bg-form">
    <div class="inner-main common-spacing container btn-demo-wrapper">
        <div class="container">
            <header class="content-heading" style="margin-top: 90px">
                <h2 class="main-heading">Kailash Tour Booking</h2>
            </header>


            <div class="booking-form-wrap">
                <form action="<?php echo URL; echo 'kpBooking?kailash_date='.$kailash_date_id ?>" enctype="multipart/form-data" method="post">
                    <div class="row vertical-divider">
                        <div class="col-md-6">
                            <h3 class="form-section-title">Personal Details</h3>
                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" accept="image/*" name="userfile" id="userfile" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="full_name">Full Name</label>
                                <input type="text" name="full_name" class="form-control" placeholder="Full Name" required>
                            </div>

                            <div class="form-group">
                                <label>Gender</label>
                                <div class="radio checkbox-inline">
                                    <label class="i-checks ">
                                        <input name="gender" id="gender-0" value="Male" type="radio"required>
                                        <i></i>
                                        Male
                                    </label>
                                </div>
                                <div class="radio checkbox-inline">
                                    <label class="i-checks ">
                                        <input name="gender" id="gender-1" value="Female" type="radio" required>
                                        <i></i>
                                        Female
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Nationality</label>
                                <input type="text" name="nationality" class="form-control" placeholder="Nationality" required>
                            </div>
                            <div class="form-group">
                                <label>Date of Birth</label>
                                <input type="date" name="dob" class="form-control" placeholder="Date Of Birth" required>
                            </div>
                            <div class="form-group">
                                <label for="passport_no">Passport No</label>
                                <input type="text" name="passport_no" class="form-control" placeholder="Passport No" required>
                            </div>
                            <div class="form-group">
                                <label for="date_of_issue">Date of Issue</label>
                                 <input type="date" name="date_of_issue" class="form-control" placeholder="Date Of Issue" required>
                            </div>
                            <div class="form-group">
                                <label for="place_of_issue">Place Of Issue</label>
                                <input type="date" name="place_of_issue" class="form-control" placeholder="Place Of Issue" required>
                            </div>
                            <div class="form-group">
                                <label for="date_of_expiry">Date of Expiry</label>
                                <input type="date" name="date_of_expiry" class="form-control" placeholder="Date Of Expiry" required>
                            </div>
                            <div class="form-group">
                                <label for="permanent_address">Permanent Address</label>
                                <input type="text" name="permanent_address" class="form-control" placeholder="Permanent Address" required>
                            </div>
                            <div class="form-group">
                                <label for="profession">Profession</label>
                                <input type="text" name="profession" class="form-control" placeholder="Profession" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <h3 class="form-section-title">Personal Details</h3>
                            <div class="form-group">
                                <label for="mobile_no">Personal No</label>
                                <input type="number" min="1" name="mobile_no" class="form-control" placeholder="Personal No" required>
                            </div>
                            <div class="form-group">
                                <label for="telephone">Telephone (Home)</label>
                                <input type="text" name="telephone" class="form-control" placeholder="Telephone (Home)">
                            </div>
                            <div class="form-group">
                                <label for="email_id">Email Address</label>
                                <input type="email" name="email_id" class="form-control" placeholder="Email Address" required>
                            </div>

                            <h3 class="form-section-title">In Case of Emergency Contact Person(Compulsory)</h3>
                            <div class="form-group">
                                <label for="e_name">Name</label>
                                <input type="text" name="e_name" class="form-control" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <label for="e_relation">Relation</label>
                                 <input type="text" name="e_relation" class="form-control" placeholder="Relation" required>
                            </div>
                            <div class="form-group">
                                <label for="e_telephone">Phone Number</label>
                                <input type="text" name="e_telephone" class="form-control" placeholder="Phone  Number" required>
                            </div>
                            <div class="form-group">
                                <label for="e_email_id">Email Address</label>
                                <input type="text" name="e_email_id" class="form-control" placeholder="Email Address" required>
                            </div>


                            <h3 class="form-section-title">Tours Details</h3>
                            <div class="form-group">
                                <label for="tour_package_type">Tour Package</label>
                                <input disabled type="text" name="package_id" class="form-control" value="<?php if($kailash_date_object){ echo $kailash_date_object->kp_title.' ('.$kailash_date_object->from_date.' / '.$kailash_date_object->to_date.')'; } ?>">
                            </div>
                            <div class="form-group">
                                <label for="preffered_arrival_date">Preferred Arrival Date</label>
                                <input type="date" name="preffered_arrival_date" class="form-control" placeholder="Preferred Arrival Date">
                            </div>

                            <!-- <div class="form-group">
                                <label for="adv_deposit">Advance Deposit</label>
                                <input type="text" name="adv_deposit" class="form-control" placeholder="Advance Deposit">
                            </div> -->
                        </div>
                    </div>
                    <?php if($msg){
                         if($msgtype == "error"){ ?>
                    <div class="alert alert-danger">
                        <?php echo $msg; ?>
                    </div>
                    <?php }
                     if($msgtype == "success"){?>
                     <div class="alert alert-success">
                         <?php echo $msg; ?>
                     </div>
                    <?php  }} ?>
                     <button class="btn btn-primary" name="bookKP" type="submit">Book Tour</button>
                </form>
            </div>
        </div>
    </div>
</section>
