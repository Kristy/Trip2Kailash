<!--Banner Area Start-->
<div class="banner-area grid-two">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <div class="title-border">
                        <h1>Adventure <span>Listing</span></h1>
                    </div>
                    <p class="text-white">Three Columns Grid View Adventure Listing</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li>Gallery</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--End of Banner Area-->
<!--Gallery Section-->
<section class="gallery-section full-width pb_2">
    <div class="auto-container">
        <!--Filter-->
        <div class="filters">
            <ul class="filter-tabs style-one clearfix anim-3-all">
                <li class="filter" data-role="button" data-filter="all">All</li>
                <li class="filter" data-role="button" data-filter=".treking">Treking</li>
                <li class="filter" data-role="button" data-filter=".rafting">Rafting</li>
                <li class="filter" data-role="button" data-filter=".hiking">Hiking</li>
                <li class="filter" data-role="button" data-filter=".mountaineering">Mountaineering</li>
            </ul>
        </div>
    </div>

    <div class="images-container">
        <div class="filter-list clearfix">
            <!--Image Box-->
            <div class="image-box mix mix_all mountaineering ">
                <div class="inner-box">
                    <figure class="image"><a href="<?php echo URL; ?>frontend/img/adventure-list/7.jpg" class="lightbox-image"><img src="<?php echo URL; ?>frontend/img/adventure-list/7.jpg" alt=""></a></figure>
                    <a href="<?php echo URL; ?>frontend/img/adventure-list/7.jpg" class="lightbox-image btn-zoom" title="Image Title Here"><span class="icon fa fa-dot-circle-o"></span></a>
                </div>
            </div>

            <!--Image Box-->
            <div class="image-box mix mix_all treking">
                <div class="inner-box">
                    <figure class="image"><a href="<?php echo URL; ?>frontend/img/adventure-list/17.jpg" class="lightbox-image"><img src="<?php echo URL; ?>frontend/img/adventure-list/17.jpg" alt=""></a></figure>
                    <a href="<?php echo URL; ?>frontend/img/adventure-list/17.jpg" class="lightbox-image btn-zoom" title="Image Title Here"><span class="icon fa fa-dot-circle-o"></span></a>
                </div>
            </div>

            <!--Image Box-->
            <div class="image-box mix mix_all rafting">
                <div class="inner-box">
                    <figure class="image"><a href="<?php echo URL; ?>frontend/img/adventure-list/7.jpg" class="lightbox-image"><img src="<?php echo URL; ?>frontend/img/adventure-list/7.jpg" alt=""></a></figure>
                    <a href="<?php echo URL; ?>frontend/img/adventure-list/7.jpg" class="lightbox-image btn-zoom" title="Image Title Here"><span class="icon fa fa-dot-circle-o"></span></a>
                </div>
            </div>

            <!--Image Box-->
            <div class="image-box mix mix_all hiking ">
                <div class="inner-box">
                    <figure class="image"><a href="<?php echo URL; ?>frontend/img/adventure-list/17.jpg" class="lightbox-image"><img src="<?php echo URL; ?>frontend/img/adventure-list/17.jpg" alt=""></a></figure>
                    <a href="<?php echo URL; ?>frontend/img/adventure-list/17.jpg" class="lightbox-image btn-zoom" title="Image Title Here"><span class="icon fa fa-dot-circle-o"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>