<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicons -->
    <link rel="chetana-icon-precomposed" href="<?php echo URL; ?>Website_assets/img/chetana-icon-precomposed.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo URL; ?>Website_assets/img/chetana-title.png"/>

    <title>Trip2Kailash | Login</title>

    <link href="<?php echo URL; ?>\AdminPanel_assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL; ?>\AdminPanel_assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo URL; ?>\AdminPanel_assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo URL; ?>\AdminPanel_assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div  style="margin-top:10%;" class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Welcome to Trip to Kailash Login</h3>
            <?php
				if(isset($_GET["login"]) == "error")
				{
			?>
            	<p style="color:red; font-weight:bold">Incorrect Username or Password.</p>
            <?php
				}
			?>
            <form class="m-t" role="form" action="<?php echo URL; ?>verifylogin" method="post">
                <div class="form-group">
                    <input name="UserName" type="text" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input name="Password" type="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>


            </form>
            <p class="m-t"> <small>&copy; 2018</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo URL; ?>\AdminPanel_assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo URL; ?>\AdminPanel_assets/js/bootstrap.min.js"></script>

</body>

</html>
