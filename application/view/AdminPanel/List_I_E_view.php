<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Inclusion and Exclusion</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List Inclusion Exclusion</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; echo 'manageInclusionExclusion/addInclusionExclusion?sp_id='.$sp_id; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Inclusion / Exclusion</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <table class="table">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>Title</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  $i = 1;
                  foreach($requirements as $requirement){
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $requirement->title; ?></td>
                    <td>
                      <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; ?>manageInclusionExclusion/editIncExc">
                        <input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>">
                          <button title="Edit" class="btn btn-info" name="submit_to_edit" type="submit"  value="<?php echo $requirement->id;?>"><i class="fa fa-edit"></i></button>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; echo 'manageInclusionExclusion/deleteInclusionExclusion?sp_id='.$sp_id; ?>" onSubmit="return confirm('Are you sure you want to delete this data?');">
                          <button title="Delete" type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $requirement->id;?>"><i class="fa fa-trash"></i></button>
                      </form>

                    </td>
                  </tr>
                  <?php
                  }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
