<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Icon</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>Add Icon</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <!-- <div class="ibox-title">
        </div> -->

        <div class="ibox-content">
          <form action="<?php echo URL; ?>manageIcon/addIcon" class="form-horizontal" method="post">          
            
            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Icon</label>
              </div>
              <div class="col-sm-10">
                <input type="text" maxlength="64" class="form-control" name="icon" value="<?php if(isset($title)){ echo $title;} ?>" required />
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Icon_name</label>
              </div>
              <div class="col-sm-10">
                <input type="text" maxlength="64" class="form-control" name="icon_name" value="<?php if(isset($title)){ echo $title;} ?>" required />
              </div>
            </div>


            
            <?php
            if(isset($formmsg)){
              if($formmsg->hasMessages()[0]['sticky'] == "error"){
            ?>
              <div class="">
                  <div class="col-sm-10 col-sm-offset-2 alert alert-danger">
                      <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
              <?php
              }
              if($formmsg->hasMessages()[0]['sticky'] == "success"){
              ?>
              <div class="">
                  <div class="col-sm-10 col-sm-offset-2 alert alert-success ">
                      <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
            <?php
              }
            }
            ?>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" name="addIcon" type="submit">Save</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>