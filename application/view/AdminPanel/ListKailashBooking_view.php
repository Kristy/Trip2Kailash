<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Kailash Package</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List Kailash Booking</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; echo 'manageKailashBooking/addBooking?kp_id='.$kp_id; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Booking</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <table class="table">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>Package</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Status</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  $i = 1;
                  foreach($bookings as $booking){
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td>
                      <?php echo $booking->kp_title; ?>
                    </td>
                    <td><?php echo $booking->from_date; ?></td>
                    <td><?php echo $booking->to_date; ?></td>
                    <td><?php echo $booking->status; ?></td>
                    <td>
                      <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; ?>manageKailashBooking/editBooking">
                          <button title="Edit" class="btn btn-info" name="submit_to_edit" type="submit"  value="<?php echo $booking->id;?>"><i class="fa fa-edit"></i></button>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; echo 'manageKailashBooking/deleteBooking?kp_id='.$kp_id; ?>" onSubmit="return confirm('Are you sure you want to delete this data?');">
                          <button title="Delete" type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $booking->id;?>"><i class="fa fa-trash"></i></button>
                      </form>

                    </td>
                  </tr>
                  <?php
                  }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
