<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Kailash Booking</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>Add Booking</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <!-- <div class="ibox-title">
        </div> -->

        <div class="ibox-content">
          <form action="<?php echo URL; echo 'manageKailashBooking/addBooking?kp_id='.$kp_id ?>" class="form-horizontal" method="post">
            
            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">From</label>
              </div>
              <div class="col-sm-10">
                <input type="date" maxlength="64" class="form-control" name="from" value="<?php if(isset($from)){ echo $from;} ?>" required />
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">To</label>
              </div>
              <div class="col-sm-10">
                <input type="date" maxlength="64" class="form-control" name="to" value="<?php if(isset($to)){ echo $to;} ?>" required />
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Status</label>
              </div>
              <div class="col-sm-10">
                <textarea name="status" class="textarea form-control"><?php if(isset($status)){ echo $status;} ?></textarea>
              </div>
            </div>
            <?php
            if(isset($formmsg)){
              if($formmsg->hasMessages()[0]['sticky'] == "error"){
            ?>
              <div class="">
                  <div class="col-sm-10 col-sm-offset-2 alert alert-danger">
                      <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
              <?php
              }
              if($formmsg->hasMessages()[0]['sticky'] == "success"){
              ?>
              <div class="">
                  <div class="col-sm-10 col-sm-offset-2 alert alert-success ">
                      <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
            <?php
              }
            }
            ?>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" name="addBooking" type="submit">Save</button>
                </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
