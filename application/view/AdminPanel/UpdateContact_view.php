            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Contact</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong><?php echo $page_purpose; ?> Contact Detail</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-8">
                  <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?php echo $page_purpose; ?> Contact</h5>
                    </div>

                    <div class="ibox-content">
                      <form action="<?php echo $form_url; ?>" class="form-horizontal" method="post">

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Label</label>
                          </div>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="label" maxlength="30" value="<?php if(isset($label)){ echo $label;} ?>" required />
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Icon <i id="icon-demo" class="fa fa-<?php if(isset($icon)){ echo $icon;} ?>"></i></label>
                          </div>
                          <div class="col-sm-10">
                            <select class="form-control select2" name="icon" required>
                              <option value="">Select Icon</option>
                              <?php foreach($icons as $i){ ?>
                              <option <?php if(isset($icon)){ if($icon == $i->icon_name){?>selected<?php }} ?> value="<?php echo $i->icon_name; ?>"><?php echo $i->icon_name; ?> <i class="fa fa-<?php echo $i->icon_name;?>"></i></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Detail</label>
                          </div>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="detail" maxlength="64" value="<?php if(isset($detail)){ echo $detail;} ?>" required />
                          </div>
                        </div>
                        <?php
                        if(isset($formmsg)){
                          if($formmsg->hasMessages()[0]['sticky'] == "error"){
                        ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-danger">
                                  <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                          <?php
                          }
                          if($formmsg->hasMessages()[0]['sticky'] == "success"){
                          ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-success ">
                                  <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                        <?php
                          }
                        }
                        ?>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" name="submit_contactdetail" value="<?php if(isset($id)){ echo $id;} ?>" type="submit"><?php echo $button_name; ?></button>
                            </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
