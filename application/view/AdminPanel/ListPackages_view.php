<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Service Package</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List Service Package</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; ?>managePackage/addPackage" class="btn btn-primary"><i class="fa fa-plus"></i> Add Package</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <table class="table">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>Package Image</th>
                  <th>Package Name</th>
                  <th>Price</th>
                  <th>Activity</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  $i = 1;
                  foreach($packages as $package){
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td>
                      <div class="lightBoxGallery">
                              <a style="color:inherit;" href="<?php echo URL; echo 'uploads/'.$package->image; ?>" data-gallery="">
                                <?php echo $package->image; ?>
                              </a>
                          <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                          <div id="blueimp-gallery" class="blueimp-gallery">
                              <div class="slides"></div>
                              <h3 class="title"></h3>
                              <a class="prev">‹</a>
                              <a class="next">›</a>
                              <a class="close">×</a>
                              <a class="play-pause"></a>
                              <ol class="indicator"></ol>
                          </div>
                          &nbsp; &nbsp;
                          <a href="<?php echo URL; echo "managePackage/package_image?package=".$package->id; ?>" title="Manage Image" class="btn btn-success"><i class="fa fa-image" ></i></a>
                      </div>
                    </td>
                    <td><?php echo $package->title; ?></td>
                    <td><?php echo $package->price; ?></td>
                    <td><?php echo $package->a_title; ?></td>
                    <td>
                      <form style="margin-left:1%; float:left;">
                        <a href="#"  title="View"  class="btn" style="border:solid 1px #9e9e9e;"><i class="fa fa-eye" ></i></a>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; ?>managePackage/editPackage">
                          <button title="Edit" class="btn btn-info" name="submit_to_edit" type="submit"  value="<?php echo $package->id;?>"><i class="fa fa-edit"></i></button>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; ?>managePackage/deletePackage" onSubmit="return confirm('Are you sure you want to delete this data?');">
                          <button title="Delete" type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $package->id;?>"><i class="fa fa-trash"></i></button>
                      </form>
                      <form style="margin-left:1%;  float:left;">
                          <a href="<?php echo URL; echo 'manageServiceItenary?sp_id='.$package->id; ?>"  title="View Itenary"  class="btn" style="border:solid 1px #9e9e9e;"><i class="fa fa-tasks" ></i></a>
                      </form>
                      <form style="margin-left:1%;  float:left;">
                          <a href="<?php echo URL; echo 'manageInclusionExclusion?sp_id='.$package->id; ?>"  title="Inclusion Exclusion"  class="btn" style="border:solid 1px #9e9e9e;"><i class="fa fa-arrows-h" ></i></a>
                      </form>

                    </td>
                  </tr>
                  <?php
                  }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
