<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Team Members</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>Add Team Member</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <!-- <div class="ibox-title">
        </div> -->

        <div class="ibox-content">
          <form action="<?php echo URL; ?>manageTeamMember/addMember" class="form-horizontal" enctype="multipart/form-data" method="post">

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Image</label>
              </div>
              <div class="col-sm-10">
                <input type="file" accept="image/*" name="userfile" id="userfile">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Name</label>
              </div>
              <div class="col-sm-10">
                <input type="text" maxlength="64" class="form-control" name="name" value="<?php if(isset($name)){ echo $name;} ?>" required />
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Designation</label>
              </div>
              <div class="col-sm-10">
                <input type="text" maxlength="64" class="form-control" name="post" value="<?php if(isset($post)){ echo $post;} ?>" required />
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Type</label>
              </div>
              <div class="col-sm-10">
                <select name="type" required class="form-control select2">                  
                    <option value="national">National</option>
                    <option value="international">InterNational</option>
                </select>
              </div>
            </div>

            <?php
            if(isset($formmsg)){
              if($formmsg->hasMessages()[0]['sticky'] == "error"){
            ?>
              <div class="">
                  <div class="col-sm-10 col-sm-offset-2 alert alert-danger">
                      <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
              <?php
              }
              if($formmsg->hasMessages()[0]['sticky'] == "success"){
              ?>
              <div class="">
                  <div class="col-sm-10 col-sm-offset-2 alert alert-success ">
                      <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
            <?php
              }
            }
            ?>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" name="addMember" type="submit">Save</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
