<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Kailash Tour</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong><?php echo $page_purpose; ?> Kailash Detail</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><?php echo $page_purpose; ?> Kailash Detail</h5>
        </div>

        <div class="ibox-content">
          <form action="<?php echo $form_url; ?>" class="form-horizontal" method="post"  enctype="multipart/form-data">

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Title</label>
              </div>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="title" maxlength="64" value="<?php if(isset($title)){ echo $title;} ?>" required />
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Description</label>
              </div>
              <div class="col-sm-10">
                <textarea name="description" id='summernote' class="textarea form-control"><?php if(isset($description)){ echo $description;} ?></textarea>
              </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" name="submit_theme" value="<?php if(isset($id)){ echo $id;} ?>" type="submit"><?php echo $button_name; ?></button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Select Theme Image</h5>
        </div>

        <div class="ibox-content">
          <form action="<?php echo $form_url; ?>" class="form-horizontal" method="post"  enctype="multipart/form-data">

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Change Image</label>
              </div>
              <div class="col-sm-10">
                <input type="file" name="file" required/>
              </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" name="submit_theme_image" value="<?php if(isset($id)){ echo $id;} ?>" type="submit">Upload</button>
                </div>
            </div>
          </form>
        </div>
        <?php if($image_theme){ ?>
        <div class="ibox-content">
          <h3>Current Image</h3>
          <img width="600" class="img-responsive" src="<?php echo URL; echo $image_theme; ?>" style="background-attachment: scroll;
          background-clip: border-box;
          background-color: rgba(0, 0, 0, 0);
          background-position: left top;
          background-repeat: no-repeat;
          background-size: 49% 100%;">
        </div>
        <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>
