            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Manage Gallery</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Add Image</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Dropzone Area</h5>

                      </div>
                      <div class="ibox-content">
                        <!-- <form action="<?php echo URL; ?>manageGallery/addupload" enctype="multipart/form-data" class="dropzone"></form> -->
                          <form id="my-awesome-dropzone" class="dropzone" enctype="multipart/form-data" action="<?php echo URL; ?>manageGallery/addImages">
                              <div id="dropzone-previews"></div>
                              <button name="submit_gallery" id="submit-dropzone" type="submit" class="btn btn-primary pull-right">Submit this form!</button>
                          </form>

                      </div>
                  </div>
                </div>
              </div>
            </div>
