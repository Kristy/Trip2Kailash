<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Manage Bookings</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List Service Booking</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; ?>manageTestimonial/addTestimonial" class="btn btn-primary"><i class="fa fa-plus"></i> Add Package</a>
        </div>
    </div> -->
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <table class="table">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>Full Name</th>
                  <th>Gender</th>
                  <th>Nationality</th>
                  <th>DOB</th>
                  <th>Passport No</th>
                  <th>Address</th>
                  <th>Profession</th>
                  <th>Mobile No</th>
                  <th>Preffered Arrival Date</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  $i = 1;
                  foreach($bookings as $booking){
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $booking->full_name; ?></td>
                    <td><?php echo $booking->gender; ?></td>
                    <td><?php echo $booking->nationality; ?></td>
                    <td><?php echo $booking->dob; ?></td>
                    <td><?php echo $booking->passport_no; ?></td>
                    <td><?php echo $booking->permanent_address; ?></td>
                    <td><?php echo $booking->profession; ?></td>
                    <td><?php echo $booking->mobile_no; ?></td>
                    <td><?php echo $booking->preffered_date; ?></td>
                    <td>
                      <form style="margin-left:1%; float:left;" method="GET" action="<?php echo URL; ?>manageServiceBooking/detailBooking">
                        <button title="View"  class="btn" name="submit_to_detail" type="submit" value="<?php echo $booking->id;?>" style="border:solid 1px #9e9e9e;" ><i class="fa fa-eye" ></i></button>
                      </form>
                      <!-- <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; ?>manageTestimonial/editGuide">
                          <button title="Edit" class="btn btn-info" name="submit_to_edit" type="submit"  value="<?php echo $bookings->id;?>"><i class="fa fa-edit"></i></button>
                      </form> -->
                      <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; ?>manageServiceBooking/deleteTestimonial" onSubmit="return confirm('Are you sure you want to delete this data?');">
                          <button title="Delete" type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $booking->id;?>"><i class="fa fa-trash"></i></button>
                      </form>
                      

                    </td>
                  </tr>
                  <?php
                  }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
