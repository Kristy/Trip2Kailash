            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Dashboard</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                        <a href="<?php echo URL; ?>DashBoardPanel/addBanner" class="btn btn-primary"><i class="fa fa-plus"></i> Add Banner</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">

                    <div class="ibox-content">
                      <table class="table">
                          <thead>
                          <tr>
                              <th>S.No.</th>
                              <th>Banner Image</th>
                              <th>Banner Title</th>
                              <th>Banner Description</th>
                              <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                              <?php
                              $i = 1;
                              foreach($banners as $banner){
                              ?>
                              <tr>
                                <td><?php echo $i++; ?></td>
                                <td>
                                  <div class="lightBoxGallery">
                                          <a style="color:inherit;" href="<?php echo URL; echo 'uploads/banner/'.$banner->banner_image; ?>" data-gallery="">
                                            <?php echo $banner->banner_image; ?>
                                          </a>
                                      <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                                      <div id="blueimp-gallery" class="blueimp-gallery">
                                          <div class="slides"></div>
                                          <h3 class="title"></h3>
                                          <a class="prev">‹</a>
                                          <a class="next">›</a>
                                          <a class="close">×</a>
                                          <a class="play-pause"></a>
                                          <ol class="indicator"></ol>
                                      </div>

                                  </div>
                                </td>
                                <td><?php echo $banner->banner_title; ?></td>
                                <td><?php echo $banner->banner_desc; ?></td>

                                <td>
                                  <form method="post" action="<?php echo URL; ?>DashBoardPanel/deleteBanner" onSubmit="return confirm('Are you sure you want to delete this data?');">
                                      <button type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $banner->id;?>"><i class="fa fa-trash"></i></button>
                                  </form>
                                </td>
                              </tr>
                              <?php
                              }
                              ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
