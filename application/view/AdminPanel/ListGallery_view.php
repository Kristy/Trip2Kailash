<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Manage Gallery</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>Gallery List</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; ?>manageGallery/addImages" class="btn btn-primary"><i class="fa fa-plus"></i> Add Image(s)</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <div class="lightBoxGallery">
              <?php
              if(!$gallery_images){
                  echo "There is no image to show.";
              }
              foreach($gallery_images as $gallery_image)
              {
              ?>
                <div class="col-sm-3 gallery-container">
                  <a href="<?php echo URL; echo 'uploads/'.$gallery_image->image_name; ?>" data-gallery="">
                    <img  class="img-responsive" src="<?php echo URL; echo 'uploads/'.$gallery_image->image_name; ?>">
                  </a>
                  <form action="<?php echo URL; ?>manageGallery/deleteImage" method="post" class="col-sm-12 text-center gallery-form" onSubmit="return confirm('Are you sure you want to delete this data?');">
                    <button type="submit" class="btn btn-danger gallery-button" name="SubmitDelete" value="<?php echo $gallery_image->id; ?>"><i class="fa fa-times"></i></button>
                  </form>
                </div>
              <?php
              }
               ?>
              <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
              <div id="blueimp-gallery" class="blueimp-gallery">
                  <div class="slides"></div>
                  <h3 class="title"></h3>
                  <a class="prev">‹</a>
                  <a class="next">›</a>
                  <a class="close">×</a>
                  <a class="play-pause"></a>
                  <ol class="indicator"></ol>
              </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
