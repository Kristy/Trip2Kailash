            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Activity</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Edit Activity</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                    <!-- <div class="ibox-title">
                    </div> -->

                    <div class="ibox-content">
                      <form action="<?php echo URL; ?>manageActivity/editActivity" class="form-horizontal" enctype="multipart/form-data" method="post">

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Title</label>
                          </div>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="<?php if(isset($title)){ echo $title;} ?>" required />
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Destination</label>
                          </div>
                          <div class="col-sm-10">
                            <select name="destination" required class="form-control select2">
                              <?php foreach($destinations as $desti){ ?>
                              <option <?php if($ActivityDetail->destination_id == $desti->id){ echo 'selected'; } ?> value="<?php echo $desti->id; ?>"><?php echo $desti->name; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Description</label>
                          </div>
                          <div class="col-sm-10">
                            <textarea name="description" rows="10" class="textarea form-control"><?php if(isset($description)){ echo $description;} ?></textarea>
                          </div>
                        </div>
                        <?php
                        if(isset($formmsg)){
                          if($formmsg->hasMessages()[0]['sticky'] == "error"){
                        ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-danger">
                                  <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                          <?php
                          }
                          if($formmsg->hasMessages()[0]['sticky'] == "success"){
                          ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-success ">
                                  <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                        <?php
                          }
                        }
                        ?>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" value="<?php if(isset($id)){ echo $id;} ?>" name="editActivity_submit" type="submit">Update</button>
                            </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
