            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Home Content</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Update Home Content</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-10">
                  <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Update Info</h5>
                    </div>

                    <div class="ibox-content">
                      <form action="<?php echo URL; ?>manageHomeContent" class="form-horizontal" method="post">
                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Description</label>
                          </div>
                          <div class="col-sm-10">
                            <textarea name="description" rows="8" class="form-control summernote"><?php if(isset($description)){ echo $description;} ?></textarea>
                          </div>
                        </div>
                        <?php
                        if(isset($formmsg)){
                          if($formmsg->hasMessages()[0]['sticky'] == "success"){
                          ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-success ">
                                  <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                        <?php
                          }
                        }
                        ?>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" name="updateHomeContent" type="submit">Save</button>
                            </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
