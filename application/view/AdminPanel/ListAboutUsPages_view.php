            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>About Us</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>About Us Pages</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                        <a href="<?php echo URL; ?>manageAboutUsPages/addAboutUS" class="btn btn-primary"><i class="fa fa-plus"></i> Add Page</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">

                    <div class="ibox-content">
                      <table class="table">
                          <thead>
                          <tr>
                              <th>S.No.</th>
                              <th>Title</th>
                              <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                              <?php
                              $i = 1;
                              foreach($about_us_pages as $about_us_page){
                              ?>
                              <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $about_us_page->heading; ?></td>
                                <td>
                                  <span class="col-sm-6">
                                    <form class="col-sm-2">
                                      <a href="<?php echo URL; echo 'Founder'; ?>" class="btn"><i class="fa fa-eye"></i></a>
                                    </form>
                                    <form class="col-sm-2" method="GET" action="<?php echo URL; ?>manageAboutUsPages/editAboutUS">
                                        <button class="btn btn-info" name="submit_to_edit" type="submit" value="<?php echo $about_us_page->id;?>"><i class="fa fa-edit"></i></button>
                                    </form>
                                    <form class="col-sm-2" method="post" action="<?php echo URL; ?>manageAboutUsPages/deleteAboutUS" onSubmit="return confirm('Are you sure you want to delete this data?');">
                                        <button type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $about_us_page->id;?>"><i class="fa fa-trash"></i></button>
                                    </form>
                                  </span>
                                </td>
                              </tr>
                              <?php
                              }
                              ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
