<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>About Us</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong><?php echo $page_purpose; ?> About Us</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <form action="<?php echo $form_url; ?>" class="form-horizontal" method="post"  enctype="multipart/form-data">

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Main Title</label>
              </div>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="title" maxlength="64" value="<?php if(isset($title)){ echo $title;} ?>" required />
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Quote</label>
              </div>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="quote" maxlength="100" value="<?php if(isset($quote)){ echo $quote;} ?>" required />
              </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <h4>Left Side</h4>
                        <div class="form-group">
                          <label class="control-label">Title</label>
                        </div>
                        <div class="form-group">
                              <input type="text" class="form-control" name="left_title" maxlength="64" value="<?php if(isset($left_title)){ echo $left_title;} ?>" required />
                        </div>
                        <div class="form-group">
                          <label class="control-label">Description</label>
                        </div>
                        <div class="form-group">
                          <textarea name="left_description" rows="12" class="textarea form-control"><?php if(isset($left_description)){ echo $left_description;} ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <h4>Left Side</h4>
                        <div class="form-group">
                          <label class="control-label">Title</label>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" name="right_title" maxlength="64" value="<?php if(isset($right_title)){ echo $right_title;} ?>" required />
                        </div>
                        <div class="form-group">
                          <label class="control-label">Description</label>
                        </div>
                        <div class="form-group">
                          <textarea name="right_description" rows="12" class="textarea form-control"><?php if(isset($right_description)){ echo $right_description;} ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if(isset($formmsg)){
              if($formmsg->hasMessages()[0]['sticky'] == "error"){
            ?>
              <div class="">
                  <div class="col-sm-10 alert alert-danger">
                      <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
              <?php
              }
              if($formmsg->hasMessages()[0]['sticky'] == "success"){
              ?>
              <div class="">
                  <div class="col-sm-10 alert alert-success ">
                      <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                  </div>
              </div>
            <?php
              }
            }
            ?>
            <div class="form-group">
                <div class="col-sm-4">
                    <button class="btn btn-primary" name="submit_theme" value="<?php if(isset($id)){ echo $id;} ?>" type="submit"><?php echo $button_name; ?></button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Select Theme Image</h5>
        </div>

        <div class="ibox-content">
          <form action="<?php echo $form_url; ?>" class="form-horizontal" method="post"  enctype="multipart/form-data">

            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Change Image</label>
              </div>
              <div class="col-sm-10">
                <input type="file" name="file" required/>
              </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" name="submit_theme_image" value="<?php if(isset($id)){ echo $id;} ?>" type="submit">Upload</button>
                </div>
            </div>
          </form>
        </div>
        <?php if($image_theme){ ?>
        <div class="ibox-content">
          <h3>Current Image</h3>
          <img width="600" class="img-responsive" src="<?php echo URL; echo $image_theme; ?>" style="background-attachment: scroll;
          background-clip: border-box;
          background-color: rgba(0, 0, 0, 0);
          background-position: left top;
          background-repeat: no-repeat;
          background-size: 49% 100%;">
        </div>
        <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>
