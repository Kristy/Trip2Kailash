            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Contact</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Contact Detail</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                        <a href="<?php echo URL; ?>manageContact/addContact" class="btn btn-primary"><i class="fa fa-plus"></i> Add Contact</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                    <!-- <div class="ibox-title">

                    </div> -->
                    <div class="ibox-content" style="overflow:auto">
                      <table class="table">
                          <thead>
                          <tr>
                              <th>S.No.</th>
                              <th>Label</th>
                              <th>Detail</th>
                              <th>Icon</th>
                              <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                              <?php
                              $i = 1;
                              foreach($contact_details as $contact_detail){
                              ?>
                              <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $contact_detail->label; ?></td>
                                <td><?php echo $contact_detail->detail; ?></td>
                                <td><?php echo $contact_detail->icon; ?> <i class="fa fa-<?php echo $contact_detail->icon; ?>"></i></td>

                                <td>
                                  <span class="form-drop-list">
                                    <div style="text-align:left;" class="ibox-tools">
                                        <a class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"> Action</i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li class="table-action-list" style="width:100%;">
                                              <a href="<?php echo URL; echo 'Contact'; ?>" target="_blank" class="btn"><i class="fa fa-eye"></i> View</a>
                                            </li>
                                            <li class="table-action-list" style="width:100%;">
                                              <form method="post" style="width:inherit;" action="<?php echo URL; ?>manageContact/showinFooter">
                                                <?php if($contact_detail->show_in_footer == False){ ?>
                                                  <button type="submit" class="btn" style="width:inherit; text-align:left;" name="SubmitInFooter" value="<?php echo $contact_detail->id;?>"><i class="fa fa-star"></i> Show in Footer</button>
                                                <?php } else{ ?>
                                                  <button type="submit" class="btn" style="width:inherit; text-align:left;" name="RemoveInFooter" value="<?php echo $contact_detail->id;?>"><i class="fa fa-star"></i> Remove from Footer</button>
                                                <?php } ?>
                                              </form>
                                            </li>
                                            <li class="table-action-list" style="width:100%;">
                                              <form method="GET" style="width:inherit;" action="<?php echo URL; ?>manageContact/editContact">
                                                  <button class="btn btn-info" style="width:inherit; text-align:left;" name="submit_to_edit" type="submit" value="<?php echo $contact_detail->id;?>"><i class="fa fa-edit"></i> Edit</button>
                                              </form>
                                            </li>
                                            <li class="table-action-list" style="width:100%;">
                                              <form method="post" style="width:inherit;" action="<?php echo URL; ?>manageContact/deleteContact" onSubmit="return confirm('Are you sure you want to delete this data?');">
                                                  <button type="submit" class="btn btn-danger" style="width:inherit; text-align:left;" name="SubmitDelete" value="<?php echo $contact_detail->id;?>"><i class="fa fa-trash"></i> Delete</button>
                                              </form>
                                            </li>
                                        </ul>
                                    </div>
                                  </span>
                                </td>
                              </tr>
                              <?php
                              }
                              ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
