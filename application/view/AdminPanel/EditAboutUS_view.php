            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>About Us Pages</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Edit About Us Page</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Details</h5>
                    </div>

                    <div class="ibox-content">
                      <form action="<?php echo URL; ?>manageAboutUsPages/editAboutUS" class="form-horizontal" method="post">

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Title</label>
                          </div>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="heading" value="<?php if(isset($AboutUsDetail->heading)){ echo $AboutUsDetail->heading;} ?>" required />
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">Description</label>
                          </div>
                          <div class="col-sm-10">
                            <textarea name="description" id="summernote" class="textarea"><?php if(isset($AboutUsDetail->description)){ echo $AboutUsDetail->description;} ?></textarea>
                          </div>
                        </div>
                        <?php
                        if(isset($formmsg)){
                          if($formmsg->hasMessages()[0]['sticky'] == "error"){
                        ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-danger">
                                  <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                        <?php
                          }
                        }
                        ?>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" name="editAboutUS_submit" value="<?php if(isset($id)){ echo $id;} ?>" type="submit">Update</button>
                            </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
