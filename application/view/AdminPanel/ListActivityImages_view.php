<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>View Activity Image(s)</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>Images List</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <!-- <a href="<?php echo URL; ?>manageGallery/addImages" class="btn btn-primary"><i class="fa fa-plus"></i> Add Image(s)</a> -->
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <div class="lightBoxGallery">
              <?php
              foreach($activity_images as $activity_image)
              {
              ?>
                <div class="col-sm-3 gallery-container">
                  <a href="<?php echo URL; echo 'uploads/'.$activity_image; ?>" data-gallery="">
                    <img  class="img-responsive" src="<?php echo URL; echo 'uploads/'.$activity_image; ?>">
                  </a>
                  <form action="<?php echo URL; ?>manageMinistryActivity/deleteactivityImage" method="post" class="col-sm-12 text-center gallery-form" onSubmit="return confirm('Are you sure you want to delete this data?');">
                    <input name="image" value="<?php echo $activity_image; ?>" type="hidden"/>
                    <button type="submit" class="btn btn-danger gallery-button" name="SubmitDelete" value="<?php if(isset($_GET['id'])){ echo $_GET['id']; }?>"><i class="fa fa-times"></i></button>
                  </form>
                </div>
              <?php
              }
               ?>
              <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
              <div id="blueimp-gallery" class="blueimp-gallery">
                  <div class="slides"></div>
                  <h3 class="title"></h3>
                  <a class="prev">‹</a>
                  <a class="next">›</a>
                  <a class="close">×</a>
                  <a class="play-pause"></a>
                  <ol class="indicator"></ol>
              </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
