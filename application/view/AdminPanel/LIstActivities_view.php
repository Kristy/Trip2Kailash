            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Activity</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>List Activity</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                        <a href="<?php echo URL; ?>manageActivity/addActivity" class="btn btn-primary"><i class="fa fa-plus"></i> Add Activity</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">

                    <div class="ibox-content">
                      <table class="table">
                          <thead>
                          <tr>
                              <th>S.No.</th>
                              <th>Activity Image</th>
                              <th>Activity Name</th>
                              <th>Destination</th>
                              <th>Description</th>
                              <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                              <?php
                              $i = 1;
                              foreach($activities as $activity){
                              ?>
                              <tr>
                                <td><?php echo $i++; ?></td>
                                <td>
                                  <div class="lightBoxGallery">
                                          <a style="color:inherit;" href="<?php echo URL; echo 'uploads/'.$activity->image; ?>" data-gallery="">
                                            <?php echo $activity->image; ?>
                                          </a>
                                      <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                                      <div id="blueimp-gallery" class="blueimp-gallery">
                                          <div class="slides"></div>
                                          <h3 class="title"></h3>
                                          <a class="prev">‹</a>
                                          <a class="next">›</a>
                                          <a class="close">×</a>
                                          <a class="play-pause"></a>
                                          <ol class="indicator"></ol>
                                      </div>

                                  </div>
                                </td>
                                <td><?php echo $activity->title; ?></td>
                                <td><?php echo $activity->d_name; ?></td>
                                <td><?php echo substr($activity->description, 0, 200); ?>...</td>
                                <td>
                                  <!-- <form class="col-sm-2">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i></a>
                                  </form> -->
                                  <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; ?>manageActivity/editActivity">
                                      <button class="btn btn-info" name="submit_to_edit" type="submit" value="<?php echo $activity->id;?>"><i class="fa fa-edit"></i></button>
                                  </form>
                                  <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; ?>manageActivity/deleteActivity" onSubmit="return confirm('Are you sure you want to delete this data?');">
                                      <button type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $activity->id;?>"><i class="fa fa-trash"></i></button>
                                  </form>
                                </td>
                              </tr>
                              <?php
                              }
                              ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
