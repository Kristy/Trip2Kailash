            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Associated With</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Associated Detail</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                        <a href="<?php echo URL; ?>manageAssociatedWith/addAssociatedWith" class="btn btn-primary"><i class="fa fa-plus"></i> Add Associated</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                    <!-- <div class="ibox-title">

                    </div> -->
                    <div class="ibox-content" style="overflow:auto">
                      <table class="table">
                          <thead>
                          <tr>
                              <th>S.No.</th>
                              <th>Title</th>
                              <th>URL</th>
                              <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                              <?php
                              $i = 1;
                              foreach($Associated_details as $Associated_detail){
                              ?>
                              <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $Associated_detail->title; ?></td>
                                <td><?php echo $Associated_detail->url; ?></td>
                                <td>
                                    <form class="col-sm-2" method="post" action="<?php echo URL; ?>manageAssociatedWith/deleteAssociated" onSubmit="return confirm('Are you sure you want to delete this data?');">
                                        <button type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $Associated_detail->id;?>"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                              </tr>
                              <?php
                              }
                              ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
