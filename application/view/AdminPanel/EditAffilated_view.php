            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Affilated</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Add Affilated</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                    <!-- <div class="ibox-title">
                    </div> -->

                    <div class="ibox-content">
                      <form action="<?php echo URL; ?>manageAffilatedTo/editAffilated" class="form-horizontal" method="post">

                        

                        <div class="form-group">
                          <div class="col-sm-2">
                            <label class="control-label">URL</label>
                          </div>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="url" value="<?php if(isset($url)){ echo $url;} ?>" required />
                          </div>
                        </div>

                        <?php
                        if(isset($formmsg)){
                          if($formmsg->hasMessages()[0]['sticky'] == "error"){
                        ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-danger">
                                  <span class="alert alert-danger"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                          <?php
                          }
                          if($formmsg->hasMessages()[0]['sticky'] == "success"){
                          ?>
                          <div class="">
                              <div class="col-sm-10 col-sm-offset-2 alert alert-success ">
                                  <span class="alert alert-success"><?php echo $formmsg->hasMessages()[0]['message']; ?></span>
                              </div>
                          </div>
                        <?php
                          }
                        }
                        ?>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" value="<?php if(isset($id)){ echo $id;} ?>" name="editAffilated_submit" type="submit">Update</button>
                            </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
