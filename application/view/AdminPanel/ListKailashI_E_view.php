<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Kailash Inclusion and Exclusion</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List Inclusion and Exclusion</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; echo 'manageKailashInclusionExclusion/addInclusionExclusion?kp_id='.$kp_id; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Inclusion / Exclusion</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <table class="table">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>Title</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  $i = 1;
                  foreach($requirements as $requirement){
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $requirement->title; ?></td>
                    <td>
                      <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; echo 'manageKailashInclusionExclusion/editIncExc'; ?>">
                         <input type="hidden" name="kp_id" value="<?php echo $kp_id; ?>">
                          <button title="Edit" class="btn btn-info" name="submit_to_edit" type="submit"  value="<?php echo $requirement->id;?>"><i class="fa fa-edit"></i></button>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; echo 'manageKailashInclusionExclusion/deleteKInclusionExclusion?kp_id='.$kp_id; ?>" onSubmit="return confirm('Are you sure you want to delete this data?');">
                          <button title="Delete" type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $requirement->id;?>"><i class="fa fa-trash"></i></button>
                      </form>

                    </td>
                  </tr>
                  <?php
                  }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
