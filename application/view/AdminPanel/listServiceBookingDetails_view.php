<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Manage Bookings</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List Kailash Booking</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; ?>manageTestimonial/addTestimonial" class="btn btn-primary"><i class="fa fa-plus"></i> Add Package</a>
        </div>
    </div> -->
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <form action="<?php echo URL; echo 'kpBooking?kailash_date='.$kailash_date_id ?>" enctype="multipart/form-data" method="post">
                    <div class="row vertical-divider">
                        <div class="col-md-6">
                            <h3 class="form-section-title">Personal Details</h3>
                            <div class="form-group">
                                <label for="full_name">Full Name</label>
                                <input type="text" name="full_name" class="form-control" placeholder="Full Name" value="<?php if(isset($full_name)){ echo $full_name;} ?>" required>
                            </div>

                            <div class="form-group">
                                <label>Gender</label>
                                <div class="radio checkbox-inline">
                                    <label class="i-checks ">
                                        <input name="gender" id="gender-0" value="Male" type="radio"required>
                                        <i></i>
                                        Male
                                    </label>
                                </div>
                                <div class="radio checkbox-inline">
                                    <label class="i-checks ">
                                        <input name="gender" id="gender-1" value="Female" type="radio" required>
                                        <i></i>
                                        Female
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Nationality</label>
                                <input type="text" name="nationality" class="form-control" placeholder="Nationality" value="<?php if(isset($nationality)){ echo $nationality;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label>Date of Birth</label>
                                <input type="date" name="dob" class="form-control" placeholder="Date Of Birth" value="<?php if(isset($dob)){ echo $dob;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="passport_no">Passport No</label>
                                <input type="text" name="passport_no" class="form-control" placeholder="Passport No" value="<?php if(isset($passport_no)){ echo $passport_no;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="date_of_issue">Date of Issue</label>
                                 <input type="date" name="date_of_issue" class="form-control" placeholder="Date Of Issue" value="<?php if(isset($date_of_issue)){ echo $date_of_issue;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="place_of_issue">Place Of Issue</label>
                                <input type="date" name="place_of_issue" class="form-control" placeholder="Place Of Issue" value="<?php if(isset($place_of_issue)){ echo $place_of_issue;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="date_of_expiry">Date of Expiry</label>
                                <input type="date" name="date_of_expiry" class="form-control" placeholder="Date Of Expiry" value="<?php if(isset($date_of_expiry)){ echo $date_of_expiry;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="permanent_address">Permanent Address</label>
                                <input type="text" name="permanent_address" class="form-control" placeholder="Permanent Address" value="<?php if(isset($permanent_address)){ echo $permanent_address;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="profession">Profession</label>
                                <input type="text" name="profession" class="form-control" placeholder="Profession" value="<?php if(isset($profession)){ echo $profession;} ?>" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <h3 class="form-section-title">Personal Details</h3>
                            <div class="form-group">
                                <label for="mobile_no">Personal No</label>
                                <input type="number" min="1" name="mobile_no" class="form-control" placeholder="Personal No" value="<?php if(isset($mobile_no)){ echo $mobile_no;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="telephone">Telephone (Home)</label>
                                <input type="text" name="telephone" class="form-control" placeholder="Telephone (Home)" value="<?php if(isset($telephone)){ echo $telephone;} ?>">
                            </div>
                            <div class="form-group">
                                <label for="email_id">Email Address</label>
                                <input type="email" name="email_id" class="form-control" placeholder="Email Address" value="<?php if(isset($email_id)){ echo $email_id;} ?>" required>
                            </div>

                            <h3 class="form-section-title">In Case of Emergency Contact Person(Compulsory)</h3>
                            <div class="form-group">
                                <label for="e_name">Name</label>
                                <input type="text" name="e_name" class="form-control" placeholder="Name" value="<?php if(isset($e_name)){ echo $e_name;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="e_relation">Relation</label>
                                 <input type="text" name="e_relation" class="form-control" placeholder="Relation" value="<?php if(isset($e_relation)){ echo $e_relation;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="e_telephone">Phone Number</label>
                                <input type="text" name="e_telephone" class="form-control" placeholder="Phone Number" value="<?php if(isset($e_telephone)){ echo $e_telephone;} ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="e_email_id">Email Address</label>
                                <input type="text" name="e_email_id" class="form-control" placeholder="Email Address" value="<?php if(isset($e_email_id)){ echo $e_email_id;} ?>" required>
                            </div>


                            <h3 class="form-section-title">Tours Details</h3>
                            <div class="form-group">
                                <label for="tour_package_type">Tour Package</label>
                                <input disabled type="text" name="package_id" class="form-control" value="<?php if($kailash_date_object){ echo $kailash_date_object->kp_title.' ('.$kailash_date_object->from_date.' / '.$kailash_date_object->to_date.')'; } ?>">
                            </div>
                            <div class="form-group">
                                <label for="preffered_arrival_date">Preferred Arrival Date</label>
                                <input type="date" name="preffered_arrival_date" class="form-control" placeholder="Preferred Arrival Date" value="<?php if(isset($preffered_arrival_date)){ echo $preffered_arrival_date;} ?>">
                            </div>

                            <!-- <div class="form-group">
                                <label for="adv_deposit">Advance Deposit</label>
                                <input type="text" name="adv_deposit" class="form-control" placeholder="Advance Deposit">
                            </div> -->
                        </div>
                    </div>
                    
                </form>
        </div>
      </div>
    </div>
  </div>
</div>
