<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>FAQ</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List FAQ</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; ?>manageFAQ/addFAQ" class="btn btn-primary"><i class="fa fa-plus"></i> Add FAQ</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <table class="table">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>FAQ Title</th>
                  <th>Description</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  $i = 1;
                  foreach($FAQs as $FAQ){
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $FAQ->title; ?></td>
                    <td><?php echo $FAQ->description; ?></td>
                    <td>
                      <form style="margin-left:1%; float:left;">
                        <a href="#"  title="View"  class="btn" style="border:solid 1px #9e9e9e;"><i class="fa fa-eye" ></i></a>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; ?>manageFAQ/editFAQ">
                          <button title="Edit" class="btn btn-info" name="submit_to_edit" type="submit"  value="<?php echo $FAQ->id;?>"><i class="fa fa-edit"></i></button>
                      </form>


                      <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; ?>manageFAQ/deleteFAQ" onSubmit="return confirm('Are you sure you want to delete this data?');">
                          <button title="Delete" type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $FAQ->id;?>"><i class="fa fa-trash"></i></button>
                      </form>
                      

                    </td>
                  </tr>
                  <?php
                  }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
