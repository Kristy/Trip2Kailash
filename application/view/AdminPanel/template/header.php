<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo $browser_title; ?></title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL; ?>AdminPanel_assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link rel="<?php echo URL; ?>AdminPanel_assets/icon" href="favicon.ico" type="image/x-icon"/>

    <!-- Toastr style -->
    <link href="<?php echo URL; ?>AdminPanel_assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- cropper style -->
    <link href="<?php echo URL; ?>AdminPanel_assets/css/plugins/cropper/cropper.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo URL; ?>AdminPanel_assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="<?php echo URL; ?>AdminPanel_assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo URL; ?>AdminPanel_assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" rel="stylesheet"> -->
    <link href="<?php echo URL; ?>AdminPanel_assets/css/style.css" rel="stylesheet">

    <!--Summernote csss--->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0-rc.2/themes/smoothness/jquery-ui.css">
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">

  <!-- Select2 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">

  <!-- gallery -->
  <link href="<?php echo URL; ?>AdminPanel_assets/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Admin</strong>
                             </span> <span class="text-muted text-xs block">Manager<b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <!-- <li><a href="profile.html">Profile</a></li> -->
                                <li><a href="#">Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo URL; ?>cmslogin/logout">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            T2K
                        </div>
                    </li>
                    <!-- <li <?php if(isset($ActivePage)){ if($ActivePage == "DashBoard"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>DashBoardPanel"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                    </li> -->
                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Banner"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageBanner"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Banner</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Aboutus_Theme"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manage_aboutus_theme"><i class="fa fa-th-large"></i> <span class="nav-label">About Us</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Kailash_Page"){ ?> class="active" <?php } }  ?> >
                        <a><i class="fa fa-th-large"></i> <span class="nav-label">Manage Kailash</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Kailash_Detail_Page"){ ?> class="active" <?php } }  ?>><a href="<?php echo URL; ?>manage_kailash_detail">Kailash Detail</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Manage_Package"){ ?> class="active" <?php } }  ?>><a href="<?php echo URL; ?>manageKailashPackage">Kailash Packages</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Manage_Guide"){ ?> class="active" <?php } }  ?>><a href="<?php echo URL; ?>manageKailashGuide">Kailash Guides</a></li>
                        </ul>
                    </li>

                    <li<?php if(isset($ActivePage)){ if($ActivePage == "Service"){ ?> class="active" <?php } }  ?> >
                        <a><i class="fa fa-th-large"></i> <span class="nav-label">Services</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "ManageHighlight"){ ?> class="active" <?php } }  ?> ><a href="<?php echo URL; ?>manageHighlight">Manage Highlight</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Manage_Destination"){ ?> class="active" <?php } }  ?> ><a href="<?php echo URL; ?>manageDestination">Manage Destination</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Manage_Activity"){ ?> class="active" <?php } }  ?> ><a href="<?php echo URL; ?>manageActivity">Manage Activity</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Manage_Package"){ ?> class="active" <?php } }  ?> ><a href="<?php echo URL; ?>managePackage">Manage Package</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Manage_Destination_Guide"){ ?> class="active" <?php } }  ?> ><a href="<?php echo URL; ?>manageDestinationGuide">Manage Destination Guide</a></li>
                        </ul>
                    </li>

                     <li <?php if(isset($ActivePage)){ if($ActivePage == "Booking"){ ?> class="active" <?php } }  ?> >
                        <a><i class="fa fa-th-large"></i> <span class="nav-label">Manage Booking</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Kailash_booking"){ ?> class="active" <?php } }  ?>><a href="<?php echo URL; ?>manageBooking">Kailash Booking</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Service_booking"){ ?> class="active" <?php } }  ?>><a href="<?php echo URL; ?>manageServiceBooking">Service Booking</a></li>
                        </ul>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Trailor"){ ?> class="active" <?php } }  ?> >
                        <a><i class="fa fa-th-large"></i> <span class="nav-label">Manage Trailor</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Kailash_trailor"){ ?> class="active" <?php } }  ?>><a href="<?php echo URL; ?>managekpTrailor">Kailash Trailor</a></li>
                            <li <?php if(isset($PageBranch)){ if($PageBranch == "Service_trailor"){ ?> class="active" <?php } }  ?>><a href="<?php echo URL; ?>managePackageTrailor">Service Trailor</a></li>
                        </ul>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Essential_Info"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manage_essential_info"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Essentail Info</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Associated"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageAssociatedWith"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Associated</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Affilated"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageAffilatedTo"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Affilated</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_TeamMember"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageTeamMember"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Team Member</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_FAQ"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageFAQ"><i class="fa fa-th-large"></i> <span class="nav-label">Manage FAQ</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Icon"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageIcon"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Icon</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Attraction"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageAttraction"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Attraction</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Manage_Testimonial"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageTestimonial"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Testimonial</span></a>
                    </li>

                    <li <?php if(isset($ActivePage)){ if($ActivePage == "Gallery"){ ?> class="active" <?php } }  ?> >
                        <a href="<?php echo URL; ?>manageGallery"><i class="fa fa-th-large"></i> <span class="nav-label">Manage Gallery</span></a>
                    </li>

                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Welcome to Trip2Kailash Admin Panel</span>
                        </li>
                        <li>
                            <a href="<?php echo URL; ?>cmslogin/logout">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
