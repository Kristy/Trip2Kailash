            <div class="footer">
              <div class="pull-right">
              </div>
              <div>
                <strong>Trip2Kailash</strong> 
              </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    <script
			  src="https://code.jquery.com/ui/1.12.0-rc.2/jquery-ui.min.js"
			  integrity="sha256-55Jz3pBCF8z9jBO1qQ7cIf0L+neuPTD1u7Ytzrp2dqo="
			  crossorigin="anonymous"></script>
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/flot/jquery.flot.spline.js"></script>

    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo URL; ?>AdminPanel_assets/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/inspinia.js"></script>
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/demo/sparkline-demo.js"></script>
    <!-- Image cropper -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/cropper/cropper.min.js"></script>
    <!-- ChartJS-->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/toastr/toastr.min.js"></script>

    <!-- Bootstrap WYSIHTML5 -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>

    <!-- Select2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
    <!-- blueimp gallery -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
    <script>
        <?php
        if(isset($flashmsg)){
          if ($flashmsg->hasMessages()) {
        ?>
        $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Admin Panel', '<?php echo $flashmsg->hasMessages()[0]["message"] ?>');

            }, 1300);

        });
        <?php
          }
        }
        ?>
        //Initialize Select2 Elements
        $(".select2").select2();
        // main summernote
        var full_custom_summer = {
          height: 200,
          dialogsInBody: false,
          dialogsFade: true,
          disableDragAndDrop: false,
          shortcuts: false,
          toolbar: [
              // [groupName, [list of button]]
              ['fontstyle', ['fontname','fontsize','color','bold', 'italic', 'underline']],
              ['font', ['strikethrough', 'superscript', 'subscript']],
              ['clear', [ 'clear']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['Insert', ['picture', 'link', 'video', 'table', 'hr']],
              // ['height', ['height']],
              ['Misc', ['undo', 'redo']]
          ],
          fontNames: ['open sans', 'Arial', 'Arial Black', 'Roboto',  'Arimo', 'Times New Roman'],
          fontNamesIgnoreCheck: ['Arial Black', 'Arimo', 'Roboto'],
        };
        var font_custom_summer = {
          dialogsInBody: false,
          dialogsFade: true,
          disableDragAndDrop: false,
          shortcuts: false,
          toolbar: [
              // [groupName, [list of button]]
              ['fontstyle', ['fontname','color','bold', 'italic', 'underline']],
          ],
          fontNames: ['open sans', 'Arial', 'Arial Black', 'Roboto',  'Arimo', 'Times New Roman'],
          fontNamesIgnoreCheck: ['Arial Black', 'Arimo', 'Roboto'],
        };

        $('#summernote').summernote(full_custom_summer);
        $('.summernote').summernote(font_custom_summer);

        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            $('#image_upload_preview').attr('width', 1000);
            $('#image_upload_preview').attr('height', 625);
            $('#image_upload_preview').attr('style', 'display:block;');
            }

            reader.readAsDataURL(input.files[0]);
        }
        }

        $("#userfile").change(function () {
        readURL(this);
        });

        $("#clear").click(function () {
        $('#image_upload_preview').attr('src', '');
        $('#image_upload_preview').attr('width', 0);
        $('#image_upload_preview').attr('height', 0);
        $('#image_upload_preview').attr('style', 'display:none;');
        });


    </script>
    <!--drop zone  -->
    <script src="<?php echo URL; ?>AdminPanel_assets/js/plugins/dropzone/dropzone.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script> -->
    <script>
        $(document).ready(function(){
          var $image = $(".image-crop > img")

          $($image).cropper({
              aspectRatio: 1.951,
              preview: ".img-preview",
              done: function(data) {
                  // Output the result data for cropping image.
                  console.log(data);
                  // window.open($image.cropper("getDataURL"));
                  $( '#imagex' ).val(data.x);
                  $( '#imagey' ).val(data.y);
                  $( '#imagew' ).val(data.width);
                  $( '#imageh' ).val(data.height);

              }
          });

          var $inputImage = $("#inputImage");
          if (window.FileReader) {
              $inputImage.change(function() {
                  var fileReader = new FileReader(),
                          files = this.files,
                          file;

                  if (!files.length) {
                      return;
                  }

                  file = files[0];

                  if (/^image\/\w+$/.test(file.type)) {
                      fileReader.readAsDataURL(file);
                      fileReader.onload = function () {
                          // $inputImage.val("");
                          $image.cropper("reset", true).cropper("replace", this.result);
                      };
                  } else {
                      showMessage("Please choose an image file.");
                  }
              });
          } else {
              $inputImage.addClass("hide");
          }

          // $("#download").click(function() {
          //     var imgaedata = $image.cropper("getCroppedCanvas");
          //     console.log(imgaedata);
          //     var posturl = "<?php echo URL; echo 'DashBoardPanel/addBanner' ?>";
          //     // window.open($image.cropper("getDataURL"));
          //     var formData = new FormData();
          //     console.log(formData);
          //     formData.append('croppedImage', imgaedata);
          //     console.log(formData.append('croppedImage', imgaedata));
          //     $.ajax(posturl, {
          //       method: "POST",
          //       data: formData,
          //       processData: false,
          //       contentType: false,
          //       success: function () {
          //         console.log('Upload success');
          //       },
          //       error: function () {
          //         console.log('Upload error');
          //       }
          //     });
          // });

          $("#zoomIn").click(function() {
              $image.cropper("zoom", 0.1);
          });

          $("#zoomOut").click(function() {
              $image.cropper("zoom", -0.1);
          });

          $("#setDrag").click(function() {
              $image.cropper("setDragMode", "crop");
          });

            Dropzone.options.myAwesomeDropzone = {
                autoProcessQueue: false,
                // uploadMultiple: true,
                parallelUploads: 50,
                maxFiles: 20,
                acceptedFiles: 'image/*',
                addRemoveLinks: true,
                maxFilesize: 10,
                // Dropzone settings
                init: function() {
                    var myDropzone = this;

                    this.element.querySelector("#submit-dropzone").addEventListener("click", function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                      });
                    this.on("sendingmultiple", function() {
                    });
                    this.on("successmultiple", function(files, response) {
                    });
                    this.on("errormultiple", function(files, response) {
                    });
                    this.on("error", function(file, message) {
                        alert(message);
                        this.removeFile(file);
                    });
                }

            }

       });
    </script>
  </body>
</html>
