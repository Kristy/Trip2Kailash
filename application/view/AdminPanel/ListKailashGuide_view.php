<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Kailash Package</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>List Kailash Guides</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="<?php echo URL; ?>manageKailashGuide/addGuide" class="btn btn-primary"><i class="fa fa-plus"></i> Add Package</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <table class="table">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>Guide Image</th>
                  <th>Guide Title</th>
                  <th>Description</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  $i = 1;
                  foreach($guides as $guide){
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td>
                      <div class="lightBoxGallery">
                              <a style="color:inherit;" href="<?php echo URL; echo 'uploads/'.$guide->image; ?>" data-gallery="">
                                <?php echo $guide->image; ?>
                              </a>
                          <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                          <div id="blueimp-gallery" class="blueimp-gallery">
                              <div class="slides"></div>
                              <h3 class="title"></h3>
                              <a class="prev">‹</a>
                              <a class="next">›</a>
                              <a class="close">×</a>
                              <a class="play-pause"></a>
                              <ol class="indicator"></ol>
                          </div>

                      </div>
                    </td>
                    <td><?php echo $guide->title; ?></td>
                    <td><?php echo $guide->description; ?></td>
                    <td>
                      <form style="margin-left:1%; float:left;">
                        <a href="#"  title="View"  class="btn" style="border:solid 1px #9e9e9e;"><i class="fa fa-eye" ></i></a>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="GET" action="<?php echo URL; ?>manageKailashGuide/editGuide">
                          <button title="Edit" class="btn btn-info" name="submit_to_edit" type="submit"  value="<?php echo $guide->id;?>"><i class="fa fa-edit"></i></button>
                      </form>
                      <form style="margin-left:1%;  float:left;" method="post" action="<?php echo URL; ?>manageKailashGuide/deletePackage" onSubmit="return confirm('Are you sure you want to delete this data?');">
                          <button title="Delete" type="submit" class="btn btn-danger" name="SubmitDelete" value="<?php echo $guide->id;?>"><i class="fa fa-trash"></i></button>
                      </form>
                      

                    </td>
                  </tr>
                  <?php
                  }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
